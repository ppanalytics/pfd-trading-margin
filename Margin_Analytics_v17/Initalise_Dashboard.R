#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#          INITIALISE DATABRICKS SPARKLYR CONNECTION AND FILTERS          #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 





#############################-INITALISING SPARKLYR AND DATABRICKS CONNECT -##############################----

library(sparklyr)
library(dplyr)

print(sessionInfo())

# Dashboard Logo
logo <- "./inputs/PFDLogo250h.png"

# Load PFD calendar data
PFD_Calendar <- read_csv("./inputs/PFD_BI_Calendar.csv")

#############################- 1A. Sparklyr - Local Version -##############################----
# # # Spark home for local
# sc <- spark_connect(method = "databricks", spark_home = "c:/users/gaov/anaconda3/envs/dbtest/lib/site-packages/pyspark")

# # # Connecting to Azure blob storage
# suppressMessages(library(AzureStor))

# endp <- AzureStor:::storage_endpoint("https://stpfdrpgmserver.blob.core.windows.net", key="H2ZYiJULDKd8xtkHO9HQWUQLoM0TcGauGBw+Ouyp4VaC+t1MD6EJoUorodmEWyUiripVJYauPDO2Eui4lyee4A==")
# cont <- AzureStor:::storage_container(endp, "test")
# fname <- tempfile()

# # Import filter lists from Azure blob storage
# Account_Manager_Master <- AzureStor::storage_read_csv(cont, "Account_Manager_Master.csv")
# Customer_Name_Master <- AzureStor::storage_read_csv(cont, "Customer_Name_Master.csv")
# Site_Name_Master <- AzureStor::storage_read_csv(cont, "Site_Name_Master.csv")
# Customer_Type_Master <- AzureStor::storage_read_csv(cont, "Customer_Type_Master.csv")
# Site_State_Master <- AzureStor::storage_read_csv(cont, "Site_State_Master.csv")
# # Item_Category_Master <- AzureStor::storage_read_csv(cont, "Item_Category_Master.csv")
# # Item_Description_Master <- AzureStor::storage_read_csv(cont, "Item_Description_Master.csv")

#############################- 1B. Sparklyr - Server version -##############################----
# setwd("/home/pfdadmin/") 
# sc <- spark_connect(method = "databricks", spark_home = '/usr/local/lib/python3.7/dist-packages/pyspark')

# # # Spark home for ubuntu VM
# # setwd("/home/pfdadmin/")
# sc <- spark_connect(method = "databricks", 
#                     spark_home = "/home/pfdadmin/.local/lib/python3.7/site-packages/pyspark")

# # Spark home for Windows VM
# # setwd("/users/pfdadmin/")
# sc <- spark_connect(method = "databricks", 
#                     spark_home = "C:/Users/pfdadmin/AppData/Local/Programs/Python/Python37/Lib/site-packages/pyspark")

# if (exists("sc")) {
#   print(sc)
# } else {
#   print("sc does not exist ")
# }

# # Spark home for VM
# # setwd("/users/pfdadmin/")
# sc <- spark_connect(method = "databricks", 
#                     spark_home = "C:/Users/pfdadmin/AppData/Local/Programs/Python/Python37/Lib/site-packages/pyspark")

# # Import filter lists from RPGM server
# Account_Manager_Master <- read_csv("http://52.255.38.7/files/Resources/filters/Account_Manager_Master.csv")
# Customer_Name_Master <- read_csv("http://52.255.38.7/files/Resources/filters/Customer_Name_Master.csv")
# Site_Name_Master <- read_csv("http://52.255.38.7/files/Resources/filters/Site_Name_Master.csv")
# Customer_Type_Master <- read_csv("http://52.255.38.7/files/Resources/filters/Customer_Type_Master.csv")
# Site_State_Master <- read_csv("http://52.255.38.7/files/Resources/filters/Site_State_Master.csv")


#############################- 2. Importing Base Table from Databricks as Spark Dataframe  -##############################----

# # Reading in entire base table as sparklyr spark dataframe
# TM_Base_Table_Raw <- spark_read_csv(sc, path="dbfs:/FileStore/df/pfd_trading_margin_full.csv/part-00000-tid-8852796306218385673-c0757c75-5f05-4171-bc92-4aa5dca4e676-197-1-c000.csv")

# # one-week extract of base table
# TM_Base_Table_Raw <- spark_read_csv(sc, path="dbfs:/FileStore/df/pfd_trading_margin_sample_v1.csv/part-00000-tid-6175667190246818734-00c16139-0f3a-4c7e-8880-3e4b5e4c03ae-10-1-c000.csv")

# # # ashley demo of dashboard 
# # TM_Base_Table_Raw <- spark_read_csv(sc, path="dbfs:/FileStore/tables/TMBaseTableSample_AshleyDemo20Jul21.csv/")

#############################- 2. Exporting Sample Data 2 to Databricks  -##############################----

# # Additional sample data provided by Ash  on 210906
# # PFD Trading Margin - Sample Data 2 - 2020-2021
# TM_Base_Table_Raw <- spark_read_csv(sc, path="dbfs:/FileStore/tables/TMBaseTableSample_2021-1.csv/")
  
# TM_Base_Filters <- read_csv("./inputs/TMBaseTableSample_2021.csv") %>%
#   select(Account_Manager, Site_Name, Customer_Type, Site_State, Customer_Name) %>%
#   distinct(Account_Manager, Site_Name, Customer_Type, Site_State, Customer_Name)

# Account_Manager_Master <- TM_Base_Filters %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager)
# Site_Name_Master <- TM_Base_Filters %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name)
# Customer_Type_Master <- TM_Base_Filters %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type)
# Site_State_Master <- TM_Base_Filters %>% select(Site_State) %>% distinct() %>% arrange(Site_State)


#############################- 2b. Importing Base Table Filters  -##############################----


# # # Import filter lists from base table
# TM_Base_Filters <- read_csv("./inputs/TM_Base_Filters.csv") %>% 
#         mutate_if(is.character, ~replace(., is.na(.), 'UNK'))

# Account_Manager_Master <- TM_Base_Filters %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager)
# Site_Name_Master <- TM_Base_Filters %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name)
# Customer_Type_Master <- TM_Base_Filters %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type)
# Site_State_Master <- TM_Base_Filters %>% select(Site_State) %>% distinct() %>% arrange(Site_State)

# # Account_Manager_Master <- TM_Base_Table_Raw %>% select(Account_Manager) %>% distinct() %>% collect() %>% arrange(Account_Manager)
# # Customer_Name_Master <- TM_Base_Table_Raw %>% select(Customer_Name) %>% distinct() %>% collect() %>% arrange(Customer_Name) 
# # Site_Name_Master <- TM_Base_Table_Raw %>% select(Site_Name) %>% distinct() %>% collect() %>% arrange(Site_Name)
# # Customer_Type_Master <- TM_Base_Table_Raw %>% select(Customer_Type) %>% distinct() %>% collect() %>% arrange(Customer_Type)
# # Site_State_Master <- TM_Base_Table_Raw %>% select(Site_State) %>% distinct() %>% collect() %>% arrange(Site_State)

# print(count(TM_Base_Table_Raw))


#############################-INITALISING LOCAL R VERSION -##############################----

# # local extract of base table - 58k rows from Dec
# TM_Base_Table_Local <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/TMBaseTableSample.csv") %>%
#     mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
#     mutate_if(is.character, ~replace(., is.na(.), 'UNK'))

# # local extract of base table - 484k rows from Dec 2020
# TM_Base_Table_Local <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/TMBaseTableDec2020.csv") %>%
#     mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
#     mutate_if(is.character, ~replace(., is.na(.), 'UNK'))

# # local extract of base table for RPGM server
# TM_Base_Table_Local <- read_csv("./inputs/TMBaseTableSample.csv") %>%
#     mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
#     mutate_if(is.character, ~replace(., is.na(.), 'UNK'))

# # local extract of base table for July 2021 demo with Ashley
# # PFD Trading Margin - Sample Data - Dec 2019-2020
# TM_Base_Table_Local <- read_csv("./inputs/TMBaseTableSample_AshleyDemo20Jul21.csv") %>%
#     mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
#     mutate_if(is.character, ~replace(., is.na(.), 'UNK'))

# # Bathurst RSL data and calculations from Ashley provided on 23/08/21 for 2020 vs 2021 - Existing Items Only
# # PFD Trading Margin - Bathurst RSL - 2020-2021
# TM_Base_Table_Local <- read_csv("./inputs/Bathurst_RSL_Data.csv") %>%
#     mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
#     mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
#     mutate(Invoice_date = as.Date(Invoice_date, format="%d/%m/%Y"),
#             Week_Start_Date = as.Date(Week_Start_Date, format="%d/%m/%Y"))

# # Additional sample data provided by Ash  on 210906
# PFD Trading Margin - Sample Data 2 - 2020-2021
TM_Base_Table_Local <- read_csv("./inputs/TMBaseTableSample_2021.csv") %>%
    mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
    mutate_if(is.character, ~replace(., is.na(.), 'UNK'))
  
TM_Base_Filters <- TM_Base_Table_Local %>%
  select(Account_Manager, Site_Name, Customer_Type, Site_State, Customer_Name) %>%
  distinct(Account_Manager, Site_Name, Customer_Type, Site_State, Customer_Name)

Account_Manager_Master <- TM_Base_Filters %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager)
Site_Name_Master <- TM_Base_Filters %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name)
Customer_Type_Master <- TM_Base_Filters %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type)
Site_State_Master <- TM_Base_Filters %>% select(Site_State) %>% distinct() %>% arrange(Site_State)

Customer_Name_Master <- TM_Base_Table_Local %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name)
Item_Category_Master <- TM_Base_Table_Local %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category)
Item_Description_Master <- TM_Base_Table_Local %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description)


#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                         INITALISE FILTERS                               #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 

# Add all as selection choice in list
Customer_Type_List <- data.frame("Customer_Type" = t(c("All"))) %>% rbind(Customer_Type_Master)
Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% rbind(Site_State_Master)
Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% rbind(Site_Name_Master)
Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% rbind(Account_Manager_Master)
# Customer_Name_List <- data.frame("Customer_Name" = t(c("Select customer"))) %>% rbind(Customer_Name_Master)
# Item_Category_List <- data.frame("Item_Category" = t(c("All"))) %>% rbind(Item_Category_Master)
# Item_Description_List <- data.frame("Item_Description" = t(c("All"))) %>% rbind(Item_Description_Master)

gui.clearChoices(rpgm.step("main", "TMA"),"CustType_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"CustType_Filter_Selector",Customer_Type_List$Customer_Type, Customer_Type_List$Customer_Type)

gui.clearChoices(rpgm.step("main", "TMA"),"State_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"State_Filter_Selector",Site_State_List$Site_State, Site_State_List$Site_State)

gui.clearChoices(rpgm.step("main", "TMA"),"SiteName_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"SiteName_Filter_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)

gui.clearChoices(rpgm.step("main", "TMA"),"AccManager_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"AccManager_Filter_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)

# # Set choices for customer selector
# gui.clearChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer")
# gui.addChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)

gui.clearChoices(rpgm.step("main", "TMA"),"State_Basket_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"State_Basket_Filter_Selector",Site_State_List$Site_State, Site_State_List$Site_State)

gui.clearChoices(rpgm.step("main", "TMA"),"SiteName_Basket_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"SiteName_Basket_Filter_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)

gui.clearChoices(rpgm.step("main", "TMA"),"AccManager_Basket_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"AccManager_Basket_Filter_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)