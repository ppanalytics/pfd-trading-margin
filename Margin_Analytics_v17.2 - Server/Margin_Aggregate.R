# Generates table to populate generate_PricingReason_Waterfall and generate_Waterfall_Table
margin_aggregate <- function(){
  
  start_time <- Sys.time()
  
  # Add all selection to filters within TM breakdown 
  if (all(custtype_filter_selector=='All')){
    # custtype_group = Customer_Type_Master
    custtype_group <- as.character((Customer_Type_Master%>%rbind("UNK"))$Customer_Type)
  } else {
    custtype_group <- custtype_filter_selector
  }
  
  if (all(state_filter_selector=='All')){
    # state_group = Site_State_Master
    state_group <- as.character((Site_State_Master%>%rbind("UNK"))$Site_State)
  } else {
    state_group <- state_filter_selector
  }
  
  if (all(sitename_filter_selector=='All')){
    # sitename_group = Site_Name_Master
    sitename_group <- as.character((Site_Name_Master%>%rbind("UNK"))$Site_Name)
  } else {
    sitename_group <- sitename_filter_selector
  }
  
  if (all(accmanager_filter_selector=='All')){
    # accmanager_group = Account_Manager_Master
    accmanager_group <- as.character((Account_Manager_Master%>%rbind("UNK"))$Account_Manager)
  } else {
    accmanager_group <- accmanager_filter_selector
  }
  
  # Filter dataframe based on user-selected GUI filters
  tm_test_1 <- tm_test_1 %>%
    filter(Customer_Type %in% custtype_group,
           Site_State %in% state_group,
           Site_Name %in% sitename_group,
           Account_Manager %in% accmanager_group)
  
  # Filter dataframe based on user-selected GUI filters
  tm_test_2 <- tm_test_2 %>%
    filter(Customer_Type %in% custtype_group,
           Site_State %in% state_group,
           Site_Name %in% sitename_group,
           Account_Manager %in% accmanager_group)
  
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(50))
  gui.setValue("this","Main_Progress_Bar",round(50))

  
  # Get summarised data tables for both time periods
  tm_test_1_item <- calculate_TM(tm_test_1 %>% 
                                   group_by(Account_Manager,
                                            Site_Name,
                                            Site_State,
                                            Customer_Type,
                                            Customer_Name, 
                                            Item_Category,
                                            Item_Description)) 
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(55))
  gui.setValue("this","Main_Progress_Bar",round(55))

  # Get summarised data tables for both time periods
  tm_test_2_item <- calculate_TM(tm_test_2 %>% 
                                   group_by(Account_Manager,
                                            Site_Name,
                                            Site_State,
                                            Customer_Type,
                                            Customer_Name, 
                                            Item_Category,
                                            Item_Description) ) 

  
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(60))
  gui.setValue("this","Main_Progress_Bar",round(60))
  
  # collect two periods into R dataframe
  tm_test_1_item <- collect(tm_test_1_item) 
  
  tm_test_1_item <- tm_test_1_item%>% 
    mutate(Customer_Product = paste0(Customer_Name, " - ", Item_Description))
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(65))
  gui.setValue("this","Main_Progress_Bar",round(65))
  
  # collect two periods into R dataframe
  tm_test_2_item <- collect(tm_test_2_item) 
  
  tm_test_2_item <- tm_test_2_item%>% 
    mutate(Customer_Product = paste0(Customer_Name, " - ", Item_Description))
  
  # Error handling for incorrect period selection
  if (is.na(nrow(tm_test_1_item)) | nrow(tm_test_1_item) == 0) {
    rpgm.notification("error", "Warning! No data for Prior Period. Please enter a valid period.", duration = 5000)
    opt <- options(show.error.messages = FALSE)
    on.exit(options(opt))
    stop()
  } else if (is.na(nrow(tm_test_2_item)) | nrow(tm_test_2_item) == 0) {
    rpgm.notification("error", "Warning! No data for Current Period. Please enter a valid period.", duration = 5000)
    opt <- options(show.error.messages = FALSE)
    on.exit(options(opt))
    stop()
  }
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(70))
  gui.setValue("this","Main_Progress_Bar",round(70))
  
  join_cols = c("Account_Manager", "Site_Name", "Site_State", "Customer_Type",
                "Customer_Name", "Item_Category", "Item_Description", "Customer_Product")
  
  # Compare periods
  df_1_vs_2 <- compare_periods(tm_test_1_item, tm_test_2_item, join_cols) %>%
    mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
    mutate_if(is.numeric, ~replace(., is.nan(.), 0)) %>%
    mutate_if(is.numeric, ~round(., 2))

  # Assigning reason for change in trading margin impact for each row
  tm_breakdown_table <- assign_pricingreasons(df_1_vs_2)

  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(80))
  gui.setValue("this","Main_Progress_Bar",round(80))
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(90))
  gui.setValue("this","Main_Progress_Bar",round(90))
  
  
  # data for generate_waterfall_overview()
  assign("waterfall_data_dollar", (get_waterfall_overview_data(tm_breakdown_table))$waterfall_data_dollar, envir = .GlobalEnv)
  assign("waterfall_data_percent", (get_waterfall_overview_data(tm_breakdown_table))$waterfall_data_percent, envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(92))
  gui.setValue("this","Main_Progress_Bar",round(92))

  # data for generate_Waterfall_NetImpact()
  assign("waterfall_netimpact_data_dollar", (get_waterfall_netimpact_data(tm_breakdown_table))$waterfall_netimpact_data_dollar, envir = .GlobalEnv)
  assign("waterfall_netimpact_data_percent", (get_waterfall_netimpact_data(tm_breakdown_table))$waterfall_netimpact_data_percent, envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(93))
  gui.setValue("this","Main_Progress_Bar",round(93))
  
  # data for generate_Waterfall_IncrDecr()
  assign("waterfall_incrdecr_data_percent", (get_waterfall_incrdecr_data(tm_breakdown_table))$waterfall_incrdecr_data_percent, envir = .GlobalEnv)
  assign("waterfall_incrdecr_data_dollar", (get_waterfall_incrdecr_data(tm_breakdown_table))$waterfall_incrdecr_data_dollar, envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(94))
  gui.setValue("this","Main_Progress_Bar",round(94))
  
  # data for generate_Waterfall_Table()
  assign("waterfall_table_dollar", (get_waterfall_table(tm_breakdown_table))$waterfall_table_dollar, envir = .GlobalEnv)
  assign("waterfall_table_percent", (get_waterfall_table(tm_breakdown_table))$waterfall_table_percent, envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(95))
  gui.setValue("this","Main_Progress_Bar",round(95))
  
  # data for generate_PricingReason_Waterfall()
  assign("pricingreasons_waterfall_data_dollar", (get_pricingreasons_data(tm_breakdown_table)$pricingreasons_waterfall_data_dollar), envir = .GlobalEnv)
  assign("pricingreasons_waterfall_data_percent", (get_pricingreasons_data(tm_breakdown_table)$pricingreasons_waterfall_data_percent), envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(96))
  gui.setValue("this","Main_Progress_Bar",round(96))
  
  # data for generate_PricingReason_Waterfall()
  assign("pricingreasons_waterfall_data_percent", (get_pricingreasons_data(tm_breakdown_table)$pricingreasons_waterfall_data_percent), envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(98))
  gui.setValue("this","Main_Progress_Bar",round(98))
  
  # data for generate_BreakdownBar()
  assign("breakdownbar_data", get_breakdownbar_data(tm_breakdown_table), envir = .GlobalEnv)
  
  # Update progress bar
  gui.setValue("this","Margin_Breakdown_Progress",round(100))
  gui.setValue("this","Main_Progress_Bar",round(100))


  # Time taken for function assigned to global environment
  end_time <- Sys.time()
  time_taken <- end_time - start_time
  assign("margin_aggregate_time", time_taken, envir = .GlobalEnv)
  
  cat(margin_aggregate_time)
  
}


get_waterfall_overview_data <- function(tm_breakdown_table) {
  
  waterfall_data_dollar = list(
    # 1 'Prior Period <br>Trading Margin',
    sum(tm_breakdown_table$Trading_Margin_1),
    # 2 'Existing Sales<br>Pricing Impact',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Price_TM_impact),
    # 3 'Existing Sales<br>Volume Impact',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Volume_TM_impact),
    # 5 'Lost Sales',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
    # 4 'New Sales',
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
    # 6 'Current Period <br>Trading Margin'
    sum(tm_breakdown_table$Trading_Margin_2)
  )
  
  # Calculate each component impact on net sales dollar
  net_sales_impact = c(# 'Prior Period<br>Trading Margin',
    sum(tm_breakdown_table$Net_Sales_1),
    # 'Existing Sales<br>Pricing Impact',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Price_sales_impact),
    # 'Existing Sales<br>Volume Impact',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Volume_sales_impact),
    # 'Lost Sales',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$Change_in_netsales),
    # 'New Sales',
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$Change_in_netsales),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Net_Sales_2))
  
  
  tm_dollar_impact = c(# 'Prior Period<br>Trading Margin',
    sum(tm_breakdown_table$Trading_Margin_1),
    # 'Existing Sales<br>Pricing Impact',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Price_TM_impact),
    # 'Existing Sales<br>Volume Impact',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Volume_TM_impact),
    # 'Lost Sales',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
    # 'New Sales',
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Trading_Margin_2))
  
  
  tm_percent_value = list()
  
  # Calculate the TM percentage for each component impact on TM $ and Net Sales
  for (i in (1:length(tm_dollar_impact))) {
    tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[1:i]) / sum(net_sales_impact[1:i]) * 100))
  }
  tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[length(tm_dollar_impact)]) / sum(net_sales_impact[length(tm_dollar_impact)]) * 100))
  tm_percent_value <- unlist(tm_percent_value)
  
  
  
  # Initalise waterfall data with impact values for TM percentage
  waterfall_data_percent <- list(tm_percent_value[1])
  for (i in (2:length(tm_percent_value)-1)) {
    waterfall_data_percent <- append(waterfall_data_percent, tm_percent_value[i] - tm_percent_value[i-1])
  }
  waterfall_data_percent[length(waterfall_data_percent)] <-  tm_percent_value[length(tm_percent_value)]
  
  
  waterfall_data <- list(waterfall_data_percent = waterfall_data_percent
                         , waterfall_data_dollar = waterfall_data_dollar)
  
  return(waterfall_data)
  
}



get_waterfall_netimpact_data <- function(tm_breakdown_table) {
  
  # Underlying data for x variables specified above
  # Filters should be self-explanatory
  waterfall_netimpact_data_dollar = list(
    # 'Prior Period<br>Trading Margin', 
    sum(tm_breakdown_table$Trading_Margin_1),
    # 'Net Impact of<br>Price on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Sell_sales_impact),
    # 'Net Impact of<br>COGS on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$COGS_impact),
    # 'Net Change in<br>Supplier Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Claim_sales_impact),
    # 'Net Change in<br>Promotional Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Promo_sales_impact),
    # 'Net Change in<br>Customer Rebates',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Rebate_sales_impact),
    # 'Net Impact of<br>Volume and Mix on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Volume_TM_impact),
    # 'Lost Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
    # 'New Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Trading_Margin_2)
  )
  
  waterfall_netimpact_data_dollar[] <- lapply(waterfall_netimpact_data_dollar,round,0)
  
  
  # Calculate each component impact on net sales dollar
  net_sales_impact = c(
    # 'Prior Period<br>Trading Margin', 
    sum(tm_breakdown_table$Net_Sales_1),
    # 'Net Impact of<br>Price on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Sell_sales_impact),
    # 'Net Impact of<br>COGS on<br>Existing Sales', 
    0,
    # 'Net Change in<br>Supplier Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Claim_sales_impact),
    # 'Net Change in<br>Promotional Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Promo_sales_impact),
    # 'Net Change in<br>Customer Rebates',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Rebate_sales_impact),
    # 'Net Impact of<br>Volume and Mix on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Volume_sales_impact),
    # 'Lost Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$Change_in_netsales),
    # 'New Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$Change_in_netsales),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Net_Sales_2)
  )
  
  # Calculate each component impact on trading margin dollar
  tm_dollar_impact = c(
    # 'Prior Period<br>Trading Margin', 
    sum(tm_breakdown_table$Trading_Margin_1),
    # 'Net Impact of<br>Price on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Sell_sales_impact),
    # 'Net Impact of<br>COGS on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$COGS_impact),
    # 'Net Change in<br>Supplier Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Claim_sales_impact),
    # 'Net Change in<br>Promotional Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Promo_sales_impact),
    # 'Net Change in<br>Customer Rebates',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Rebate_sales_impact),
    # 'Net Impact of<br>Volume and Mix on<br>Existing Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Volume_TM_impact),
    # 'New Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
    # 'Lost Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Trading_Margin_2)
  )
  
  # Calculate the TM percentage for each component impact on TM $ and Net Sales
  tm_percent_value = list()
  for (i in (1:length(tm_dollar_impact))) {
    tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[1:i]) / sum(net_sales_impact[1:i]) * 100))
  }
  
  tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[length(tm_dollar_impact)]) / sum(net_sales_impact[length(tm_dollar_impact)]) * 100))
  tm_percent_value <- unlist(tm_percent_value)
  
  
  # Initalise waterfall data with impact values for TM percentage
  waterfall_netimpact_data_percent <- list(tm_percent_value[1])
  for (i in (2:length(tm_percent_value)-1)) {
    waterfall_netimpact_data_percent <- append(waterfall_netimpact_data_percent, tm_percent_value[i] - tm_percent_value[i-1])
  }
  
  waterfall_netimpact_data_percent[length(waterfall_netimpact_data_percent)] <-  tm_percent_value[length(tm_percent_value)]
  
  
  waterfall_netimpact_data_percent[] <- lapply(waterfall_netimpact_data_percent,round,2)
  
  
  
  
  waterfall_data <- list(waterfall_netimpact_data_percent = waterfall_netimpact_data_percent
                         , waterfall_netimpact_data_dollar = waterfall_netimpact_data_dollar)
  
  return(waterfall_data)
  
}



get_waterfall_incrdecr_data <- function(tm_breakdown_table) {
  
  
  waterfall_incrdecr_data_dollar = list(
    # 'Prior Period<br>Trading Margin', 
    sum(tm_breakdown_table$Trading_Margin_1),
    # 'Impact of<br>Price Increases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_sell>0))$Sell_sales_impact),
    # 'Impact of<br>Price Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_sell<=0))$Sell_sales_impact),
    # 'Impact of<br>COGS Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_COGS_u<=0))$COGS_impact),
    # 'Impact of<br>COGS Increases',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_COGS_u>0))$COGS_impact),
    # 'Impact of<br>Supplier Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Claim_sales_impact),
    # 'Impact of<br>Promotional Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Promo_sales_impact),
    # 'Impact of<br>Customer Rebates',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Rebate_sales_impact),
    # 'Impact of<br>Volume Increases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty>0))$Volume_TM_impact),
    # 'Impact of<br>Volume Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty<=0))$Volume_TM_impact),
    # 'Lost Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
    # 'New Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Trading_Margin_2)
  )
  
  waterfall_incrdecr_data_dollar[] <- lapply(waterfall_incrdecr_data_dollar,round,0)
  
  
  # Calculate each component impact on net sales dollar
  net_sales_impact = c(
    # 'Prior Period<br>Trading Margin', 
    sum(tm_breakdown_table$Net_Sales_1),
    # 'Impact of<br>Price Increases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_sell>0 ))$Sell_sales_impact),
    # 'Impact of<br>Price Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_sell<=0 ))$Sell_sales_impact),
    # 'Impact of<br>COGS Decreases', 
    0,
    # 'Impact of<br>COGS Increases',
    0, 
    # 'Impact of<br>Supplier Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Claim_sales_impact),
    # 'Impact of<br>Promotional Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Promo_sales_impact),
    # 'Impact of<br>Customer Rebates',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Rebate_sales_impact),
    # 'Impact of<br>Volume Increases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty>0))$Volume_sales_impact),
    # 'Impact of<br>Volume Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty<=0))$Volume_sales_impact),
    # 'Lost Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$Change_in_netsales),
    # 'New Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$Change_in_netsales),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Net_Sales_2)
  )
  
  
  # Calculate each component impact on trading margin dollar
  tm_dollar_impact = c(
    # 'Prior Period<br>Trading Margin', 
    sum(tm_breakdown_table$Trading_Margin_1),
    # 'Impact of<br>Price Increases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_sell>0))$Sell_sales_impact),
    # 'Impact of<br>Price Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_sell<=0))$Sell_sales_impact),
    # 'Impact of<br>COGS Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_COGS_u<=0))$COGS_impact),
    # 'Impact of<br>COGS Increases',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_COGS_u>0))$COGS_impact),
    # 'Impact of<br>Supplier Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Claim_sales_impact),
    # 'Impact of<br>Promotional Claims',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Promo_sales_impact),
    # 'Impact of<br>Customer Rebates',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0))$Rebate_sales_impact),
    # 'Impact of<br>Volume Increases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty>0))$Volume_TM_impact),
    # 'Impact of<br>Volume Decreases', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty<=0))$Volume_TM_impact),
    # 'Lost Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
    # 'New Sales', 
    sum((tm_breakdown_table %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
    # 'Current Period<br>Trading Margin'
    sum(tm_breakdown_table$Trading_Margin_2)
  )
  
  
  # Calculate the TM percentage for each component impact on TM $ and Net Sales
  tm_percent_value = list()
  for (i in (1:length(tm_dollar_impact))) {
    tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[1:i]) / sum(net_sales_impact[1:i]) * 100))
  }
  
  tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[length(tm_dollar_impact)]) / sum(net_sales_impact[length(tm_dollar_impact)]) * 100))
  tm_percent_value <- unlist(tm_percent_value)
  
  
  # Initalise waterfall data with impact values for TM percentage
  waterfall_incrdecr_data_percent <- list(tm_percent_value[1])
  for (i in (2:length(tm_percent_value)-1)) {
    waterfall_incrdecr_data_percent <- append(waterfall_incrdecr_data_percent, tm_percent_value[i] - tm_percent_value[i-1])
  }
  
  waterfall_incrdecr_data_percent[length(waterfall_incrdecr_data_percent)] <-  tm_percent_value[length(tm_percent_value)]
  
  waterfall_incrdecr_data_percent
  
  
  waterfall_incrdecr_data_percent[] <- lapply(waterfall_incrdecr_data_percent,round,2)
  
  
  waterfall_data <- list(waterfall_incrdecr_data_percent = waterfall_incrdecr_data_percent
                         , waterfall_incrdecr_data_dollar = waterfall_incrdecr_data_dollar)
  
  return(waterfall_data)
  
}



get_waterfall_table <- function(tm_breakdown_table){
  
  # get the aggregation level you want to use 
  margin_agg_selector <- Margin_Agg_Selector
  
  # extract unique values based on user-selected aggregation level
  agg_values <- sort(unique(tm_breakdown_table[[margin_agg_selector]])) 
  
  
  # create a dataframe to store data for table
  waterfall_table_dollar <- data.frame(val                = character()
                                       , tm_1             = numeric()
                                       , pricing_impact   = numeric() 
                                       , vol_impact       = numeric()
                                       , lost_sales       = numeric()
                                       , new_sales        = numeric()
                                       , tm_2             = numeric()
                                       , stringsAsFactors = FALSE)
  
  
  # iterate through unique values to calculate data for display
  for (val in agg_values) {
    
    waterfall_data = list(
      # 'Prior Period <br>Trading Margin',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)))$Trading_Margin_1),
      # 'Existing Sales<br>Pricing Impact',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2!=0))$Price_TM_impact),
      # 'Existing Sales<br>Volume Impact',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2!=0))$Volume_TM_impact),
      # 'Lost Sales',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
      # 'New Sales',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
      # 'Current Period <br>Trading Margin'
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)))$Trading_Margin_2)
    )
    
    waterfall_data[] <- lapply(waterfall_data,round,0)
    
    tmp <- data.frame(val                 = val
                      , tm_1               = paste0("$", prettyNum(waterfall_data[[1]],big.mark=",",scientific=FALSE))
                      , pricing_impact     = paste0("$", prettyNum(waterfall_data[[2]],big.mark=",",scientific=FALSE))
                      , vol_impact         = paste0("$", prettyNum(waterfall_data[[3]],big.mark=",",scientific=FALSE))
                      , lost_sales         = paste0("$", prettyNum(waterfall_data[[4]],big.mark=",",scientific=FALSE))
                      , new_sales          = paste0("$", prettyNum(waterfall_data[[5]],big.mark=",",scientific=FALSE))
                      , tm_2               = paste0("$", prettyNum(waterfall_data[[6]],big.mark=",",scientific=FALSE))
                      , stringsAsFactors   = FALSE)
    
    
    rownames(tmp) <- NULL
    
    tmp
    
    
    waterfall_table_dollar <- waterfall_table_dollar %>% rbind(tmp)
    
  } 
  
  # create a dataframe to store data for table
  waterfall_table_percent <- data.frame(val               = character()
                                        , tm_1             = numeric()
                                        , pricing_impact   = numeric() 
                                        , vol_impact       = numeric()
                                        , lost_sales       = numeric()
                                        , new_sales        = numeric()
                                        , tm_2             = numeric()
                                        , stringsAsFactors = FALSE)
  
  # iterate through unique values to calculate data for display
  for (val in agg_values) {
    val = val
    
    # Calculate each component impact on net sales dollar
    net_sales_impact = c(# 'Prior Period<br>Trading Margin',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)))$Net_Sales_1),
      # 'Existing Sales<br>Pricing Impact',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2!=0))$Price_sales_impact),
      # 'Existing Sales<br>Volume Impact',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2!=0))$Volume_sales_impact),
      # 'Lost Sales',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2==0))$Change_in_netsales),
      # 'New Sales',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1==0, Qty_2!=0))$Change_in_netsales),
      # 'Current Period<br>Trading Margin'
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)))$Net_Sales_2))
    
    
    tm_dollar_impact = c(# 'Prior Period<br>Trading Margin',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)))$Trading_Margin_1),
      # 'Existing Sales<br>Pricing Impact',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2!=0))$Price_TM_impact),
      # 'Existing Sales<br>Volume Impact',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2!=0))$Volume_TM_impact),
      # 'Lost Sales',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1!=0, Qty_2==0))$TM_Change),
      # 'New Sales',
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)) %>% filter(Qty_1==0, Qty_2!=0))$TM_Change),
      # 'Current Period<br>Trading Margin'
      sum((tm_breakdown_table %>% filter(get(margin_agg_selector) == (val)))$Trading_Margin_2))
    
    
    tm_percent_value = list()
    
    # Calculate the TM percentage for each component impact on TM $ and Net Sales
    for (i in (1:length(tm_dollar_impact))) {
      tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[1:i]) / sum(net_sales_impact[1:i]) * 100))
    }
    tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[length(tm_dollar_impact)]) / sum(net_sales_impact[length(tm_dollar_impact)]) * 100))
    tm_percent_value <- unlist(tm_percent_value)
    
    
    
    # Initalise waterfall data with impact values for TM percentage
    waterfall_data <- list(tm_percent_value[1])
    for (i in (2:length(tm_percent_value)-1)) {
      waterfall_data <- append(waterfall_data, tm_percent_value[i] - tm_percent_value[i-1])
    }
    waterfall_data[length(waterfall_data)] <-  tm_percent_value[length(tm_percent_value)]
    
    
    waterfall_data[] <- lapply(waterfall_data,round,2)
    
    tmp <- data.frame(val                 = val
                      , tm_1               = paste0(prettyNum(waterfall_data[[1]],big.mark=",",scientific=FALSE),"%")
                      , pricing_impact     = paste0(prettyNum(waterfall_data[[2]],big.mark=",",scientific=FALSE),"%")
                      , vol_impact         = paste0(prettyNum(waterfall_data[[3]],big.mark=",",scientific=FALSE),"%")
                      , lost_sales         = paste0(prettyNum(waterfall_data[[4]],big.mark=",",scientific=FALSE),"%")
                      , new_sales          = paste0(prettyNum(waterfall_data[[5]],big.mark=",",scientific=FALSE),"%")
                      , tm_2               = paste0(prettyNum(waterfall_data[[6]],big.mark=",",scientific=FALSE),"%")
                      , stringsAsFactors   = FALSE)
    
    rownames(tmp) <- NULL
    
    tmp
    
    waterfall_table_percent <- waterfall_table_percent %>% rbind(tmp)
    
  } 
  
  waterfall_table <- list(waterfall_table_percent = waterfall_table_percent
                          , waterfall_table_dollar = waterfall_table_dollar)
  
  return(waterfall_table)
  
}



get_pricingreasons_data <- function(tm_breakdown_table) {
  
  
  pricingreasons_waterfall_data_dollar = list(
    # 0 Prior Period<br>Existing Basket<br>Trading Margin  
    sum((tm_breakdown_table %>% filter(Qty_1!=0 & Qty_2!=0))$Trading_Margin_1),
    # 3 'Impact of<br>Selling Price<br>Increase Greater<br>Than Cost <br>Increase',
    sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Incr_Greater"))$Price_TM_impact),
    # 4 'Impact of<br>Cost Price<br>Decrease Not <br>Passed On', 
    sum((tm_breakdown_table %>% filter(Pricing_Reason=="Cost_Decr_Greater"))$Price_TM_impact),
    # 5 'Impact of<br>Selling Price<br>Increase Only', 
    sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Incr"))$Price_TM_impact),
    # 6 'Impact of<br>Selling Price<br>Decrease Only',
    sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Decr"))$Price_TM_impact),
    # 7 'Impact of<br>Cost Price<br>Increase Not <br>Passed On', 
    sum((tm_breakdown_table %>% filter(Pricing_Reason=="Cost_Incr_Greater"))$Price_TM_impact),
    # 8 'Impact of<br>Selling Price<br>Decrease Greater<br>Than Cost <br>Price Decrease', 
    sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Decr_Greater"))$Price_TM_impact),
    # 1 'Impact of<br>Volume Increase',
    sum((tm_breakdown_table %>% filter(Qty_1 != 0, Qty_2 != 0, Qty_1 <= Qty_2))$Volume_TM_impact),
    # 2 'Impact of<br>Volume Decrease',
    sum((tm_breakdown_table %>% filter(Qty_1 != 0, Qty_2 != 0, Qty_1 > Qty_2))$Volume_TM_impact),
    # 9 Current Period<br>Existing Basket<br>Trading Margin  
    sum((tm_breakdown_table %>% filter(Qty_1!=0 & Qty_2!=0))$Trading_Margin_2)
  )
  
  pricingreasons_waterfall_data_dollar[] <- lapply(pricingreasons_waterfall_data_dollar,round,2)
  
  
  
  # Calculate each component impact on net sales dollar
  net_sales_impact = c(# 0 Prior Period<br>Existing Basket<br>Trading Margin  
    sum((tm_breakdown_table %>% filter(Qty_1!=0 & Qty_2!=0))$Net_Sales_1),
    # 3 'Impact of<br>Selling Price<br>Increase Greater<br>Than Cost <br>Increase',
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Incr_Greater"))$Price_sales_impact)),
    # 4 'Impact of<br>Cost Price<br>Decrease Not <br>Passed On', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Cost_Decr_Greater"))$Price_sales_impact)),
    # 5 'Impact of<br>Selling Price<br>Increase Only', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Incr"))$Price_sales_impact)),
    # 6 'Impact of<br>Selling Price<br>Decrease Only',
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Decr"))$Price_sales_impact)),
    # 7 'Impact of<br>Cost Price<br>Increase Not <br>Passed On', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Cost_Incr_Greater"))$Price_sales_impact)),
    # 8 'Impact of<br>Selling Price<br>Decrease Greater<br>Than Cost <br>Price Decrease', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Decr_Greater"))$Price_sales_impact)),
    # 1 'Impact of<br>Volume Increase',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty>0))$Volume_sales_impact),
    # 2 'Impact of<br>Volume Decrease',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty<=0))$Volume_sales_impact),
    # 9 Current Period<br>Existing Basket<br>Trading Margin  
    sum((tm_breakdown_table %>% filter(Qty_1!=0 & Qty_2!=0))$Net_Sales_2))
  
  tm_dollar_impact = c(# 0 Prior Period<br>Existing Basket<br>Trading Margin  
    sum((tm_breakdown_table %>% filter(Qty_1!=0 & Qty_2!=0))$Trading_Margin_1),
    # 3 'Impact of<br>Selling Price<br>Increase Greater<br>Than Cost <br>Increase',
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Incr_Greater"))$Price_TM_impact)),
    # 4 'Impact of<br>Cost Price<br>Decrease Not <br>Passed On', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Cost_Decr_Greater"))$Price_TM_impact)),
    # 5 'Impact of<br>Selling Price<br>Increase Only', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Incr"))$Price_TM_impact)),
    # 6 'Impact of<br>Selling Price<br>Decrease Only',
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Decr"))$Price_TM_impact)),
    # 7 'Impact of<br>Cost Price<br>Increase Not <br>Passed On', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Cost_Incr_Greater"))$Price_TM_impact)),
    # 8 'Impact of<br>Selling Price<br>Decrease Greater<br>Than Cost <br>Price Decrease', 
    (sum((tm_breakdown_table %>% filter(Pricing_Reason=="Price_Decr_Greater"))$Price_TM_impact)),
    # 1 'Impact of<br>Volume Increase',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty>0))$Volume_TM_impact),
    # 2 'Impact of<br>Volume Decrease',
    sum((tm_breakdown_table %>% filter(Qty_1!=0, Qty_2!=0, Change_in_qty<=0))$Volume_TM_impact),
    # 9 Current Period<br>Existing Basket<br>Trading Margin  
    sum((tm_breakdown_table %>% filter(Qty_1!=0 & Qty_2!=0))$Trading_Margin_2))
  
  # Calculate the TM percentage for each component impact on TM $ and Net Sales
  tm_percent_value = list()
  for (i in (1:length(tm_dollar_impact))) {
    tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[1:i]) / sum(net_sales_impact[1:i]) * 100))
  }
  
  tm_percent_value <- append(tm_percent_value, (sum(tm_dollar_impact[length(tm_dollar_impact)]) / sum(net_sales_impact[length(tm_dollar_impact)]) * 100))
  tm_percent_value <- unlist(tm_percent_value)
  
  
  # Initalise waterfall data with impact values for TM percentage
  pricingreasons_waterfall_data_percent <- list(tm_percent_value[1])
  for (i in (2:length(tm_percent_value)-1)) {
    pricingreasons_waterfall_data_percent <- append(pricingreasons_waterfall_data_percent, tm_percent_value[i] - tm_percent_value[i-1])
  }
  
  pricingreasons_waterfall_data_percent[length(pricingreasons_waterfall_data_percent)] <-  tm_percent_value[length(tm_percent_value)]
  
  
  pricingreasons_waterfall_data_percent[] <- lapply(pricingreasons_waterfall_data_percent,round,2)
  
  
  pricingreasons_data <- list(pricingreasons_waterfall_data_percent = pricingreasons_waterfall_data_percent
                              , pricingreasons_waterfall_data_dollar = pricingreasons_waterfall_data_dollar)
  
  return(pricingreasons_data)

  
}



get_breakdownbar_data <- function(tm_breakdown_table) {
  
  tm_breakdown_table <- tm_breakdown_table %>%
    filter(Item_Description != "UNK")
  
  # inputs obtained from visual named breakdown_sortby_selector
  breakdown_sortby_selector_options <- c("TM_Percent_Change"
                                         ,"TM_Change"
                                         ,"Price_TM_impact"
                                         ,"Volume_TM_impact"
                                         ,"Change_in_net_selling_price"
                                         ,"Change_in_net_cost_price"
                                         ,"Change_in_netsales"
                                         ,"Change_in_qty"
                                         ,"TM_Percent_Change")

  pricing_reasons_list <- c("Price_Incr_Greater"
                            ,"Cost_Decr_Greater"
                            ,"Price_Incr"
                            ,"No_Change"
                            ,"Equal"
                            ,"Price_Decr"
                            ,"Cost_Incr_Greater"
                            ,"Price_Decr_Greater")
    
  # obtain the top and bottom 10 rows from a dataframe
  get_topbot <- function(df) {
    topbot <- df %>% 
      slice_head(n = 10) %>% 
      rbind(df %>% slice_tail(n = 10))
    
    return(topbot)
  }
  

  breakdownbar_data <- data.frame()
  
  reasons_data <- data.frame()
  
  # obtain top and bottom values for all combinations of Breakdown_SortBy_Selector and Breakdown_Component_Selector
  for (i in breakdown_sortby_selector_options) {
    
    # all_sales
    all_sales <- tm_breakdown_table %>% 
      arrange(breakdown_sortby_selector_options[i]) %>% 
      get_topbot()
    
    # lost_sales
    lost_sales <- tm_breakdown_table %>%
      filter(Qty_1!=0, Qty_2!=0) %>% 
      arrange(breakdown_sortby_selector_options[i]) %>% 
      get_topbot()
    
    # new_sales
    new_sales <- tm_breakdown_table %>%
      filter(Qty_1==0 & Qty_2!=0) %>% 
      arrange(breakdown_sortby_selector_options[i]) %>% 
      get_topbot()
    
    # existing_sales
    existing_sales <- tm_breakdown_table %>%
      filter(Qty_1!=0 & Qty_2==0) %>% 
      arrange(breakdown_sortby_selector_options[i]) %>% 
      get_topbot()
    

    
    df <- all_sales %>% 
      rbind(lost_sales) %>% 
      rbind(new_sales) %>% 
      rbind(existing_sales)
    
    
    breakdownbar_data <- breakdownbar_data %>% 
      rbind(df)
    
  }
  
  breakdownbar_data <- distinct(breakdownbar_data) 
  
  
  return(breakdownbar_data)
  
}







