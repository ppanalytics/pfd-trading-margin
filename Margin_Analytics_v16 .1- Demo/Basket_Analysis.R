#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                      BASKET ANALYSIS TAB FUCNTIONS                      #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


#############################- DATE_RANGE_FILTER() FUNCTION-##############################

# Function for filtering for desired date range
date_range_filter <- function(df, time1, time2){ 
  df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
  return(df1)
}


#############################- PRE PROECESSING FOR BASKET ANALYSIS TAB-##############################

# Adapted from pricing_reasons() function
# Pre-prcoessing data for basket analysis tab
customer_level_preprocessing <- function(){

    start_time <- Sys.time()

    # Update progress bar
    upload_progress <- round(0,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Get customer from user 
    basket_analysis_customer <- Basket_Analysis_Customer
    # Get the filter level from the user
    customer_level_grouping <- Customer_Level_Grouping

    # Filter based on user selection or no filter
    if (basket_analysis_customer=='All'){
        # customer_group = Customer_Name_Master
        customer_group <- as.character((Customer_Name_Master%>%rbind("UNK"))$Customer_Name)
    } else {
        customer_group <- basket_analysis_customer
    }

    #Calculate trading margin
    calculate_TM <- function(df){
        df1 <- df %>% 
        # Filter by user-selected choice
        filter(Customer_Name %in% customer_group) %>%
        # Group by user-selected choice
        group_by_at(c(customer_level_grouping)) %>% 
        summarise(Qty = sum(Qty),
                    Sales_Inc_GST = sum(Sales_Inc_GST),
                    COGS = sum(COGS),
                    Customer_Rebate = sum(Customer_Rebate),
                    Sales_Claims = sum(Sales_Claims), 
                    Promo_Claims = sum(Promo_Claims)) %>% 
        mutate(Selling_Price = ifelse(Qty != 0, (Sales_Inc_GST) / Qty, 0),
                COGS_u = COGS / Qty,
                Net_Sales = Sales_Inc_GST + Sales_Claims + Promo_Claims + Customer_Rebate,
                Net_Selling_Price = ifelse(Qty != 0, (Sales_Inc_GST + Sales_Claims + Promo_Claims + Customer_Rebate) / Qty, 0),
                Net_Cost_Price = COGS / Qty,
                Trading_Margin = Sales_Inc_GST - COGS + Customer_Rebate + Sales_Claims + Promo_Claims) %>%
        mutate_if(is.numeric, ~round(., 2)) %>%
        mutate(Net_Sales = round(Net_Sales, 0),
                Trading_Margin = round(Trading_Margin, 0),
                Qty = round(Qty, 0)) %>%
        mutate(TM_Percent = case_when(abs(Qty) == 0 ~ 0,
                                        abs(Net_Sales) == 0 ~ 0,
                                        round(Trading_Margin, 0) == 0 ~ 0, 
                                        TRUE ~ Trading_Margin / Net_Sales * 100)) %>%
        mutate_if(is.numeric, ~round(., 2))
        return(df1)
    }

    # Update progress bar
    upload_progress <- round(5,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Get summarised data tables for both time periods
    tm_test_1_item <- calculate_TM(tm_test_1)
    
    # Error handling for incorrect customer selection 
    if (nrow(tm_test_1_item) == 0) { 
        rpgm.notification("error", "Warning! Customer selection is incorrect. Please select a valid customer for the period.", duration = 5000) 
        opt <- options(show.error.messages = FALSE)
        on.exit(options(opt))
        stop()
    } 

    # Update progress bar
    upload_progress <- round(10,0)
    gui.setValue("this","Basket_Progress",upload_progress)

    # Get summarised data tables for both time periods
    tm_test_2_item <- calculate_TM(tm_test_2)

    # Error handling for incorrect customer selection 
    if (nrow(tm_test_2_item) == 0) { 
        rpgm.notification("error", "Warning! Customer selection is incorrect. Please select a valid customer for the period.", duration = 5000) 
        opt <- options(show.error.messages = FALSE)
        on.exit(options(opt))
        stop()    
    } 
    # Update progress bar
    upload_progress <- round(20,0)
    gui.setValue("this","Basket_Progress",upload_progress)

    # Convert spark dataframe into R
    tm_test_1_item <- collect(tm_test_1_item)

    # Update progress bar
    upload_progress <- round(50,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    tm_test_2_item <- collect(tm_test_2_item)

    # Update progress bar
    upload_progress <- round(80,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    compare_periods <- function(df1, df2){
        tm <- df1 %>% 
        full_join(df2, 
                    by=unique(c(customer_level_grouping)),
                    suffix=c("_1","_2")) %>%
        ungroup() %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(Change_in_cost =  COGS_2 - COGS_1,
                Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
                Change_in_sell = Selling_Price_2 - Selling_Price_1,
                Change_in_COGS_u = COGS_u_2 - COGS_u_1,
                Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
                Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
                Change_in_promo = Promo_Claims_2 - Promo_Claims_1,
                Change_in_qty = Qty_2 - Qty_1,
                Change_in_netsales = Net_Sales_2 - Net_Sales_1,
                TM_Change = Trading_Margin_2 - Trading_Margin_1,
                TM_Percent_Change = TM_Percent_2 - TM_Percent_1) %>% 
        mutate(Change_in_net_selling_price = Net_Selling_Price_2 - Net_Selling_Price_1,
               Change_in_net_cost_price = Net_Cost_Price_2 - Net_Cost_Price_1) %>% 
        # Total change in net selling price and net cost price 
        mutate(Total_Sell_Cost_eff =  Change_in_net_selling_price - Change_in_net_cost_price) %>% 
        # Total price impact on net sales and TM dollar
        mutate(Price_sales_impact = Change_in_net_selling_price * Qty_2,
                Price_TM_impact = Total_Sell_Cost_eff * Qty_2,
                # Total volume impact on net sales and TM dollar
                Volume_sales_impact = Change_in_qty * Net_Selling_Price_1,
                Volume_TM_impact = Change_in_qty * (Net_Selling_Price_1 - Net_Cost_Price_1)) %>%
        mutate(Price_NetSales_eff = Change_in_sell * Qty_2,
                Qty_NetSales_eff = Change_in_qty * Net_Selling_Price_1) %>% 
        mutate(Sell_sales_impact = Change_in_sell * Qty_2,
                COGS_impact = -Change_in_net_cost_price * Qty_2,
                Claim_sales_impact = (Sales_Claims_2/Qty_2 - Sales_Claims_1/Qty_1) * Qty_2,
                Promo_sales_impact = (Customer_Rebate_2/Qty_2 - Customer_Rebate_1/Qty_1) * Qty_2,
                Rebate_sales_impact = (Promo_Claims_2/Qty_2 - Promo_Claims_1/Qty_1) * Qty_2) %>%
        filter(!(Qty_1==0 & Qty_2==0)) %>%
        mutate_if(is.numeric, ~round(., 2)) %>% 
        arrange(Change_in_qty)
      return(tm)
    }

    df_drilldown_comparison <- compare_periods(tm_test_1_item, tm_test_2_item) %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
        mutate_if(is.numeric, ~replace(., is.nan(.), 0)) %>%
        mutate_if(is.numeric, ~round(., 2))

    # Update progress bar
    upload_progress <- round(90,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Assigning reason for change in trading margin impact for each row
    customer_level_table <- df_drilldown_comparison %>% 
        mutate(Pricing_Reason = case_when((Qty_1!=0 & Qty_2!=0 &  
                                            Change_in_net_selling_price>0 & Change_in_net_cost_price>0 & 
                                            Change_in_net_selling_price>Change_in_net_cost_price)          
                                                                                                    ~ "Price_Incr_Greater",
                                        (Qty_1!=0 &  Qty_2!=0 &  
                                            Change_in_net_selling_price>0 & Change_in_net_cost_price==0)   
                                                                                                    ~ "Price_Incr",
                                        (Qty_1!=0 & Qty_2!=0 & 
                                            Change_in_net_cost_price<0 & 
                                            Change_in_net_cost_price<Change_in_net_selling_price)          
                                                                                                    ~ "Cost_Decr_Greater",
                                        (Qty_1!=0 & Qty_2!=0 &  
                                            Change_in_net_selling_price==0 & Change_in_net_cost_price==0)  
                                                                                                    ~ "No_Change",
                                        (Qty_1!=0 & Qty_2!=0 &  
                                            Change_in_net_selling_price==Change_in_net_cost_price)  
                                                                                                    ~ "Equal",
                                        (Qty_1!=0 &  Qty_2!=0 &  
                                            Change_in_net_selling_price<0 & Change_in_net_cost_price==0)   
                                                                                                    ~ "Price_Decr",
                                        (Qty_1!=0 & Qty_2!=0 & 
                                            Change_in_net_cost_price>0 & 
                                            Change_in_net_cost_price>Change_in_net_selling_price)          
                                                                                                    ~ "Cost_Incr_Greater",
                                        (Qty_1!=0 & Qty_2!=0 &  
                                            Change_in_net_selling_price<0 & Change_in_net_cost_price<0 & 
                                            Change_in_net_cost_price>Change_in_net_selling_price)
                                                                                                    ~ "Price_Decr_Greater",
                                        (Qty_2==0) 
                                                                                                    ~ "Lost_Sales",
                                        (Qty_1==0)
                                                                                                    ~ "New_Sales"))

    assign("customer_level_table", customer_level_table, envir = .GlobalEnv)

    # Update progress bar
    upload_progress <- round(100,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Time taken for function assigned to global environment
    end_time <- Sys.time()
    time_taken <- end_time - start_time
    assign("basket_analysis_time", time_taken, envir = .GlobalEnv)

}


get_basket_filters <- function() {

    # Obtain unique customer names from user-selected periods
    Customer_Name_1 <- tm_test_1 %>% select(Customer_Name) %>% distinct() %>% collect() 
    Customer_Name_2 <- tm_test_2 %>% select(Customer_Name) %>% distinct() %>% collect() 
    Customer_Name_Master <- Customer_Name_1 %>%
        rbind(Customer_Name_2) %>%
        distinct(Customer_Name) %>%
        arrange(Customer_Name)

    Customer_Name_List <- Customer_Name_Master

    # Set choices for customer selector 
    gui.clearChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer")
    gui.addChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)     

    # BASKET FILTERS ###

    # On first load of dashboard, assign filter variables as 'All'
    if (counter_2 == 0){
        state_basket_filter_selector <- 'All'
        sitename_basket_filter_selector <- 'All'
        accmanager_basket_filter_selector <- 'All'
    assign("counter_2", 1, envir = .GlobalEnv) 
    } else {
        state_basket_filter_selector <- State_Basket_Filter_Selector
        sitename_basket_filter_selector <- SiteName_Basket_Filter_Selector
        accmanager_basket_filter_selector <- AccManager_Basket_Filter_Selector
    }
        # If only 'All' is selected or no options selected, update selection to 'All', otherwise automatically de-select the 'All' option
    if (length(State_Basket_Filter_Selector) == 0) {
        state_basket_filter_selector <- 'All'
    } else if (State_Basket_Filter_Selector[1] == as.character("All") & length(State_Basket_Filter_Selector) == 1) {
        state_basket_filter_selector <- 'All'
    } else {
        state_basket_filter_selector <- str_remove(state_basket_filter_selector, "All")
        gui.setValue("this","State_Basket_Filter_Selector", state_basket_filter_selector) 
    }

    if (length(SiteName_Basket_Filter_Selector) == 0) {
        sitename_basket_filter_selector <- 'All'
    } else if (SiteName_Basket_Filter_Selector[1] == as.character("All") & length(SiteName_Basket_Filter_Selector) == 1) {
        sitename_basket_filter_selector <- 'All'
    } else {
        sitename_basket_filter_selector <- str_remove(sitename_basket_filter_selector, "All")
        gui.setValue("this","SiteName_Basket_Filter_Selector", sitename_basket_filter_selector) 
    }

    if (length(AccManager_Basket_Filter_Selector) == 0) {
        accmanager_basket_filter_selector <- 'All'
    } else if (AccManager_Basket_Filter_Selector[1] == as.character("All") & length(AccManager_Basket_Filter_Selector) == 1) {
        accmanager_basket_filter_selector <- 'All'
    } else {
        accmanager_basket_filter_selector <- str_remove(accmanager_basket_filter_selector, "All")
        gui.setValue("this","AccManager_Basket_Filter_Selector", accmanager_basket_filter_selector) 

    }

    assign("state_basket_filter_selector", state_basket_filter_selector, envir = .GlobalEnv)
    assign("sitename_basket_filter_selector", sitename_basket_filter_selector, envir = .GlobalEnv)
    assign("AccManager_Basket_Filter_Selector", accmanager_basket_filter_selector, envir = .GlobalEnv)
}


# Dynamically updates the filter options based on previous user-selection
update_basket_filters <- function() {

    TM_Base_Filters <- TM_Base_Filters

    # If All is selected, update the list to reflect all items
    if (length(State_Basket_Filter_Selector) == 0) {
        state_basket_filter_selector <- (TM_Base_Filters %>% select(Site_State) %>% distinct())$Site_State
    } else if (State_Basket_Filter_Selector[1] == as.character("All") & length(State_Basket_Filter_Selector) == 1) {
        state_basket_filter_selector <- (TM_Base_Filters %>% select(Site_State) %>% distinct())$Site_State
    } else {

    }

    if (length(SiteName_Basket_Filter_Selector) == 0) {
        sitename_basket_filter_selector <- (TM_Base_Filters %>% select(Site_Name) %>% distinct())$Site_Name
    } else if (SiteName_Basket_Filter_Selector[1] == as.character("All") & length(SiteName_Basket_Filter_Selector) == 1) {
        sitename_basket_filter_selector <- (TM_Base_Filters %>% select(Site_Name) %>% distinct())$Site_Name
    } else {
       sitename_basket_filter_selector <- SiteName_Basket_Filter_Selector
    }

    if (length(AccManager_Basket_Filter_Selector) == 0) {
        accmanager_basket_filter_selector <- (TM_Base_Filters %>% select(Account_Manager) %>% distinct())$Account_Manager
    } else if (AccManager_Basket_Filter_Selector[1] == as.character("All") & length(AccManager_Basket_Filter_Selector) == 1) {
        accmanager_basket_filter_selector <- (TM_Base_Filters %>% select(Account_Manager) %>% distinct())$Account_Manager
    } else {
       accmanager_basket_filter_selector <- AccManager_Basket_Filter_Selector
    }

    # Filter dataframe based on user-selected groups
    TM_Base_Filters <- TM_Base_Filters %>%
        filter(Site_State %in% state_basket_filter_selector, 
                Site_Name %in% sitename_basket_filter_selector,
                Account_Manager %in% accmanager_basket_filter_selector)

    # Pre-populate filters
    Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% 
        rbind(TM_Base_Filters %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
    Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% 
        rbind(TM_Base_Filters %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name))
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% 
        rbind(TM_Base_Filters %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% 
        rbind(TM_Base_Filters %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))

    Customer_Name_List <- (TM_Base_Filters %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))

    # Set choices for customer selector 
    gui.clearChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer")
    gui.addChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)  

    # Update filter GUI based on user selection
    gui.clearChoices("this","State_Basket_Filter_Selector")
    gui.addChoices("this","State_Basket_Filter_Selector",Site_State_List$Site_State, Site_State_List$Site_State)
    gui.setChoiceText("this","State_Basket_Filter_Selector",Site_State_List[1,"Site_State"],Site_State_List[1,"Site_State"] ) 

    gui.clearChoices("this","SiteName_Basket_Filter_Selector")
    gui.addChoices("this","SiteName_Basket_Filter_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)
    gui.setChoiceText("this","SiteName_Basket_Filter_Selector",Site_Name_List[1,"Site_Name"],Site_Name_List[1,"Site_Name"] ) 

    gui.clearChoices("this","AccManager_Basket_Filter_Selector")
    gui.addChoices("this","AccManager_Basket_Filter_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)
    gui.setChoiceText("this","AccManager_Basket_Filter_Selector",Account_Manager_List[1,"Account_Manager"],Account_Manager_List[1,"Account_Manager"] ) 

}

# Resets all filters
reset_basket_filters <- function() {

    
    # Obtain unique customer names from user-selected periods
    Customer_Name_1 <- tm_test_1 %>% select(Customer_Name) %>% distinct() %>% collect() 
    Customer_Name_2 <- tm_test_2 %>% select(Customer_Name) %>% distinct() %>% collect() 
    Customer_Name_Master <- Customer_Name_1 %>%
        rbind(Customer_Name_2) %>%
        distinct(Customer_Name) %>%
        arrange(Customer_Name)

    Customer_Name_List <- Customer_Name_Master

    # Set choices for customer selector 
    gui.clearChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer")
    gui.addChoices(rpgm.step("main", "TMA"),"Basket_Analysis_Customer",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)     

    # Add all as selection choice in list
    Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% rbind(Site_State_Master)
    Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% rbind(Site_Name_Master)
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% rbind(Account_Manager_Master)

    gui.clearChoices(rpgm.step("main", "TMA"),"State_Basket_Filter_Selector")
    gui.addChoices(rpgm.step("main", "TMA"),"State_Basket_Filter_Selector",Site_State_List$Site_State, Site_State_List$Site_State)

    gui.clearChoices(rpgm.step("main", "TMA"),"SiteName_Basket_Filter_Selector")
    gui.addChoices(rpgm.step("main", "TMA"),"SiteName_Basket_Filter_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)

    gui.clearChoices(rpgm.step("main", "TMA"),"AccManager_Basket_Filter_Selector")
    gui.addChoices(rpgm.step("main", "TMA"),"AccManager_Basket_Filter_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)

    gui.setValue("this","State_Basket_Filter_Selector", "All") 
    gui.setValue("this","SiteName_Basket_Filter_Selector", "All") 
    gui.setValue("this","AccManager_Basket_Filter_Selector", "All") 
    
    state_basket_filter_selector <- 'All'
    sitename_basket_filter_selector <- 'All'
    accmanager_basket_filter_selector <- 'All'



    assign("state_basket_filter_selector", state_basket_filter_selector, envir = .GlobalEnv)
    assign("sitename_basket_filter_selector", sitename_basket_filter_selector, envir = .GlobalEnv)
    assign("accmanager_basket_filter_selector", accmanager_basket_filter_selector, envir = .GlobalEnv)


}


# Save underlying data table to user-input location
export_basket_underlying_data <- function() {

    basket_underlying_data_name <- Basket_Underlying_Data_Name
    file_name <- paste0("/", Basket_Underlying_Data_Name, ".csv")

    # Dashboard Logo
    if (rpgm.isServer() == TRUE) {
        file_location <- c(rpgm.output)
    } else {
        file_location <- c("C:\\Users\\gaov\\Downloads\\")
    }

    write_csv(customer_level_table, paste0(file_location, file_name))

    
    if(file.exists(paste0(file_location, file_name)) == TRUE) {
        rpgm.notification("success", "Data successfully exported.", duration = 5000)
    } else {
        rpgm.notification("error", "Error exporting data.", duration = 5000)
    }

    rpgm.open(paste0(file_location, file_name)) 

    print(paste0(file_location, file_name))

}