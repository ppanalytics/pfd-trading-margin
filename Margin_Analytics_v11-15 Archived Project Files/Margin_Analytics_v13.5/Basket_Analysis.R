#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#            BASKET ANALYSIS TAB FUCNTIONS, VISUALS AND GRAPHS            #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


#############################- DATE_RANGE_FILTER() FUNCTION-##############################

# Function for filtering for desired date range
date_range_filter <- function(df, time1, time2){ 
  df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
  return(df1)
}


#############################- PRE PROECESSING FOR BASKET ANALYSIS TAB-##############################

# Adapted from pricing_reasons() function
# Pre-prcoessing data for basket analysis tab
basket_preprocessing <- function(){

    start_time <- Sys.time()

    # Update progress bar
    upload_progress <- round(0,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Get the filter level from the user
    basket_analysis_grouping <- Basket_Analysis_Grouping

    # Calculate trading margin
    calculate_TM <- function(df){
        df1 <- df %>% 
        # Group by user-selected choice
        group_by_at(c(basket_analysis_grouping)) %>% 
        summarise(Qty=sum(Qty),
                    Sales_Inc_GST=sum(Sales_Inc_GST),
                    COGS=sum(COGS),
                    Customer_Rebate=sum(Customer_Rebate),
                    Sales_Claims=sum(Sales_Claims), 
                    Promo_Claims=sum(Promo_Claims)) %>% 
        mutate(Selling_Price = (Sales_Inc_GST)/Qty,
                COGS_u = COGS/Qty,
                Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
                Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
                TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
                TM_percent = ifelse(is.na(TM_percent), 0, TM_percent),
                # Additional formula from excel sheet "Customer Example" provided by PFD
                Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
                Net_Cost_Price = COGS/Qty)
        return(df1)
    }


    # Update progress bar
    upload_progress <- round(5,0)
    gui.setValue("this","Margin_Breakdown_Progress",upload_progress)  

    # Get summarised data tables for both time periods
    tm_test_1_item <- calculate_TM(tm_test_1)

    # Update progress bar
    upload_progress <- round(10,0)
    gui.setValue("this","Basket_Progress",upload_progress)

    # Get summarised data tables for both time periods
    tm_test_2_item <- calculate_TM(tm_test_2)

    # Update progress bar
    upload_progress <- round(20,0)
    gui.setValue("this","Basket_Progress",upload_progress)

    # Convert spark dataframe into R
    tm_test_1_item <- collect(tm_test_1_item)

    # Update progress bar
    upload_progress <- round(50,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    tm_test_2_item <- collect(tm_test_2_item)

    # Update progress bar
    upload_progress <- round(80,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    compare_periods <- function(df1, df2){
        tm <- df1 %>% 
        full_join(df2, 
                    by=unique(c(basket_analysis_grouping)),
                    suffix=c("_1","_2")) %>%
        ungroup() %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(Change_in_cost =  COGS_2 - COGS_1,
                Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
                Change_in_sell = Selling_Price_2 - Selling_Price_1,
                Change_in_COGS_u = COGS_u_2 - COGS_u_1,
                Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
                Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
                Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
                Change_in_qty = Qty_2 - Qty_1,
                TM_change = Trading_Margin_2-Trading_Margin_1,
                TM_percent_change = TM_percent_2 - TM_percent_1,
                #  Additional formula from excel sheet "Customer Example" provided by PFD
                Change_in_net_selling_price = Net_Selling_Price_2 - Net_Selling_Price_1, 
                Change_in_net_cost_price = Net_Cost_Price_2 - Net_Cost_Price_1) %>% 
        # Additional formula from excel sheet "Customer Example" provided by PFD
        # Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
        # mutate(Net_Selling_Price_eff = (Net_Selling_Price_2/Qty_2)-(Net_Selling_Price_1/Qty_1),
        mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
                Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>%
        mutate(Net_TM_impact = (Net_Selling_Price_eff - Net_Cost_Price_eff)*Qty_2) %>%
        # mutate(Price_eff = Change_in_sell*Qty_1,
        #      COGS_eff = -Change_in_COGS_u*Qty_1,
        #      Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
        #      Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2),
        # filter(!(Qty_1==0 & Qty_2==0)) %>%
        arrange(Change_in_qty)
        return(tm)
    }

    df_drilldown_comparison <- compare_periods(tm_test_1_item, tm_test_2_item) %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
        mutate_if(is.numeric, ~round(., 2)) 

    # Update progress bar
    upload_progress <- round(90,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    assign("basket_analysis_table", df_drilldown_comparison, envir = .GlobalEnv)

    # Update progress bar
    upload_progress <- round(100,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Time taken for function assigned to global environment
    end_time <- Sys.time()
    time_taken <- end_time - start_time
    assign("basket_analysis_time", time_taken, envir = .GlobalEnv)

}

#############################- BASKET ANALYSIS TAB VISUALS  -##############################

updateNewBasketVisuals <- function(){

    # Updated basket analysis bar chart 
    gui.setValue("this", "MyGraph38", generategraph38())
    gui.setValue("this", "MyGraph39", generategraph39())

    # Progress bar update
    uploadProgressBar1 <- function(){
        upload_progress <- round(100,0)
        gui.setValue("this","Upload_progess_1",upload_progress)
    }

    uploadProgressBar1()

}

# Update graph38 basket analysis bar chart
updateBasketVisuals_Bar <- function(){
    # Basket analysis bar chart
    gui.setValue("this", "MyGraph38", generategraph38())
    gui.setValue("this", "MyGraph39", generategraph39())

}

#############################- GRAPHS FOR BASKET ANALYSIS TAB -##############################

# Bar chart of highest contributers to trading margin change 
generategraph38 <- function(){

    basket_analysis_table <- basket_analysis_table
    basket_analysis_grouping <- Basket_Analysis_Grouping

    # Filter for only rows that are from a basket change
    basket_analysis_table <- basket_analysis_table %>% 
        filter((Qty_1==0 & Qty_2!=0) | (Qty_1!= 0 & Qty_2==0))

    #### PRE-PROCESSING TO GROUP DATA DYNAMICALLY BY USER SELECTION #####
    basket_analysis_table <- basket_analysis_table %>% 
        # Group items by selector
        group_by_at(c(Basket_Analysis_Grouping)) %>% 
        # Summarize relevant values
        summarise(Qty_1 = sum(Qty_1),
                    Qty_2 = sum(Qty_2),
                    Sales_Inc_GST_1 = sum(Sales_Inc_GST_1),
                    Sales_Inc_GST_2 = sum(Sales_Inc_GST_2),
                    # Original Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate
                    Net_Sales_1 = sum(Net_Sales_1),
                    Net_Sales_2 = sum(Net_Sales_2),
                    # Original Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims
                    Trading_Margin_1 = sum(Trading_Margin_1),
                    Trading_Margin_2 = sum(Trading_Margin_2),
                    # Original Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty
                    Net_Selling_Price_1 = sum(Net_Selling_Price_1),
                    Net_Selling_Price_2 = sum(Net_Selling_Price_2),
                    # Original Net_Cost_Price = COGS/Qty
                    Net_Cost_Price_1  = sum(Net_Cost_Price_1),
                    Net_Cost_Price_2  = sum(Net_Cost_Price_2)) %>%
        # Original TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
        mutate(TM_percent_1 = Trading_Margin_1/Net_Sales_1*100,
                TM_percent_2 = Trading_Margin_2/Net_Sales_2*100) %>%
        mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
                Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>%
        mutate(Net_TM_impact = (Net_Selling_Price_eff - Net_Cost_Price_eff)*Qty_2) %>%
        mutate(TM_Dollar_Change = Trading_Margin_2 - Trading_Margin_1,
                TM_Percent_Change = TM_percent_2-TM_percent_1) %>%
        mutate(Qty_Change = Qty_2 - Qty_1) %>%
        # Filter out rows where TM impact is not zero
        filter(Net_TM_impact != 0)
    
    # Adding a conditional formatting colour row for bar
    basket_analysis_table <- basket_analysis_table %>%
        mutate(Bar_Colour = ifelse(Net_TM_impact>0, 'rgb(0,0,142)', 'rgb(142,0,0)'))

    basketbar_ascdesc_selector <- BasketBar_AscDesc_Selector
    if (basketbar_ascdesc_selector == "descending") {
        basket_analysis_table <- basket_analysis_table %>%
            arrange(desc(Net_TM_impact))
    } else {
        basket_analysis_table <- basket_analysis_table %>%
            arrange(Net_TM_impact)
    }
    
    # Dyanamically chooses number of bars to be shown based on user selection
    basketbar_shownum_selector <- BasketBar_ShowNum_Selector
    if (basketbar_shownum_selector == "show10") {
        xData_Selector <- head((basket_analysis_table), n=10)[[c(Basket_Analysis_Grouping)]]
        yData_Selector <- head((basket_analysis_table), n=10)$Net_TM_impact
        Bar_Colour <- as.list(head((basket_analysis_table), n=10)$Bar_Colour)
    } else if (basketbar_shownum_selector == "show25") {
        xData_Selector <- head((basket_analysis_table), n=25)[[c(Basket_Analysis_Grouping)]]
        yData_Selector <- head((basket_analysis_table), n=25)$Net_TM_impact
        Bar_Colour <- as.list(head((basket_analysis_table), n=25)$Bar_Colour)
    } else if (basketbar_shownum_selector == "show50") {
        xData_Selector <- head((basket_analysis_table), n=50)[[c(Basket_Analysis_Grouping)]]
        yData_Selector <- head((basket_analysis_table), n=50)$Net_TM_impact
        Bar_Colour <- as.list(head((basket_analysis_table), n=50)$Bar_Colour)
    } else if (basketbar_shownum_selector == "show100") {
        xData_Selector <- head((basket_analysis_table), n=100)[[c(Basket_Analysis_Grouping)]]
        yData_Selector <- head((basket_analysis_table), n=100)$Net_TM_impact
        Bar_Colour <- as.list(head((basket_analysis_table), n=100)$Bar_Colour)
    } else {
        xData_Selector <- (basket_analysis_table)[[c(Basket_Analysis_Grouping)]]
        yData_Selector <- (basket_analysis_table)$Net_TM_impact
        Bar_Colour <- as.list((basket_analysis_table)$Bar_Colour)
    }

    xData <- xData_Selector
    yData <- yData_Selector

    
    trace1 <- list(
        x = xData,
        y = yData,
        type = 'bar',
        orientation = 'v',
        text = xData,
        textposition = 'inside',
        # marker = list(
        #     color = 'rgb(0,0,142)'
        # ),
        marker = list(color = Bar_Colour),
        # hovertemplate = "<b>Change in trading margin: </b>%{x:$,.2f}<extra></extra>"
        hovertemplate = "<b>%{text}</b>
                        <br><b>Impact on Trading Margin: </b>%{y:$,.2f}<extra></extra>"
    )

    data = list(trace1)


    layout = list(
        xaxis = list(
            # title = "{text}",
            showgrid = FALSE,
            showline = FALSE
        ),
        yaxis = list(
            title = "Basket Change Impact on Trading Margin ($)",
            showgrid = FALSE,
            showline = FALSE
            # ,
            # visible = FALSE
        ),
        margin = list(
            # l = 150,
            r = 20, 
            b = 150,
            t = 5,
            pad = 5
        ),
        paper_bgcolor = 'rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0,0,0,0)'
    )

    return(list(traces=data, layout=layout))
}

# Underlying data 
generategraph39 <- function(){

    # Get the filter level you want to use
    basket_analysis_grouping <- Basket_Analysis_Grouping

    # Filter for only rows that are from a basket change
    basket_analysis_table <- basket_analysis_table %>% 
        filter((Qty_1==0 & Qty_2!=0) | (Qty_1!= 0 & Qty_2==0))

    filter_text <- str_replace(Basket_Analysis_Grouping, "_", " ")
    filter_text <- sprintf("<b>%s</b>", filter_text)        # get the string variable and css combined 

    #### PRE-PROCESSING TO GROUP DATA DYNAMICALLY BY USER SELECTION #####
    basket_analysis_table <- basket_analysis_table %>% 
        # Group items by selector
        group_by_at(c(Basket_Analysis_Grouping)) %>% 
        # Summarize relevant values
        summarise(Qty_1 = sum(Qty_1),
                    Qty_2 = sum(Qty_2),
                    Sales_Inc_GST_1 = sum(Sales_Inc_GST_1),
                    Sales_Inc_GST_2 = sum(Sales_Inc_GST_2),
                    # Original Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate
                    Net_Sales_1 = sum(Net_Sales_1),
                    Net_Sales_2 = sum(Net_Sales_2),
                    # Original Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims
                    Trading_Margin_1 = sum(Trading_Margin_1),
                    Trading_Margin_2 = sum(Trading_Margin_2),
                    # Original Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty
                    Net_Selling_Price_1 = sum(Net_Selling_Price_1),
                    Net_Selling_Price_2 = sum(Net_Selling_Price_2),
                    # Original Net_Cost_Price = COGS/Qty
                    Net_Cost_Price_1  = sum(Net_Cost_Price_1),
                    Net_Cost_Price_2  = sum(Net_Cost_Price_2)) %>%
        # Original TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
        mutate(TM_percent_1 = Trading_Margin_1/Net_Sales_1*100,
                TM_percent_2 = Trading_Margin_2/Net_Sales_2*100) %>%
        mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
                Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>%
        mutate(Net_TM_impact = (Net_Selling_Price_eff - Net_Cost_Price_eff)*Qty_2) %>%
        mutate(TM_Dollar_Change = Trading_Margin_2 - Trading_Margin_1,
                TM_Percent_Change = TM_percent_2-TM_percent_1) %>%
        mutate(Qty_Change = Qty_2 - Qty_1) %>%
        # Filter out rows where TM impact is not zero
        filter(Net_TM_impact != 0)
    


    table_sortby_selector <- 'Net_TM_impact'
    basketbar_ascdesc_selector <- BasketBar_AscDesc_Selector

    if (basketbar_ascdesc_selector == "descending") {
        basket_analysis_table <- basket_analysis_table %>%
            arrange(desc(get(table_sortby_selector)))
    } else {
        basket_analysis_table <- basket_analysis_table %>%
            arrange(get(table_sortby_selector))
    }

    # This is the data you will be displaying as a table
    trace0 = list(
        # basket_analysis_grouping
        basket_analysis_table[[basket_analysis_grouping]],
        # Period 1 Sales
        paste('$',formatC(basket_analysis_table$Sales_Inc_GST_1, big.mark=',', digits=2, format = 'f')),
        # Period 1 Volume
        formatC(basket_analysis_table$Qty_1, big.mark=',', digits=2, format = 'f'),
        # Period 1 TM $
        paste(formatC(basket_analysis_table$Trading_Margin_1, big.mark=',', digits=2, format = 'f'), '%'),
        # Period 1 TM %
        paste(formatC(basket_analysis_table$TM_percent_1, big.mark=',', digits=2, format = 'f'), '%'),
        # Period 2 Sales 
        paste('$',formatC(basket_analysis_table$Sales_Inc_GST_2, big.mark=',', digits=2, format = 'f')),
        # Period 2 Volume
        formatC(basket_analysis_table$Qty_2, big.mark=',', digits=2, format = 'f'), 
        # Period 2 TM $ 
        paste(formatC(basket_analysis_table$Trading_Margin_2, big.mark=',', digits=2, format = 'f'), '%'),
        # Period 2 TM %
        paste(formatC(basket_analysis_table$TM_percent_2, big.mark=',', digits=2, format = 'f'), '%'),
        # Net Selling Price Change
        paste('$',formatC(basket_analysis_table$Net_Selling_Price_eff, big.mark=',', digits=2, format = 'f')),
        # Net Cost Price Change
        paste('$',formatC(basket_analysis_table$Net_Cost_Price_eff, big.mark=',', digits=2, format = 'f')),
        # Net Impact on Trading Margin
        paste('$',formatC(basket_analysis_table$Net_TM_impact, big.mark=',', digits=2, format = 'f')),
        # Change in TM $
        paste('$',formatC(basket_analysis_table$TM_Dollar_Change, big.mark=',', digits=2, format = 'f')),
        # Change in TM %
        paste(formatC(basket_analysis_table$TM_Percent_Change, big.mark=',', digits=2, format = 'f'), '%')
    )

    # Adding a conditional formatting colour for each row
    basket_analysis_table <- basket_analysis_table %>%
        mutate(Colour_Sales_Inc_GST_1 = ifelse(Sales_Inc_GST_1>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Qty_1 = ifelse(Qty_1>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Trading_Margin_1 = ifelse(Trading_Margin_1>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_TM_percent_1 = ifelse(TM_percent_1>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Sales_Inc_GST_2 = ifelse(Sales_Inc_GST_2>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Qty_2 = ifelse(Qty_2>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Trading_Margin_2 = ifelse(Trading_Margin_2>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_TM_percent_2 = ifelse(TM_percent_2>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Net_Selling_Price_eff = ifelse(Net_Selling_Price_eff>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Net_Cost_Price_eff = ifelse(Net_Cost_Price_eff>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_Net_TM_impact = ifelse(Net_TM_impact>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_TM_Dollar_Change = ifelse(TM_Dollar_Change>0, 'rgb(0,0,142)', 'rgb(142,0,0)'), 
                Colour_TM_Percent_Change = ifelse(TM_Percent_Change>0, 'rgb(0,0,142)', 'rgb(142,0,0)')
        )

    # # Conditional formatting for columns 9-13
    # # n = 0 
    # # positive
    # if (trace0[9] > 0) {
    #     Line_Condition <- "list(color = 'black', width = 1)"
    #     # fill is light green & font is dark green
    #     Fill_Condition <- "list(color = 'rgba(194,255,194,1)')"
    #     Font_Condition <- "list(family = 'Arial', size = 13, color = 'rgba(0, 124, 0, 0.8)')"
    #     # n <- n + 1
    # # negative
    # } else if (trace0[9] < 0) {
    #     Line_Condition <- "list(color = 'black', width = 1)"
    #     Fill_Condition <- "list(color = c('white', 'white'))"
    #     Font_Condition <- "list(family = 'Arial', size = 13, color = c('black'))"
    #     # n <- n + 1
    # } else {
    #     Line_Condition <- "list(color = 'black', width = 1)"
    #     Fill_Condition <- "list(color = c('white', 'white'))"
    #     Font_Condition <- "list(family = 'Arial', size = 13, color = c('black'))"
    #     # n <- n + 1
    # }

    #     # positive
    # if (trace0[9] > 0) {
    #     Line_Condition <- c(list(color = "black", width = 1))
    #     # fill is light green & font is dark green
    #     Fill_Condition <- c(list(color = 'rgba(194,255,194,1)'))
    #     Font_Condition <- c(list(family = "Arial", size = 13, color = 'rgba(0, 124, 0, 0.8)'))
    #     # n <- n + 1
    # # negative
    # } else if (trace0[9] < 0) {
    #     Line_Condition <- c(list(color = "black", width = 1))
    #     Fill_Condition <- c(list(color = c('white', 'white')))
    #     Font_Condition <- c(list(family = "Arial", size = 13, color = c("black")))
    #     # n <- n + 1
    # } else {
    #     Line_Condition <- c(list(color = "black", width = 1))
    #     Fill_Condition <- c(list(color = c('white', 'white')))
    #     Font_Condition <- c(list(family = "Arial", size = 13, color = c("black")))
    #     # n <- n + 1
    # }

    data = list(
        type = "table",
        # columnwidth = list(1400, 400, 400, 500, 500, 600, 600),      
        columnwidth = list(600,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150),
        header = list(
            values = list(
                filter_text, 
                # Period 1 Sales
                c("<b>Period 1 Sales</b>"), 
                # Period 1 Volume
                c("<b>Period 1 Volume</b>"), 
                # Period 1 TM $
                c("<b>Period 1 TM $</b>"),
                # Period 1 TM %
                c("<b>Period 1 TM %</b>"), 
                # Period 2 Sales 
                c("<b>Period 2 Sales</b>"),
                # Period 2 Volume
                c("<b>Period 2 Volume</b>"), 
                # Period 2 TM $ 
                c("<b>Period 2 TM $</b>"),
                # Period 2 TM %
                c("<b>Period 2 TM %</b>"),
                # Net Selling Price Change
                c("<b>Net Selling Price Change</b>"),
                # Net Cost Price Change
                c("<b>Net Cost Price Change</b>"),
                # Net Impact on Trading Margin
                c("<b>Net Impact on TM</b>"),
                # Change in TM $
                c("<b>Change in TM $</b>"),
                # Change in TM %
                c("<b>Change in TM %</b>")
            ),
            align = "center",
            line = list(
                width = 1,
                color = 'black'
            ),
            fill = list(color = '#124478'),
            font = list(
                family = "Arial",
                size = 13,
                color = "white"
            )
        ),
        cells = list(
            values = trace0,
            align = c("left", "center"),
            line = list(
                color = "black",
                width = 1
            ),
            fill = list(
                color = c('white', 'white')),
                # color = c('white',
                #             as.list(basket_analysis_table$Colour_Sales_Inc_GST_1),
                #             as.list(basket_analysis_table$Colour_Qty_1),
                #             as.list(basket_analysis_table$Colour_Trading_Margin_1),
                #             as.list(basket_analysis_table$Colour_TM_percent_1),
                #             as.list(basket_analysis_table$Colour_Sales_Inc_GST_2),
                #             as.list(basket_analysis_table$Colour_Qty_2),
                #             as.list(basket_analysis_table$Colour_Trading_Margin_2),
                #             as.list(basket_analysis_table$Colour_TM_percent_2),
                #             as.list(basket_analysis_table$Colour_Net_Selling_Price_eff),
                #             as.list(basket_analysis_table$Colour_Net_Cost_Price_eff),
                #             as.list(basket_analysis_table$Colour_Net_TM_impact),
                #             as.list(basket_analysis_table$Colour_TM_Dollar_Change),
                #             as.list(basket_analysis_table$Colour_TM_Percent_Change)
                #         )),
                        
                # color = c('white', 'white', 'white','white','white','white','white','white','white', 
                #         lapply()
                #         (ifelse(trace0[[10]] > 0,'red','green')),
                #         (ifelse(trace0[[11]] > 0,'red','green')),
                #         (ifelse(trace0[[12]] > 0,'red','green')),
                #         (ifelse(trace0[[13]] > 0,'red','green')))
            font = list(
                family = "Arial",
                size = 13,
                color = c("black")
            )
            # line = Line_Condition,
            # fill = Fill_Condition, 
            # font = Font_Condition,
        )
    )

    layout = list(
        autosize = TRUE, 
        margin = list(
            l = 5,
            r = 10, 
            b = 10,
            t = 10,
            pad = 2
        ),
        paper_bgcolor = 'rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0,0,0,0)'
    )

    return(list(traces=data, layout=layout))
}


