

# Import sample datasets 
Summary_Sales_Data <- read_csv("C:/Users/gaov/OneDrive - Pitcher Partners Pty Ltd/1 PFD Trading Margin/Inputs/TMBaseTableSample.csv")
Sales_Data <- read_csv("C:/Users/gaov/OneDrive - Pitcher Partners Pty Ltd/1 PFD Trading Margin/Inputs/TMAggregatedBaseTableDecOnly5perc.csv")
Complete_Sales_Data  <- read_csv("C:/Users/gaov/OneDrive - Pitcher Partners Pty Ltd/1 PFD Trading Margin/Inputs/TMBaseTableSample.csv")


#############################-INITALISING SPARKLYR AND DATABRICKS CONNECT -##############################----

# Spark home for local
sc <- spark_connect(method = "databricks", spark_home = "c:/users/gaov/anaconda3/envs/dbtest/lib/site-packages/pyspark")

# Spark home for VM
# setwd("/home/pfdadmin/")
# sc <- spark_connect(method = "databricks", spark_home = "/usr/local/lib/python3.7/dist-packages/pyspark")
# print(sessionInfo())

# Reading in entire base table as sparklyr spark dataframe
# TM_Base_Table <- spark_read_csv(sc, path="dbfs:/FileStore/df/pfd_trading_margin_full.csv/part-00000-tid-8852796306218385673-c0757c75-5f05-4171-bc92-4aa5dca4e676-197-1-c000.csv")

# one-week extract of base table
TM_Base_Table <- spark_read_csv(sc, path="dbfs:/FileStore/df/pfd_trading_margin_sample_v1.csv/part-00000-tid-6175667190246818734-00c16139-0f3a-4c7e-8880-3e4b5e4c03ae-10-1-c000.csv")



#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                         INITALISE FILTERS                               #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 

# Connecting to Azure blob storage
endp <- AzureStor:::storage_endpoint("https://stpfdrpgmserver.blob.core.windows.net", key="H2ZYiJULDKd8xtkHO9HQWUQLoM0TcGauGBw+Ouyp4VaC+t1MD6EJoUorodmEWyUiripVJYauPDO2Eui4lyee4A==")
cont <- AzureStor:::storage_container(endp, "test")
fname <- tempfile()
AzureStor:::storage_download(cont, "Account_Manager_Master.csv", fname)


# # Import filter lists
Account_Manager_Master <- AzureStor::storage_read_csv(cont, "Account_Manager_Master.csv")
Customer_Name_Master <- AzureStor::storage_read_csv(cont, "Customer_Name_Master.csv")
Site_Name_Master <- AzureStor::storage_read_csv(cont, "Site_Name_Master.csv")
Customer_Type_Master <- AzureStor::storage_read_csv(cont, "Customer_Type_Master.csv")
Site_State_Master <- AzureStor::storage_read_csv(cont, "Site_State_Master.csv")
Item_Category_Master <- AzureStor::storage_read_csv(cont, "Item_Category_Master.csv")
Item_Description_Master <- AzureStor::storage_read_csv(cont, "Item_Description_Master.csv")


# Customer_Type_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Customer_Type_Master.csv")
# Site_Name_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Site_Name_Master.csv")
# Site_State_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Site_State_Master.csv")
# Account_Manager_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Account_Manager_Master.csv")
# Customer_Name_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Customer_Name_Master.csv")
# Item_Category_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Item_Category_Master.csv")
# Item_Description_Master <- read_csv("C:/Users/gaov/Documents/Bitbucket/PFD Trading Margin/Base Tables/Item_Description_Master.csv")

# Add all as selection choice in list
Customer_Type_List <- data.frame("Customer_Type" = t(c("All"))) %>% rbind(Customer_Type_Master)
Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% rbind(Site_State_Master)
Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% rbind(Site_Name_Master)
Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% rbind(Account_Manager_Master)
Customer_Name_List <- data.frame("Customer_Name" = t(c("All"))) %>% rbind(Customer_Name_Master)
Item_Category_List <- data.frame("Item_Category" = t(c("All"))) %>% rbind(Item_Category_Master)
Item_Description_List <- data.frame("Item_Description" = t(c("All"))) %>% rbind(Item_Description_Master)

gui.clearChoices(rpgm.step("main", "TMA"),"CustType_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"CustType_Filter_Selector",Customer_Type_List$Customer_Type, Customer_Type_List$Customer_Type)
gui.setChoiceText(rpgm.step("main", "TMA"),"CustType_Filter_Selector",Customer_Type_List[1,"Customer_Type"],Customer_Type_List[1,"Customer_Type"] ) 

gui.clearChoices(rpgm.step("main", "TMA"),"State_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"State_Filter_Selector",Site_State_List$Site_State, Site_State_List$Site_State)
gui.setChoiceText(rpgm.step("main", "TMA"),"State_Filter_Selector",Site_State_List[1,"Site_State"],Site_State_List[1,"Site_State"] ) 

gui.clearChoices(rpgm.step("main", "TMA"),"SiteName_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"SiteName_Filter_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)
gui.setChoiceText(rpgm.step("main", "TMA"),"SiteName_Filter_Selector",Site_Name_List[1,"Site_Name"],Site_Name_List[1,"Site_Name"] ) 

gui.clearChoices(rpgm.step("main", "TMA"),"AccManager_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"AccManager_Filter_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)
gui.setChoiceText(rpgm.step("main", "TMA"),"AccManager_Filter_Selector",Account_Manager_List[1,"Account_Manager"],Account_Manager_List[1,"Account_Manager"] ) 

gui.clearChoices(rpgm.step("main", "TMA"),"Customer_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"Customer_Filter_Selector",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)
gui.setChoiceText(rpgm.step("main", "TMA"),"Customer_Filter_Selector",Customer_Name_List[1,"Customer_Name"],Customer_Name_List[1,"Customer_Name"] ) 

gui.clearChoices(rpgm.step("main", "TMA"),"ItmCategory_Filter_Selector")
gui.addChoices(rpgm.step("main", "TMA"),"ItmCategory_Filter_Selector",Item_Category_List$Item_Category, Item_Category_List$Item_Category)
gui.setChoiceText(rpgm.step("main", "TMA"),"ItmCategory_Filter_Selector",Item_Category_List[1,"Item_Category"],Item_Category_List[1,"Item_Category"] ) 

# gui.clearChoices(rpgm.step("main", "TMA"),"Item_Filter_Selector")
# gui.addChoices(rpgm.step("main", "TMA"),"Item_Filter_Selector",Item_Description_List$Item_Description, Item_Description_List$Item_Description)
# gui.setChoiceText(rpgm.step("main", "TMA"),"Item_Filter_Selector",Item_Description_List[1,"Item_Description"],Item_Description_List[1,"Item_Description"] ) 


if (counter_1 == 0){
    custtype_filter_selector <- 'All'
    state_filter_selector <- 'All'
    sitename_filter_selector <- 'All'
    accmanager_filter_selector <- 'All'
    customer_filter_selector <- 'All'
    itmcategory_filter_selector <- 'All'
    # item_filter_selector <- 'All'
    assign("counter_1", 1, envir = .GlobalEnv) 
} else {
    custtype_filter_selector <- CustType_Filter_Selector
    state_filter_selector <- State_Filter_Selector
    sitename_filter_selector <- SiteName_Filter_Selector
    accmanager_filter_selector <- AccManager_Filter_Selector
    customer_filter_selector <- Customer_Filter_Selector
    itmcategory_filter_selector <- ItmCategory_Filter_Selector
    # item_filter_selector <- Item_Filter_Selector
}


#### OLD FUNCTIONS FOR INITALISING GRAPHS FUNCTION
# compare_the_pair();updateDashboardVisuals();margin_breakdown(Summary_Sales_Data);updateMarginVisuals();customers_of_interest(Sales_Data);updateCustomersofInterest();deep_insights(Sales_Data);updateDeepInsightsVisuals();get_filters_2();drill_down_2();updateDrillDownVisuals_2();pricing_reasons();updateReasonVisuals();get_reason_filters();update_time_view();basket_preprocessing();get_basket_filters();updateNewBasketVisuals();
# compare_the_pair();updateDashboardVisuals();margin_breakdown(Summary_Sales_Data);updateMarginVisuals();customers_of_interest(Sales_Data);updateCustomersofInterest();deep_insights(Sales_Data);updateDeepInsight

