

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                     Functions for Basket_Analysis Tab                   #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


# Adapted from pricing_reasons() function
# Pre-prcoessing data for basket analysis tab
basket_preprocessing <- function(){

    tm_test <- Complete_Sales_Data %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(
            Item_Description = gsub('#|[*]', '', Item_Description),
            Customer_Name = gsub("^\\*", "", Customer_Name)) 

    #' *Filter for desired date range*
    date_range_filter <- function(df, time1, time2){
        df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)
        return(df1)
    }

    # Reset filters upon first run of code for Basket Analysis tab
    if (counter_m == 0){
        basket_custtype_selector <- 'All'
        gui.setValue("this","Basket_CustType_Selector", "All")   
        basket_state_selector <- 'All'
        gui.setValue("this","Basket_State_Selector", "All")   
        basket_sitename_selector <- 'All'
        gui.setValue("this","Basket_SiteName_Selector", "All")   
        basket_accmanager_selector <- 'All'
        gui.setValue("this","Basket_AccManager_Selector", "All")   
        basket_customer_selector <- 'All'
        gui.setValue("this","Basket_Customer_Selector", "All")   
        basket_itmcategory_selector <- 'All'
        gui.setValue("this","Basket_ItmCategory_Selector", "All")   
        basket_item_selector <- 'All'
        gui.setValue("this","Basket_Item_Selector", "All")   
        assign("counter_m", 1, envir = .GlobalEnv) 
    } else {
        basket_custtype_selector <- Basket_CustType_Selector
        basket_state_selector <- Basket_State_Selector
        basket_sitename_selector <- Basket_SiteName_Selector
        basket_accmanager_selector <- Basket_AccManager_Selector
        basket_customer_selector <- Basket_Customer_Selector
        basket_itmcategory_selector <- Basket_ItmCategory_Selector
        basket_item_selector <- Basket_Item_Selector
    }

    starttime1 <- Start_Time_P1
    starttime2 <- Start_Time_P2
    endtime1 <- End_Time_P1
    endtime2 <- End_Time_P2

        
    tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
    tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)

    tm_test_temp <- tm_test_1 %>% rbind(tm_test_2)

    
    if (basket_custtype_selector=='All' | is.null(basket_custtype_selector) | is.na(basket_custtype_selector)){
        custtype_group = (tm_test_temp %>% select(Customer_Type) %>% distinct())$Customer_Type
    } else {
        custtype_group <- basket_custtype_selector
    }
    
    if (basket_state_selector=='All' | is.null(basket_state_selector) | is.na(basket_state_selector)){
        state_group = (tm_test_temp %>% select(Site_State) %>% distinct())$Site_State
    } else {
        state_group <- basket_state_selector
    }
    
    if (basket_sitename_selector=='All' | is.null(basket_sitename_selector) | is.na(basket_sitename_selector)){
        sitename_group = (tm_test_temp %>% select(Site_Name) %>% distinct())$Site_Name
    } else {
        sitename_group <- basket_sitename_selector
    }
    
    if (basket_accmanager_selector=='All' | is.null(basket_accmanager_selector) | is.na(basket_accmanager_selector)){
        accmanager_group = (tm_test_temp %>% select(Account_Manager) %>% distinct())$Account_Manager
    } else {
        accmanager_group <- basket_accmanager_selector
    }
    
    if (basket_customer_selector=='All' | is.null(basket_customer_selector) | is.na(basket_customer_selector)){
        customer_group = (tm_test_temp %>% select(Customer_Name) %>% distinct())$Customer_Name
    } else {
        customer_group <- basket_customer_selector
    }
    
    if (basket_itmcategory_selector=='All' | is.null(basket_itmcategory_selector) | is.na(basket_itmcategory_selector)){
        itmcategory_group = (tm_test_temp %>% select(Item_Category) %>% distinct())$Item_Category
    } else {
        itmcategory_group <- basket_itmcategory_selector
    }
    
    if (basket_item_selector=='All' | is.null(basket_item_selector) | is.na(basket_item_selector)){
        item_group = (tm_test_temp %>% select(Item_Description) %>% distinct())$Item_Description
    } else {
        item_group <- basket_item_selector
    }

    tm_test_1 <- tm_test_1 %>%
        filter(Customer_Type %in% custtype_group,
                Site_State %in% state_group,
                Site_Name %in% sitename_group,
                Account_Manager %in% accmanager_group,
                Customer_Name %in% customer_group,
                Item_Category %in% itmcategory_group,
                Item_Description %in% item_group)

    tm_test_2 <- tm_test_2 %>%
        filter(Customer_Type %in% custtype_group,
                Site_State %in% state_group,
                Site_Name %in% sitename_group,
                Account_Manager %in% accmanager_group,
                Customer_Name %in% customer_group,
                Item_Category %in% itmcategory_group,
                Item_Description %in% item_group)

    #' *Calculate trading margin*
    calculate_TM <- function(df){
        df1 <- df %>% 
            group_by(Customer_Type, Site_State, Site_Name, Account_Manager, 
                     Customer_Name, Item_Category, Item_Description) %>%
            summarise(Qty=sum(Qty),
                        Sales_Inc_GST=sum(Sales_Inc_GST),
                        COGS=sum(COGS),
                        Customer_Rebate=sum(Customer_Rebate),
                        Sales_Claims=sum(Sales_Claims), 
                        Promo_Claims=sum(Promo_Claims)) %>% 
            mutate(Selling_Price = (Sales_Inc_GST)/Qty,
                    COGS_u = COGS/Qty,
                    Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
                    Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
                    TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
                    TM_percent = ifelse(is.na(TM_percent), 0, TM_percent),
                    # Additional formula from excel sheet "Customer Example" provided by PFD
                    Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
                    Net_Cost_Price = COGS/Qty)
        return(df1)
        
    }

    tm_test_1_item <- calculate_TM(tm_test_1)
    tm_test_2_item <- calculate_TM(tm_test_2)


    compare_periods <- function(df1, df2){
      tm <- df1 %>% 
        full_join(df2, 
                    by=c("Customer_Type",
                         "Site_State",
                         "Site_Name",
                         "Account_Manager",
                         "Customer_Name",
                         "Item_Category",
                         "Item_Description"),
                    suffix=c("_1","_2")) %>%
        ungroup() %>%
        # rename_all(function(x) paste0(x,"_1")) %>%
        # bind_cols(df2 %>% rename_all(function(x) paste0(x,"_2"))) %>% 
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(Change_in_cost =  COGS_2 - COGS_1,
               Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
               Change_in_sell = Selling_Price_2 - Selling_Price_1,
               Change_in_COGS_u = COGS_u_2 - COGS_u_1,
               Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
               Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
               Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
               Change_in_qty = Qty_2 - Qty_1,
               TM_change = Trading_Margin_2-Trading_Margin_1,
               TM_percent_change = TM_percent_2 - TM_percent_1,
                #  Additional formula from excel sheet "Customer Example" provided by PFD
               Change_in_net_selling_price = Net_Selling_Price_2 - Net_Selling_Price_1, 
               Change_in_net_cost_price = Net_Cost_Price_2 - Net_Cost_Price_1) %>% 
        # Additional formula from excel sheet "Customer Example" provided by PFD
        # Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
        # mutate(Net_Selling_Price_eff = (Net_Selling_Price_2/Qty_2)-(Net_Selling_Price_1/Qty_1),
        mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
               Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>%
        mutate(Net_TM_impact = (Net_Selling_Price_eff - Net_Cost_Price_eff)*Qty_2) %>%
        # mutate(Price_eff = Change_in_sell*Qty_1,
        #      COGS_eff = -Change_in_COGS_u*Qty_1,
        #      Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
        #      Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2),
        # filter(!(Qty_1==0 & Qty_2==0)) %>%
        arrange(Change_in_qty)
        return(tm)
    }

    df_drilldown_comparison <- compare_periods(tm_test_1_item, tm_test_2_item) %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
        mutate_if(is.numeric, ~round(., 2)) 

    assign("basket_analysis_table", df_drilldown_comparison, envir = .GlobalEnv)
}



###########################################################################
#####-------------- FILTER FUNCTIONS FOR BASKET ANALYSIS
###########################################################################

# Obtains and updates filters for Basket Analysis tab
# Adapted from get_reason_filters() in Pricing_Reason_Functions
get_basket_filters <- function(){

    tm_test_temp <- Complete_Sales_Data %>%
        # select(-Column_1) %>% 
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(
            Item_Description = gsub('#|[*]', '', Item_Description),
            Customer_Name = gsub("^\\*", "", Customer_Name)) 
        #     %>%
        # filter(Item_Code!='TRAILER', Item_Code!='MEMO')

    starttime1 <- Start_Time_P1
    starttime2 <- Start_Time_P2
    endtime1 <- End_Time_P1
    endtime2 <- End_Time_P2

    #' *Filter for desired date range*
    date_range_filter <- function(df, time1, time2){
        # df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
        df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)
        return(df1)
        
    }

    tm_test_temp_1 <- date_range_filter(tm_test_temp, starttime1, endtime1)
    tm_test_temp_2 <- date_range_filter(tm_test_temp, starttime2, endtime2)

    tm_test_temp <- tm_test_temp_1 %>% rbind(tm_test_temp_2)



    if (counter_n == 0){
        basket_custtype_selector <- 'All'
        basket_state_selector <- 'All'
        basket_sitename_selector <- 'All'
        basket_accmanager_selector <- 'All'
        basket_customer_selector <- 'All'
        basket_itmcategory_selector <- 'All'
        basket_item_selector <- 'All'
        assign("counter_n", 1, envir = .GlobalEnv) 
    } else {
        basket_custtype_selector <- Basket_CustType_Selector
        basket_state_selector <- Basket_State_Selector
        basket_sitename_selector <- Basket_SiteName_Selector
        basket_accmanager_selector <- Basket_AccManager_Selector
        basket_customer_selector <- Basket_Customer_Selector
        basket_itmcategory_selector <- Basket_ItmCategory_Selector
        basket_item_selector <- Basket_Item_Selector
    }


    if (basket_custtype_selector=='All' | is.null(basket_custtype_selector) | is.na(basket_custtype_selector)){
        custtype_group = (tm_test_temp %>% select(Customer_Type) %>% distinct())$Customer_Type
    } else {
        custtype_group <- basket_custtype_selector
    }
    
    if (basket_state_selector=='All' | is.null(basket_state_selector) | is.na(basket_state_selector)){
        state_group = (tm_test_temp %>% select(Site_State) %>% distinct())$Site_State
    } else {
        state_group <- basket_state_selector
    }
    
    if (basket_sitename_selector=='All' | is.null(basket_sitename_selector) | is.na(basket_sitename_selector)){
        sitename_group = (tm_test_temp %>% select(Site_Name) %>% distinct())$Site_Name
    } else {
        sitename_group <- basket_sitename_selector
    }
    
    if (basket_accmanager_selector=='All' | is.null(basket_accmanager_selector) | is.na(basket_accmanager_selector)){
        accmanager_group = (tm_test_temp %>% select(Account_Manager) %>% distinct())$Account_Manager
    } else {
        accmanager_group <- basket_accmanager_selector
    }
    
    if (basket_customer_selector=='All' | is.null(basket_customer_selector) | is.na(basket_customer_selector)){
        customer_group = (tm_test_temp %>% select(Customer_Name) %>% distinct())$Customer_Name
    } else {
        customer_group <- basket_customer_selector
    }
    
    if (basket_itmcategory_selector=='All' | is.null(basket_itmcategory_selector) | is.na(basket_itmcategory_selector)){
        itmcategory_group = (tm_test_temp %>% select(Item_Category) %>% distinct())$Item_Category
    } else {
        itmcategory_group <- basket_itmcategory_selector
    }
    
    if (basket_item_selector=='All' | is.null(basket_item_selector) | is.na(basket_item_selector)){
        item_group = (tm_test_temp %>% select(Item_Description) %>% distinct())$Item_Description
    } else {
        item_group <- basket_item_selector
    }

    tm_test_temp <- tm_test_temp %>%
        filter(Customer_Type %in% custtype_group,
                Site_State %in% state_group,
                Site_Name %in% sitename_group,
                Account_Manager %in% accmanager_group,
                Customer_Name %in% customer_group,
                Item_Category %in% itmcategory_group,
                Item_Description %in% item_group)


    # Pre-populate filters
    Customer_Type_List <- data.frame("Customer_Type" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type))
    Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
    Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name))
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))
    Customer_Name_List <- data.frame("Customer_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))
    Item_Category_List <- data.frame("Item_Category" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category))
    Item_Description_List <- data.frame("Item_Description" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description))


    gui.clearChoices("this","Basket_CustType_Selector")
    gui.addChoices("this","Basket_CustType_Selector",Customer_Type_List$Customer_Type, Customer_Type_List$Customer_Type)
    gui.setChoiceText("this","Basket_CustType_Selector",Customer_Type_List[1,"Customer_Type"],Customer_Type_List[1,"Customer_Type"] ) 

    gui.clearChoices("this","Basket_State_Selector")
    gui.addChoices("this","Basket_State_Selector",Site_State_List$Site_State, Site_State_List$Site_State)
    gui.setChoiceText("this","Basket_State_Selector",Site_State_List[1,"Site_State"],Site_State_List[1,"Site_State"] ) 

    gui.clearChoices("this","Basket_SiteName_Selector")
    gui.addChoices("this","Basket_SiteName_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)
    gui.setChoiceText("this","Basket_SiteName_Selector",Site_Name_List[1,"Site_Name"],Site_Name_List[1,"Site_Name"] ) 

    gui.clearChoices("this","Basket_AccManager_Selector")
    gui.addChoices("this","Basket_AccManager_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)
    gui.setChoiceText("this","Basket_AccManager_Selector",Account_Manager_List[1,"Account_Manager"],Account_Manager_List[1,"Account_Manager"] ) 

    gui.clearChoices("this","Basket_Customer_Selector")
    gui.addChoices("this","Basket_Customer_Selector",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)
    gui.setChoiceText("this","Basket_Customer_Selector",Customer_Name_List[1,"Customer_Name"],Customer_Name_List[1,"Customer_Name"] ) 

    gui.clearChoices("this","Basket_ItmCategory_Selector")
    gui.addChoices("this","Basket_ItmCategory_Selector",Item_Category_List$Item_Category, Item_Category_List$Item_Category)
    gui.setChoiceText("this","Basket_ItmCategory_Selector",Item_Category_List[1,"Item_Category"],Item_Category_List[1,"Item_Category"] ) 

    gui.clearChoices("this","Basket_Item_Selector")
    gui.addChoices("this","Basket_Item_Selector",Item_Description_List$Item_Description, Item_Description_List$Item_Description)
    gui.setChoiceText("this","Basket_Item_Selector",Item_Description_List[1,"Item_Description"],Item_Description_List[1,"Item_Description"] ) 

}

# Adapted from reset_filters_2 function
reset_basket_filters <- function(){


    tm_test_temp <- Complete_Sales_Data %>%
        # select(-Column_1) %>% 
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(
            Item_Description = gsub('#|[*]', '', Item_Description),
            Customer_Name = gsub("^\\*", "", Customer_Name)) 
        #     %>%
        # filter(Item_Code!='TRAILER', Item_Code!='MEMO')



    starttime1 <- Start_Time_P1
    starttime2 <- Start_Time_P2
    endtime1 <- End_Time_P1
    endtime2 <- End_Time_P2

    #' *Filter for desired date range*
    date_range_filter <- function(df, time1, time2){
        # df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
        df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)
        return(df1)
    }
    

    tm_test_temp_1 <- date_range_filter(tm_test_temp, starttime1, endtime1)
    tm_test_temp_2 <- date_range_filter(tm_test_temp, starttime2, endtime2)

    tm_test_temp <- tm_test_temp_1 %>% rbind(tm_test_temp_2)

    # Pre-populate filters
    Customer_Type_List <- data.frame("Customer_Type" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type))
    Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
    Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name))
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))
    Customer_Name_List <- data.frame("Customer_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))
    Item_Category_List <- data.frame("Item_Category" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category))
    Item_Description_List <- data.frame("Item_Description" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description))


    # For every press of reset filters button, resets selection of filters and displays 'All'
    gui.clearChoices("this","Basket_CustType_Selector")
    gui.addChoices("this","Basket_CustType_Selector",Customer_Type_List$Customer_Type, Customer_Type_List$Customer_Type)
    gui.setChoiceText("this","Basket_CustType_Selector",Customer_Type_List[1,"Customer_Type"],Customer_Type_List[1,"Customer_Type"] ) 
    gui.setValue("this","Basket_CustType_Selector", "All") 

    gui.clearChoices("this","Basket_State_Selector")
    gui.addChoices("this","Basket_State_Selector",Site_State_List$Site_State, Site_State_List$Site_State)
    gui.setChoiceText("this","Basket_State_Selector",Site_State_List[1,"Site_State"],Site_State_List[1,"Site_State"] ) 
    gui.setValue("this","Basket_State_Selector", "All") 

    gui.clearChoices("this","Basket_SiteName_Selector")
    gui.addChoices("this","Basket_SiteName_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)
    gui.setChoiceText("this","Basket_SiteName_Selector",Site_Name_List[1,"Site_Name"],Site_Name_List[1,"Site_Name"] ) 
    gui.setValue("this","Basket_SiteName_Selector", "All") 

    gui.clearChoices("this","Basket_AccManager_Selector")
    gui.addChoices("this","Basket_AccManager_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)
    gui.setChoiceText("this","Basket_AccManager_Selector",Account_Manager_List[1,"Account_Manager"],Account_Manager_List[1,"Account_Manager"] ) 
    gui.setValue("this","Basket_AccManager_Selector", "All") 

    gui.clearChoices("this","Basket_Customer_Selector")
    gui.addChoices("this","Basket_Customer_Selector",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)
    gui.setChoiceText("this","Basket_Customer_Selector",Customer_Name_List[1,"Customer_Name"],Customer_Name_List[1,"Customer_Name"] ) 
    gui.setValue("this","Basket_Customer_Selector", "All") 

    gui.clearChoices("this","Basket_ItmCategory_Selector")
    gui.addChoices("this","Basket_ItmCategory_Selector",Item_Category_List$Item_Category, Item_Category_List$Item_Category)
    gui.setChoiceText("this","Basket_ItmCategory_Selector",Item_Category_List[1,"Item_Category"],Item_Category_List[1,"Item_Category"] ) 
    gui.setValue("this","Basket_ItmCategory_Selector", "All") 

    gui.clearChoices("this","Basket_Item_Selector")
    gui.addChoices("this","Basket_Item_Selector",Item_Description_List$Item_Description, Item_Description_List$Item_Description)
    gui.setChoiceText("this","Basket_Item_Selector",Item_Description_List[1,"Item_Description"],Item_Description_List[1,"Item_Description"] ) 
    gui.setValue("this","Basket_Item_Selector", "All") 
}
