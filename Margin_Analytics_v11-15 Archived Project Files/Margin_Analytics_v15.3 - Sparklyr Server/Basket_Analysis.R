#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                      BASKET ANALYSIS TAB FUCNTIONS                      #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


#############################- DATE_RANGE_FILTER() FUNCTION-##############################

# Function for filtering for desired date range
date_range_filter <- function(df, time1, time2){ 
  df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
  return(df1)
}


#############################- PRE PROECESSING FOR BASKET ANALYSIS TAB-##############################

# Adapted from pricing_reasons() function
# Pre-prcoessing data for basket analysis tab
customer_level_preprocessing <- function(){

    start_time <- Sys.time()

    # Update progress bar
    upload_progress <- round(0,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Get customer from user 
    basket_analysis_customer <- Basket_Analysis_Customer
    # Get the filter level from the user
    customer_level_grouping <- Customer_Level_Grouping

    # Filter based on user selection or no filter
    if (basket_analysis_customer=='All'){
        # customer_group = Customer_Name_Master
        customer_group <- as.character((Customer_Name_Master%>%rbind("UNK"))$Customer_Name)
    } else {
        customer_group <- basket_analysis_customer
    }

    #Calculate trading margin
    calculate_TM <- function(df){
        df1 <- df %>% 
        # Filter by user-selected choice
        filter(Customer_Name %in% customer_group) %>%
        # Group by user-selected choice
        group_by_at(c(customer_level_grouping)) %>% 
        summarise(Qty=sum(Qty),
                    Sales_Inc_GST=sum(Sales_Inc_GST),
                    COGS=sum(COGS),
                    Customer_Rebate=sum(Customer_Rebate),
                    Sales_Claims=sum(Sales_Claims), 
                    Promo_Claims=sum(Promo_Claims)) %>% 
        mutate(Selling_Price = (Sales_Inc_GST)/Qty,
                COGS_u = COGS/Qty,
                Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
                Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
                Net_Cost_Price = COGS/Qty) %>%
        mutate(Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims) %>%
        mutate(TM_Percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
                TM_Percent = ifelse(is.na(TM_Percent), 0, TM_Percent)) 
        return(df1)
    }

    # Update progress bar
    upload_progress <- round(5,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Get summarised data tables for both time periods
    tm_test_1_item <- calculate_TM(tm_test_1)

    # Update progress bar
    upload_progress <- round(10,0)
    gui.setValue("this","Basket_Progress",upload_progress)

    # Get summarised data tables for both time periods
    tm_test_2_item <- calculate_TM(tm_test_2)

    # Update progress bar
    upload_progress <- round(20,0)
    gui.setValue("this","Basket_Progress",upload_progress)

    # Convert spark dataframe into R
    tm_test_1_item <- collect(tm_test_1_item)

    # Update progress bar
    upload_progress <- round(50,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    tm_test_2_item <- collect(tm_test_2_item)

    # Update progress bar
    upload_progress <- round(80,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    compare_periods <- function(df1, df2){
        tm <- df1 %>% 
        full_join(df2, 
                    by=unique(c(customer_level_grouping)),
                    suffix=c("_1","_2")) %>%
        ungroup() %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(Change_in_cost =  COGS_2 - COGS_1,
              Change_in_COGS_u = COGS_u_2 - COGS_u_1,
              Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
              Change_in_sell = Selling_Price_2 - Selling_Price_1,
              Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
              Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
              Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
              Change_in_qty = Qty_2 - Qty_1,
              TM_Change = Trading_Margin_2 - Trading_Margin_1,
              TM_Percent_Change = TM_Percent_2 - TM_Percent_1) %>%
        mutate(Price_eff = Change_in_sell*Qty_1,
                COGS_eff = -Change_in_COGS_u*Qty_1,
                Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
                Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2)) %>%
        #  Additional formula from excel sheet "Customer Example" provided by PFD
        mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
               Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>% 
        mutate(Net_TM_Impact = ifelse((Qty_1!=0 & Qty_2!=0), 
                                      ((Net_Selling_Price_eff - Net_Cost_Price_eff) * Qty_2),
                                      Trading_Margin_2 - Trading_Margin_1)) %>%
        mutate(Volume_Change = ifelse((Qty_1!=0 & Qty_2!=0),
                                      (Qty_2 - Qty_1) * (Net_Selling_Price_1 - Net_Cost_Price_1),
                                      0)) %>% 
        mutate_if(is.numeric, ~round(., 2)) %>% 
        arrange(Change_in_qty)
      return(tm)
    }

    df_drilldown_comparison <- compare_periods(tm_test_1_item, tm_test_2_item) %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
        mutate_if(is.numeric, ~round(., 2)) 

    # Update progress bar
    upload_progress <- round(90,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Assigning reason for change in trading margin impact for each row
    customer_level_table <- df_drilldown_comparison %>% 
        mutate(Pricing_Reason = case_when((Qty_1!=0 & Qty_2!=0 &  
                                            Net_Selling_Price_eff>0 & Net_Cost_Price_eff>0 & 
                                            Net_Selling_Price_eff>Net_Cost_Price_eff)          
                                                                                                    ~ "Price_Incr_Greater",
                                        (Qty_1!=0 &  Qty_2!=0 &  
                                            Net_Selling_Price_eff>0 & Net_Cost_Price_eff==0)   
                                                                                                    ~ "Price_Incr",
                                        (Qty_1!=0 & Qty_2!=0 & 
                                            Net_Cost_Price_eff<0 & 
                                            Net_Cost_Price_eff<Net_Selling_Price_eff)          
                                                                                                    ~ "Cost_Decr_Greater",
                                        (Qty_1!=0 & Qty_2!=0 &  
                                            Net_Selling_Price_eff==0 & Net_Cost_Price_eff==0)  
                                                                                                    ~ "No_Change",
                                        (Qty_1!=0 & Qty_2!=0 &  
                                            Net_Selling_Price_eff==Net_Cost_Price_eff)  
                                                                                                    ~ "Equal",
                                        (Qty_1!=0 &  Qty_2!=0 &  
                                            Net_Selling_Price_eff<0 & Net_Cost_Price_eff==0)   
                                                                                                    ~ "Price_Decr",
                                        (Qty_1!=0 & Qty_2!=0 & 
                                            Net_Cost_Price_eff>0 & 
                                            Net_Cost_Price_eff>Net_Selling_Price_eff)          
                                                                                                    ~ "Cost_Incr_Greater",
                                        (Qty_1!=0 & Qty_2!=0 &  
                                            Net_Selling_Price_eff<0 & Net_Cost_Price_eff<0 & 
                                            Net_Cost_Price_eff>Net_Selling_Price_eff)
                                                                                                    ~ "Price_Decr_Greater",
                                        (Qty_2==0) 
                                                                                                    ~ "Lost_Sales",
                                        (Qty_1==0)
                                                                                                    ~ "New_Sales"))

    assign("customer_level_table", customer_level_table, envir = .GlobalEnv)

    # Update progress bar
    upload_progress <- round(100,0)
    gui.setValue("this","Basket_Progress",upload_progress)  

    # Time taken for function assigned to global environment
    end_time <- Sys.time()
    time_taken <- end_time - start_time
    assign("basket_analysis_time", time_taken, envir = .GlobalEnv)

}

# Save underlying data table to user-input location
export_basket_underlying_data <- function() {

    basket_underlying_data_name <- Basket_Underlying_Data_Name
    basket_underlying_data_location <- Basket_Underlying_Data_Location

    file_name <- paste0("\\", Basket_Underlying_Data_Name, ".csv")
    file_location <- c(Basket_Underlying_Data_Location)
    write_csv(customer_level_table, paste0(file_location, file_name))

}