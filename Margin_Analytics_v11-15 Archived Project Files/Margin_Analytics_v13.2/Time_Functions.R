#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                            TIME FUNCTIONS                               #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 

# Update time GUI based on frequency selection 
update_time_view <- function(){

    time_selector <- Time_Selector

    ### DATE RANGE TABLE ###
    # Table of dates from 2019-01-01 to today and extract month, year and day 
    Date_Range <- as.data.frame(seq(as.Date("2019-01-01"), Sys.Date(),"days")) %>% setNames(c("Invoice_date")) 
    Date_Range <- Date_Range %>% 
    mutate(Year = as.numeric(format(Date_Range$Invoice_date,"%Y")),
            Month = as.numeric(format(Date_Range$Invoice_date,"%m")),
            Day = as.numeric(format(Date_Range$Invoice_date,"%d"))) 
    Date_Range <- Date_Range %>% 
    mutate(Month_Name = month.name[Date_Range$Month])

    # Displays the relevant selection option based on frequency selected
    if (time_selector=="financial_year"){
        gui.show("this", "Date_Option_FinancialYear")
        gui.hide("this", "Date_Option_Annually")
        gui.hide("this", "Date_Option_Quarterly")
        gui.hide("this", "Date_Option_Monthly")
        gui.hide("this", "Date_Option_Weekly")
        gui.hide("this", "Date_Option_Custom")
        gui.show("this", "Time_Viewer")
    } else if (time_selector=="annually"){
        gui.hide("this", "Date_Option_FinancialYear")
        gui.show("this", "Date_Option_Annually")
        gui.hide("this", "Date_Option_Quarterly")
        gui.hide("this", "Date_Option_Monthly")
        gui.hide("this", "Date_Option_Weekly")
        gui.hide("this", "Date_Option_Custom")
        gui.show("this", "Time_Viewer")
    } else if (time_selector=="quarterly"){
        gui.hide("this", "Date_Option_FinancialYear")
        gui.hide("this", "Date_Option_Annually")
        gui.show("this", "Date_Option_Quarterly")
        gui.hide("this", "Date_Option_Monthly")
        gui.hide("this", "Date_Option_Weekly")
        gui.hide("this", "Date_Option_Custom")
        gui.show("this", "Time_Viewer")
    } else if (time_selector=="monthly"){
        gui.hide("this", "Date_Option_FinancialYear")
        gui.hide("this", "Date_Option_Annually")
        gui.hide("this", "Date_Option_Quarterly")
        gui.show("this", "Date_Option_Monthly")
        gui.hide("this", "Date_Option_Weekly")
        gui.hide("this", "Date_Option_Custom")
        gui.show("this", "Time_Viewer")
    } else if (time_selector=="weekly"){
        gui.hide("this", "Date_Option_FinancialYear")
        gui.hide("this", "Date_Option_Annually")
        gui.hide("this", "Date_Option_Quarterly")
        gui.hide("this", "Date_Option_Monthly")
        gui.show("this", "Date_Option_Weekly")
        gui.hide("this", "Date_Option_Custom")
        gui.disable("this", "Date_Option_Custom")
        gui.hide("this", "Time_Viewer")
    } else if (time_selector=="custom"){
        gui.hide("this", "Date_Option_FinancialYear")
        gui.hide("this", "Date_Option_Annually")
        gui.hide("this", "Date_Option_Quarterly")
        gui.hide("this", "Date_Option_Monthly")
        gui.hide("this", "Date_Option_Weekly")
        gui.disable("this", "Date_Option_Weekly")
        gui.show("this", "Date_Option_Custom")
        gui.hide("this", "Time_Viewer")
    }

    # Adds selection choices based on time frequency selected
    if (time_selector=="financial_year"){
        gui.clearChoices("this","FinanicalYear_Selector1")
        gui.addChoices("this","FinanicalYear_Selector1",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
        gui.clearChoices("this","FinanicalYear_Selector2")
        gui.addChoices("this","FinanicalYear_Selector2",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
    # Extracts years from data
    } else if (time_selector=="annually"){
        gui.clearChoices("this","Annually_Selector1")
        gui.addChoices("this","Annually_Selector1",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
        gui.clearChoices("this","Annually_Selector2")
        gui.addChoices("this","Annually_Selector2",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
    # Extracts years from data and adds quarterly option 
    } else if (time_selector=="quarterly"){
        gui.clearChoices("this","Quarterly_Year_Selector1")
        gui.addChoices("this","Quarterly_Year_Selector1",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
        gui.clearChoices("this","Quarterly_Quarter_Selector1")
        gui.addChoices("this","Quarterly_Quarter_Selector1",
                        list("Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"), 
                        list("Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"))
        gui.clearChoices("this","Quarterly_Year_Selector2")
        gui.addChoices("this","Quarterly_Year_Selector2",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
                gui.clearChoices("this","Quarterly_Quarter_Selector2")
        gui.addChoices("this","Quarterly_Quarter_Selector2",
                        list("Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"), 
                        list("Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"))
    # Extracts years and months from data
    } else if (time_selector=="monthly"){
        gui.clearChoices("this","Monthly_Year_Selector1")
        gui.addChoices("this","Monthly_Year_Selector1",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
        gui.clearChoices("this","Monthly_Month_Selector1")
        gui.addChoices("this","Monthly_Month_Selector1",unique((Date_Range %>% arrange(Month))$Month_Name), 
                                                        unique((Date_Range %>% arrange(Month))$Month_Name))
        gui.clearChoices("this","Monthly_Year_Selector2")
        gui.addChoices("this","Monthly_Year_Selector2",sort(unique(Date_Range$Year), decreasing=TRUE), sort(unique(Date_Range$Year), decreasing=TRUE))
        gui.clearChoices("this","Monthly_Month_Selector2")
        gui.addChoices("this","Monthly_Month_Selector2",unique((Date_Range %>% arrange(Month))$Month_Name), 
                                                        unique((Date_Range %>% arrange(Month))$Month_Name))
    }

}

# Get start and end dates for period 1 and 2 based on frequency selected 
get_time <- function(){

    time_selector <- Time_Selector

    ### DATE RANGE TABLE ###
    # Table of dates from 2019-01-01 to today and extract month, year and day 
    Date_Range <- as.data.frame(seq(as.Date("2019-01-01"), Sys.Date(),"days")) %>% setNames(c("Invoice_date")) 
    Date_Range <- Date_Range %>% 
    mutate(Year = as.numeric(format(Date_Range$Invoice_date,"%Y")),
            Month = as.numeric(format(Date_Range$Invoice_date,"%m")),
            Day = as.numeric(format(Date_Range$Invoice_date,"%d"))) 
    Date_Range <- Date_Range %>% 
    mutate(Month_Name = month.name[Date_Range$Month])

    if (time_selector=="financial_year"){
        financialyear_selector1 <- FinanicalYear_Selector1
        financialyear_selector2 <- FinanicalYear_Selector2

        # Returns start date of financial year with year as input 
        fy_start <- function(start_year) {
            start_year <- as.numeric(start_year)
            start_date <- as.Date("0000-07-01")
            year(start_date) <- start_year
            return(start_date)
        }
        # Returns end date of financial year with year as input 
        fy_end <- function(start_year) {
            start_year <- as.numeric(start_year)
            end_date <- as.Date("0000-06-30")
            year(end_date) <- start_year + 1
            return(end_date)
        }

        start_time_p1 = fy_start(financialyear_selector1)
        end_time_p1 = fy_end(financialyear_selector1)
        start_time_p2 = fy_start(financialyear_selector2)
        end_time_p2 = fy_end(financialyear_selector2)

    } else if (time_selector=="annually"){

        annually_selector1 <- Annually_Selector1
        annually_selector2 <- Annually_Selector2

        # Returns start date of calendar year with year as input 
        year_start <- function(start_year) {
            start_year <- as.numeric(start_year)
            start_date <- as.Date("0000-01-01")
            year(start_date) <- start_year
            return(start_date)
        }
        # Returns end date of calendar year with year as input 
        year_end <- function(start_year) {
            start_year <- as.numeric(start_year)
            end_date <- as.Date("0000-12-31")
            year(end_date) <- start_year + 1
            return(end_date)
        }

        start_time_p1 = year_start(annually_selector1)
        end_time_p1 = year_end(annually_selector1)
        start_time_p2 = year_start(annually_selector2)
        end_time_p2 = year_end(annually_selector2)

    } else if (time_selector=="quarterly"){

        quarterly_year_selector1 <- Quarterly_Year_Selector1
        quarterly_quarter_selector1 <- Quarterly_Quarter_Selector1
        quarterly_year_selector2 <- Quarterly_Year_Selector2
        quarterly_quarter_selector2 <- Quarterly_Quarter_Selector2

        quarter_start <- function(yr, quarter) {
            yr = as.numeric(yr)
            quarter = as.character(quarter)
            if (quarter=="Quarter 1") {
                start_date <- as.Date("0000-07-01")
                year(start_date) <- yr
            } else if (quarter=="Quarter 2") { 
                start_date <- as.Date("0000-10-01")
                year(start_date) <- yr
            } else if (quarter=="Quarter 3") { 
                start_date <- as.Date("0000-01-01")
                year(start_date) <- yr + 1
            } else if (quarter=="Quarter 4") {
                start_date <- as.Date("0000-04-01")
                year(start_date) <- yr + 1
            }
            return(start_date) 
        }

        quarter_end <- function(yr, quarter) {
            yr = as.numeric(yr)
            quarter = as.character(quarter)
            if (quarter=="Quarter 1") {
                end_date <- as.Date("0000-09-30")
                year(end_date) <- yr
            } else if (quarter=="Quarter 2") { 
                end_date <- as.Date("0000-12-31")
                year(end_date) <- yr
            } else if (quarter=="Quarter 3") { 
                end_date <- as.Date("0000-03-31")
                year(end_date) <- yr + 1
            } else if (quarter=="Quarter 4") {
                end_date <- as.Date("0000-06-30")
                year(end_date) <- yr + 1
            }
            return(end_date) 
        }

        start_time_p1 = quarter_start(quarterly_year_selector1, quarterly_quarter_selector1)
        end_time_p1 = quarter_end(quarterly_year_selector1, quarterly_quarter_selector1)
        start_time_p2 = quarter_start(quarterly_year_selector2, quarterly_quarter_selector2)
        end_time_p2 = quarter_end(quarterly_year_selector2, quarterly_quarter_selector2)

    } else if (time_selector=="monthly"){

        monthly_year_selector1 <- Monthly_Year_Selector1
        monthly_month_selector1 <- Monthly_Month_Selector1
        monthly_year_selector2 <- Monthly_Year_Selector2
        monthly_month_selector2 <- Monthly_Month_Selector2

        month_start <- function(yr, month) {
            month = match(month, month.name)
            yr = as.numeric(yr)
            start_date = as.Date("0000-01-01")
            year(start_date) <- yr
            month(start_date) <- month
            return(start_date)
        }

        month_end <- function(month_start){
            month_start = as.Date(month_start)
            end_date = month_start %m+% months(1)
            end_date = end_date - 1
            return(end_date)
        }

        start_time_p1 <- month_start(monthly_year_selector1, monthly_month_selector1)
        start_time_p2 <- month_start(monthly_year_selector2, monthly_month_selector2)
        end_time_p1 <- month_end(start_time_p1)
        end_time_p2 <- month_end(start_time_p2)

    } else if (time_selector=="weekly"){

        weekly_start_selector1 <- Weekly_Start_Selector1
        weekly_start_selector2 <- Weekly_Start_Selector2
        
        weekly_start_selector1 <- as.Date(weekly_start_selector1)
        weekly_start_selector2 <- as.Date(weekly_start_selector2)
        weekly_end_selector1 = weekly_start_selector1 + 7
        weekly_end_selector2 = weekly_start_selector2 + 7

        gui.setValue("this","Weekly_End_Selector1", weekly_end_selector1) 
        gui.setValue("this","Weekly_End_Selector2", weekly_end_selector2) 

        start_time_p1 <- weekly_start_selector1
        start_time_p2 <- weekly_start_selector2
        end_time_p1 <- weekly_end_selector1
        end_time_p2 <- weekly_end_selector2

    } else if (time_selector=="custom"){

        start_time_p1 <- Custom_Start_Selector1
        start_time_p2 <- Custom_Start_Selector2
        end_time_p1 <- Custom_End_Selector1
        end_time_p2 <- Custom_End_Selector2
    }

    gui.setValue("this","Start_Time_P1",  as.character(start_time_p1)) 
    gui.setValue("this","End_Time_P1",  as.character(end_time_p1)) 
    gui.setValue("this","Start_Time_P2", as.character(start_time_p2)) 
    gui.setValue("this","End_Time_P2", as.character(end_time_p2)) 

}