#############################-START OF DATE_RANGE_FILTER() FUNCTION-##############################

# Function for filtering for desired date range
date_range_filter <- function(df, time1, time2){ 
  df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
  return(df1)
}

#############################-START OF GET_ALL_FILTERS() FUNCTION-##############################

# Creates a list of all distinct column values after filtering for time period selected
get_all_filters <- function(){

  # Initial data cleansing
  tm_test <- Full_Sales_Spark_Data %>% 
    # Only select relevant columns
    select(Customer_No, Customer_Name, Customer_Type, Key_Account, 
           Account_Manager, Invoice_date, Week_Start_Date, Item_Code,
           Supplier_Name, Item_Description, Item_Category, Site, Site_State,
           Site_Name, Brand) %>%
    # Handling NAs
    na.replace(Customer_No = "UNK",
               Customer_Name = "UNK",
               Customer_Type = "UNK",
               Key_Account = "UNK",
               Account_Manager = "UNK",
               Item_Code = "UNK",
               Supplier_Name = "UNK",
               Item_Description = "UNK",
               Item_Category = "UNK",
               Site = "UNK",
               Site_State = "UNK",
               Site_Name = "UNK",
               Brand = "UNK")
  
  starttime1 <- Start_Time_P1
  endtime2 <- End_Time_P2
  
  tm_test <- date_range_filter(tm_test, starttime1, endtime2)

  # Obtaining list of distinct values for all filters
  Customer_No_Master <- c("All", (tm_test %>% select(Customer_No) %>% distinct() %>% arrange(Customer_No) %>% pull))
  assign("Customer_No_Master", Customer_No_Master, envir = .GlobalEnv)
  Customer_Name_Master <- c("All", (tm_test %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name) %>% pull))
  assign("Customer_Name_Master", Customer_Name_Master, envir = .GlobalEnv)
  Customer_Type_Master <- c("All", (tm_test %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type) %>% pull))
  assign("Customer_Type_Master", Customer_Type_Master, envir = .GlobalEnv)
  Key_Account_Master <- c("All", (tm_test %>% select(Key_Account) %>% distinct() %>% arrange(Key_Account) %>% pull))
  assign("Key_Account_Master", Key_Account_Master, envir = .GlobalEnv)
  Account_Manager_Master <- c("All", (tm_test %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager) %>% pull))
  assign("Account_Manager_Master", Account_Manager_Master, envir = .GlobalEnv)
  Item_Code_Master <- c("All", (tm_test %>% select(Item_Code) %>% distinct() %>% arrange(Item_Code) %>% pull))
  assign("Item_Code_Master", Item_Code_Master, envir = .GlobalEnv)
  Supplier_Name_Master <- c("All", (tm_test %>% select(Supplier_Name) %>% distinct() %>% arrange(Supplier_Name) %>% pull))
  assign("Supplier_Name_Master", Supplier_Name_Master, envir = .GlobalEnv)
  Item_Description_Master <- c("All", (tm_test %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description) %>% pull))
  assign("Item_Description_Master", Item_Description_Master, envir = .GlobalEnv)
  Item_Category_Master <- c("All", (tm_test %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category) %>% pull))
  assign("Item_Category_Master", Item_Category_Master, envir = .GlobalEnv)
  Site_Master <- c("All", (tm_test %>% select(Site) %>% distinct() %>% arrange(Site) %>% pull))
  assign("Site_Master", Site_Master, envir = .GlobalEnv)
  Site_State_Master <- c("All", (tm_test %>% select(Site_State) %>% distinct() %>% arrange(Site_State) %>% pull))
  assign("Site_State_Master", Site_State_Master, envir = .GlobalEnv)
  Site_Name_Master <- c("All", (tm_test %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name) %>% pull))
  assign("Site_Name_Master", Site_Name_Master, envir = .GlobalEnv)
  Brand_Master <- c("All", (tm_test %>% select(Brand) %>% distinct() %>% arrange(Brand) %>% pull))
  assign("Brand_Master", Brand_Master, envir = .GlobalEnv)
  
}

#############################-START OF MARGIN_BREAKDOWN() FUNCTION-##############################

# Generates base table for margin_breakdown tab
margin_breakdown <- function(){
  
  # Initializing GUI filter selectors 
  if (counter_h == 0){
    margin_checkbox_1 <- "All"
    gui.setValue("this","Margin_Checkbox_1", "All") 
    margin_checkbox_2 <- "All"
    gui.setValue("this","Margin_Checkbox_2", "All") 
    margin_checkbox_3 <- "All"
    gui.setValue("this","Margin_Checkbox_3", "All") 
    margin_checkbox_4 <- "All"
    gui.setValue("this","Margin_Checkbox_4", "All") 
    assign("counter_h", 1, envir = .GlobalEnv) 
  } else {
    margin_checkbox_1 <- Margin_Checkbox_1
    margin_checkbox_2 <- Margin_Checkbox_2
    margin_checkbox_3 <- Margin_Checkbox_3
    margin_checkbox_4 <- Margin_Checkbox_4
  }
    
  # Inital data cleansing
  tm_test <- Full_Sales_Spark_Data %>% 
    # Only select relevant columns
    select(Customer_Type, Site_State, Account_Manager, Invoice_date, Site_Name,
           Qty, Sales_Inc_GST, COGS, Customer_Rebate, Sales_Claims, Promo_Claims) %>%
    # Handling NAs
    na.replace(Customer_Type = "UNK",
               Site_State = "UNK",
               Account_Manager = "UNK",
               Invoice_date = "UNK",  
               Site_Name = "UNK",  
               Qty = 0, 
               Sales_Inc_GST = 0, 
               COGS = 0, 
               Customer_Rebate = 0, 
               Sales_Claims = 0, 
               Promo_Claims = 0)
  
  # Obtaining list of distinct values for filters
  Site_State_List <- Site_State_Master
  Customer_Type_List <- Customer_Type_Master
  Site_Name_List <- Site_Name_Master
  Account_Manager_List <- Account_Manager_Master

  # Assigning values to filter GUI
  gui.clearChoices("this","Margin_Checkbox_1")
  gui.addChoices("this","Margin_Checkbox_1",Site_State_List, Site_State_List)
  gui.setChoiceText("this","Margin_Checkbox_1",Site_State_List[1],Site_State_List[1])
  
  gui.clearChoices("this","Margin_Checkbox_2")
  gui.addChoices("this","Margin_Checkbox_2",Customer_Type_List, Customer_Type_List)
  gui.setChoiceText("this","Margin_Checkbox_2",Customer_Type_List[1],Customer_Type_List[1])
  
  gui.clearChoices("this","Margin_Checkbox_3")
  gui.addChoices("this","Margin_Checkbox_3",Site_Name_List, Site_Name_List)
  gui.setChoiceText("this","Margin_Checkbox_3",Site_Name_List[1],Site_Name_List[1] )
  
  gui.clearChoices("this","Margin_Checkbox_4")
  gui.addChoices("this","Margin_Checkbox_4",Account_Manager_List, Account_Manager_List)
  gui.setChoiceText("this","Margin_Checkbox_4",Account_Manager_List[1],Account_Manager_List[1])
  
  # Add all selection to filters within TM breakdown 
  if ((margin_checkbox_1 =='All') | (is.null(margin_checkbox_1)) | (is.na(margin_checkbox_1))){
    sitestate_group = Site_State_List
  } else {
    sitestate_group <- margin_checkbox_1
  }
  
  if ((margin_checkbox_2 =='All') | (is.null(margin_checkbox_2)) | (is.na(margin_checkbox_2))){
    custtype_group = Customer_Type_List
  } else {
    custtype_group <- margin_checkbox_2
  }
  
  if ((margin_checkbox_3 =='All') | (is.null(margin_checkbox_3)) | (is.na(margin_checkbox_3))){
    sitename_group = Site_Name_List
  } else {
    sitename_group <- margin_checkbox_3
  }
  
  if ((margin_checkbox_4 =='All') | (is.null(margin_checkbox_2)) | (is.na(margin_checkbox_2))){
    accmanager_group = Account_Manager_List
  } else {
    accmanager_group <- margin_checkbox_4
  }
  
  # Filter dataframe based on user-selected GUI filters
  tm_test <- tm_test %>%
    filter(Site_State %in% sitestate_group, 
           Customer_Type %in% custtype_group,
           Site_Name %in% sitename_group,
           Account_Manager %in% accmanager_group)
  
  # Obtaining updated list of distinct values for filters based on previous selection
  Site_State_List <- c("All", (tm_test %>% select(Site_State) %>% distinct() %>% arrange(Site_State) %>% pull))
  Customer_Type_List <- c("All", (tm_test %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type) %>% pull))
  Site_Name_List <- c("All", (tm_test %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name) %>% pull))
  Account_Manager_List <- c("All", (tm_test %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager) %>% pull))
  
  # Filtering by selected date range
  date_range_filter <- function(df, time1, time2){ 
    df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
    return(df1)
  }
  
  starttime1 <- Start_Time_P1
  endtime1 <- End_Time_P1
  starttime2 <- Start_Time_P2
  endtime2 <- End_Time_P2
  
  tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
  tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)
  
  # Calculate trading margin
  calculate_TM <- function(df){
    df1 <- df %>% 
      summarise(Qty=sum(Qty),
                Sales_Inc_GST=sum(Sales_Inc_GST),
                COGS=sum(COGS),
                Customer_Rebate=sum(Customer_Rebate),
                Sales_Claims=sum(Sales_Claims), 
                Promo_Claims=sum(Promo_Claims)) %>% 
      mutate(Selling_Price = (Sales_Inc_GST)/Qty,
             COGS_u = COGS/Qty,
             Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
             TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
             Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
             TM_percent = ifelse(is.na(TM_percent), 0, TM_percent)) %>%
      ungroup()
    return(df1)
  }
  
  tm_test_1_item <- calculate_TM(tm_test_1)
  tm_test_2_item <- calculate_TM(tm_test_2)
  
  
  # Compare periods
  compare_periods <- function(df1, df2){
    tm <- df1 %>% 
      # Add _1 suffix for first period df
      rename_all(function(x) paste0(x,"_1")) %>%
      # Bind to second period df and add _2 suffix
      sdf_bind_cols(df2 %>% rename_all(function(x) paste0(x,"_2"))) %>%
      mutate(Change_in_cost =  COGS_2 - COGS_1,
             Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
             Change_in_sell = Selling_Price_2 - Selling_Price_1,
             Change_in_COGS_u = COGS_u_2 - COGS_u_1,
             Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
             Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
             Change_in_promo = Promo_Claims_2 - Promo_Claims_1,
             Change_in_qty = Qty_2 - Qty_1,
             TM_change = Trading_Margin_2-Trading_Margin_1) %>%
      mutate(Price_eff = Change_in_sell*Qty_1,
             COGS_eff = -Change_in_COGS_u*Qty_1,
             Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
             Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2)) 
    return(tm)
  }
  
  df_1_vs_2 <- compare_periods(tm_test_1_item, tm_test_2_item)
  
  assign("sales_margin_breakdown_v2", collect(df_1_vs_2), envir = .GlobalEnv)

}

#############################-START OF RESET_MARGIN_FILTERS() FUNCTION-##############################

# Resets all filter GUIs to 'All'
reset_margin_filters <- function(){
  
  # Initial data cleansing
  tm_test <- Full_Sales_Spark_Data %>% 
    # Only select relevant columns
    select(Customer_Type, Site_State, Account_Manager, Invoice_date, Site_Name,
           Qty, Sales_Inc_GST, COGS, Customer_Rebate, Sales_Claims, Promo_Claims) %>%
    # Handling NAs
    na.replace(Customer_Type = "UNK",
               Site_State = "UNK",
               Account_Manager = "UNK",
               Invoice_date = "UNK",  
               Site_Name = "UNK",  
               Qty = 0, 
               Sales_Inc_GST = 0, 
               COGS = 0, 
               Customer_Rebate = 0, 
               Sales_Claims = 0, 
               Promo_Claims = 0)
  
  # Obtaining list of distinct values for filters
  Site_State_List <- Site_State_Master
  Customer_Type_List <- Customer_Type_Master
  Site_Name_List <- Site_Name_Master
  Account_Manager_List <- Account_Manager_Master
  
  # For every press of reset filters button, resets selection of filters and displays 'All'
  gui.clearChoices("this","Margin_Checkbox_1")
  gui.addChoices("this","Margin_Checkbox_1",Site_State_List, Site_State_List)
  gui.setChoiceText("this","Margin_Checkbox_1",Site_State_List[1],Site_State_List[1])
  gui.setValue("this","Margin_Checkbox_1", "All") 
  
  gui.clearChoices("this","Margin_Checkbox_2")
  gui.addChoices("this","Margin_Checkbox_2",Customer_Type_List, Customer_Type_List)
  gui.setChoiceText("this","Margin_Checkbox_2",Customer_Type_List[1],Customer_Type_List[1])
  gui.setValue("this","Margin_Checkbox_2", "All") 
  
  gui.clearChoices("this","Margin_Checkbox_3")
  gui.addChoices("this","Margin_Checkbox_3",Site_Name_List, Site_Name_List)
  gui.setChoiceText("this","Margin_Checkbox_3",Site_Name_List[1],Site_Name_List[1] )
  gui.setValue("this","Margin_Checkbox_3", "All") 
  
  gui.clearChoices("this","Margin_Checkbox_4")
  gui.addChoices("this","Margin_Checkbox_4",Account_Manager_List, Account_Manager_List)
  gui.setChoiceText("this","Margin_Checkbox_4",Account_Manager_List[1],Account_Manager_List[1])
  gui.setValue("this","Margin_Checkbox_4", "All") 
  
}

#############################-START OF GET_MARGIN_FILTERS() FUNCTION-##############################

# Dynamically changes the filter GUI that are visible based on user selection 
get_margin_filters <- function(){
  
  # Initial data cleansing
  tm_test <- Full_Sales_Spark_Data %>% 
    # Only select relevant columns
    select(Customer_Type, Site_State, Account_Manager, Invoice_date, Site_Name,
           Qty, Sales_Inc_GST, COGS, Customer_Rebate, Sales_Claims, Promo_Claims) %>%
    # Handling NAs
    na.replace(Customer_Type = "UNK",
               Site_State = "UNK",
               Account_Manager = "UNK",
               Invoice_date = "UNK",  
               Site_Name = "UNK",  
               Qty = 0, 
               Sales_Inc_GST = 0, 
               COGS = 0, 
               Customer_Rebate = 0, 
               Sales_Claims = 0, 
               Promo_Claims = 0)
  
  starttime1 <- Start_Time_P1
  endtime1 <- End_Time_P1
  starttime2 <- Start_Time_P2
  endtime2 <- End_Time_P2
  
  tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
  tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)
  
  if (counter_f == 0){
    margin_checkbox_1 <- 'All'
    margin_checkbox_2 <- 'All'
    margin_checkbox_3 <- 'All'
    margin_checkbox_4 <- 'All'
    assign("counter_f", 1, envir = .GlobalEnv) 
  } else {
    margin_checkbox_1 <- Margin_Checkbox_1
    margin_checkbox_2 <- Margin_Checkbox_2
    margin_checkbox_3 <- Margin_Checkbox_3
    margin_checkbox_4 <- Margin_Checkbox_4
  }
  
  # Add all selection to filters within TM breakdown 
  if ((margin_checkbox_1 =='All') | (is.null(margin_checkbox_1)) | (is.na(margin_checkbox_1))){
    sitestate_group = Site_State_Master
  } else {
    sitestate_group <- margin_checkbox_1
  }
  
  if ((margin_checkbox_2 =='All') | (is.null(margin_checkbox_2)) | (is.na(margin_checkbox_2))){
    custtype_group = Customer_Type_Master
  } else {
    custtype_group <- margin_checkbox_2
  }
  
  if ((margin_checkbox_3 =='All') | (is.null(margin_checkbox_3)) | (is.na(margin_checkbox_3))){
    sitename_group = Site_Name_Master
  } else {
    sitename_group <- margin_checkbox_3
  }
  
  if ((margin_checkbox_4 =='All') | (is.null(margin_checkbox_2)) | (is.na(margin_checkbox_2))){
    accmanager_group = Account_Manager_Master
  } else {
    accmanager_group <- margin_checkbox_4
  }
  
  # Filter dataframe based on margin_checkbox selected groups
  tm_test <- tm_test %>%
    filter(Site_State %in% sitestate_group, 
           Customer_Type %in% custtype_group,
           Site_Name %in% sitename_group,
           Account_Manager %in% accmanager_group)
  
  # Obtaining updated list of distinct values for filters based on previous selection
  Site_State_List <- c("All", (tm_test %>% select(Site_State) %>% distinct() %>% arrange(Site_State) %>% pull))
  Customer_Type_List <- c("All", (tm_test %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type) %>% pull))
  Site_Name_List <- c("All", (tm_test %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name) %>% pull))
  Account_Manager_List <- c("All", (tm_test %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager) %>% pull))

  # Assigning values to filter GUI
  gui.clearChoices("this","Margin_Checkbox_1")
  gui.addChoices("this","Margin_Checkbox_1",Site_State_List, Site_State_List)
  gui.setChoiceText("this","Margin_Checkbox_1",Site_State_List[1],Site_State_List[1])
  
  gui.clearChoices("this","Margin_Checkbox_2")
  gui.addChoices("this","Margin_Checkbox_2",Customer_Type_List, Customer_Type_List)
  gui.setChoiceText("this","Margin_Checkbox_2",Customer_Type_List[1],Customer_Type_List[1])
  
  gui.clearChoices("this","Margin_Checkbox_3")
  gui.addChoices("this","Margin_Checkbox_3",Site_Name_List, Site_Name_List)
  gui.setChoiceText("this","Margin_Checkbox_3",Site_Name_List[1],Site_Name_List[1] )
  
  gui.clearChoices("this","Margin_Checkbox_4")
  gui.addChoices("this","Margin_Checkbox_4",Account_Manager_List, Account_Manager_List)
  gui.setChoiceText("this","Margin_Checkbox_4",Account_Manager_List[1],Account_Manager_List[1])
  
}