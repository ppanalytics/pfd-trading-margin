#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                       PRICING REASONS WATERFALL                         #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


# Adapted from Drill_Down_2 function
pricing_reasons <- function(){

    # Reset filters upon first run of code for Pricing Reasons tab
    if (counter_j == 0){
        reason_custtype_selector <- 'All'
        gui.setValue("this","Reason_CustType_Selector", "All")   
        reason_state_selector <- 'All'
        gui.setValue("this","Reason_State_Selector", "All")   
        reason_sitename_selector <- 'All'
        gui.setValue("this","Reason_SiteName_Selector", "All")   
        reason_accmanager_selector <- 'All'
        gui.setValue("this","Reason_AccManager_Selector", "All")   
        reason_customer_selector <- 'All'
        gui.setValue("this","Reason_Customer_Selector", "All")   
        reason_itmcategory_selector <- 'All'
        gui.setValue("this","Reason_ItmCategory_Selector", "All")   
        reason_item_selector <- 'All'
        gui.setValue("this","Reason_Item_Selector", "All")   
        assign("counter_j", 1, envir = .GlobalEnv) 
    } else {
        reason_custtype_selector <- Reason_CustType_Selector
        reason_state_selector <- Reason_State_Selector
        reason_sitename_selector <- Reason_SiteName_Selector
        reason_accmanager_selector <- Reason_AccManager_Selector
        reason_customer_selector <- Reason_Customer_Selector
        reason_itmcategory_selector <- Reason_ItmCategory_Selector
        reason_item_selector <- Reason_Item_Selector
    }

    tm_test <- Complete_Sales_Data %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(
            Item_Description = gsub('#|[*]', '', Item_Description),
            Customer_Name = gsub("^\\*", "", Customer_Name)) 

    #' *Filter for desired date range*
    date_range_filter <- function(df, time1, time2){
        df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)
        return(df1)
    }



    starttime1 <- Start_Time_P1
    starttime2 <- Start_Time_P2
    endtime1 <- End_Time_P1
    endtime2 <- End_Time_P2

        
    tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
    tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)

    tm_test_temp <- tm_test_1 %>% rbind(tm_test_2)

    
    if (reason_custtype_selector=='All' | is.null(reason_custtype_selector) | is.na(reason_custtype_selector)){
        custtype_group = (tm_test_temp %>% select(Customer_Type) %>% distinct())$Customer_Type
    } else {
        custtype_group <- reason_custtype_selector
    }
    
    if (reason_state_selector=='All' | is.null(reason_state_selector) | is.na(reason_state_selector)){
        state_group = (tm_test_temp %>% select(Site_State) %>% distinct())$Site_State
    } else {
        state_group <- reason_state_selector
    }
    
    if (reason_sitename_selector=='All' | is.null(reason_sitename_selector) | is.na(reason_sitename_selector)){
        sitename_group = (tm_test_temp %>% select(Site_Name) %>% distinct())$Site_Name
    } else {
        sitename_group <- reason_sitename_selector
    }
    
    if (reason_accmanager_selector=='All' | is.null(reason_accmanager_selector) | is.na(reason_accmanager_selector)){
        accmanager_group = (tm_test_temp %>% select(Account_Manager) %>% distinct())$Account_Manager
    } else {
        accmanager_group <- reason_accmanager_selector
    }
    
    if (reason_customer_selector=='All' | is.null(reason_customer_selector) | is.na(reason_customer_selector)){
        customer_group = (tm_test_temp %>% select(Customer_Name) %>% distinct())$Customer_Name
    } else {
        customer_group <- reason_customer_selector
    }
    
    if (reason_itmcategory_selector=='All' | is.null(reason_itmcategory_selector) | is.na(reason_itmcategory_selector)){
        itmcategory_group = (tm_test_temp %>% select(Item_Category) %>% distinct())$Item_Category
    } else {
        itmcategory_group <- reason_itmcategory_selector
    }
    
    if (reason_item_selector=='All' | is.null(reason_item_selector) | is.na(reason_item_selector)){
        item_group = (tm_test_temp %>% select(Item_Description) %>% distinct())$Item_Description
    } else {
        item_group <- reason_item_selector
    }

    tm_test_1 <- tm_test_1 %>%
        filter(Customer_Type %in% custtype_group,
                Site_State %in% state_group,
                Site_Name %in% sitename_group,
                Account_Manager %in% accmanager_group,
                Customer_Name %in% customer_group,
                Item_Category %in% itmcategory_group,
                Item_Description %in% item_group)

    tm_test_2 <- tm_test_2 %>%
        filter(Customer_Type %in% custtype_group,
                Site_State %in% state_group,
                Site_Name %in% sitename_group,
                Account_Manager %in% accmanager_group,
                Customer_Name %in% customer_group,
                Item_Category %in% itmcategory_group,
                Item_Description %in% item_group)

    #' *Calculate trading margin*
    calculate_TM <- function(df){
        df1 <- df %>% 
            group_by(Customer_Type, Site_State, Site_Name, Account_Manager, 
                     Customer_Name, Item_Category, Item_Description) %>%
            summarise(Qty=sum(Qty),
                        Sales_Inc_GST=sum(Sales_Inc_GST),
                        COGS=sum(COGS),
                        Customer_Rebate=sum(Customer_Rebate),
                        Sales_Claims=sum(Sales_Claims), 
                        Promo_Claims=sum(Promo_Claims)) %>% 
            mutate(Selling_Price = (Sales_Inc_GST)/Qty,
                    Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
                    Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate),
                    Net_Cost_Price = COGS/Qty) %>%
            mutate(Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims) %>%
            mutate(TM_percent = Trading_Margin/Sales_Inc_GST*100)
            # mutate(Selling_Price = (Sales_Inc_GST)/Qty,
            #         # COGS_u = COGS/Qty,
            #         Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
            #         Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
            #         TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
            #         TM_percent = ifelse(is.na(TM_percent), 0, TM_percent),
            #         # Additional formula from excel sheet "Customer Example" provided by PFD
            #         Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate),
            #         Net_Cost_Price = COGS/Qty)
        return(df1)
        
    }

    tm_test_1_item <- calculate_TM(tm_test_1)
    tm_test_2_item <- calculate_TM(tm_test_2)


    compare_periods <- function(df1, df2){
      tm <- df1 %>% 
        full_join(df2, 
                    by=c("Customer_Type",
                         "Site_State",
                         "Site_Name",
                         "Account_Manager",
                         "Customer_Name",
                         "Item_Category",
                         "Item_Description"),
                    suffix=c("_1","_2")) %>%
        ungroup() %>%
        # rename_all(function(x) paste0(x,"_1")) %>%
        # bind_cols(df2 %>% rename_all(function(x) paste0(x,"_2"))) %>% 
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(Change_in_cost =  COGS_2 - COGS_1,
                Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
                Change_in_sell = Selling_Price_2 - Selling_Price_1,
                Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
                Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
                Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
                Change_in_qty = Qty_2 - Qty_1,
                TM_change = Trading_Margin_2-Trading_Margin_1,
                TM_percent_change = TM_percent_2 - TM_percent_1,
                #  Additional formula from excel sheet "Customer Example" provided by PFD
                Net_Selling_Price_eff = Net_Selling_Price_2/Qty_2 - Net_Selling_Price_1/Qty_1,
                Net_Cost_Price_eff = COGS_2/Qty_2 - COGS_1/Qty_1) %>% 
        mutate(Net_TM_impact = ifelse((Qty_1!=0 & Qty_2!=0), 
                                        ((Net_Selling_Price_eff - Net_Cost_Price_eff) * Qty_2),
                                        Trading_Margin_2 - Trading_Margin_1)) %>%
        # mutate(Change_in_cost =  COGS_2 - COGS_1,
        #        Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
        #        Change_in_sell = Selling_Price_2 - Selling_Price_1,
        #        Change_in_COGS_u = COGS_u_2 - COGS_u_1,
        #        Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
        #        Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
        #        Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
        #        Change_in_qty = Qty_2 - Qty_1,
        #        TM_change = Trading_Margin_2-Trading_Margin_1,
        #        TM_percent_change = TM_percent_2 - TM_percent_1,
        #         #  Additional formula from excel sheet "Customer Example" provided by PFD
        #        Change_in_net_selling_price = Net_Selling_Price_2/Qty_2 - Net_Selling_Price_1/Qty_1, 
        #        Change_in_net_cost_price = Net_Cost_Price_2 - Net_Cost_Price_1) %>% 
        # # Additional formula from excel sheet "Customer Example" provided by PFD
        # # Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
        # # mutate(Net_Selling_Price_eff = (Net_Selling_Price_2/Qty_2)-(Net_Selling_Price_1/Qty_1),
        # mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
        #        Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>%
        # mutate(Net_TM_impact = (Net_Selling_Price_eff - Net_Cost_Price_eff)*Qty_2) %>%
        # # mutate(Price_eff = Change_in_sell*Qty_1,
        # #      COGS_eff = -Change_in_COGS_u*Qty_1,
        # #      Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
        # #      Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2),
        # # filter(!(Qty_1==0 & Qty_2==0)) %>%
        arrange(Change_in_qty)
        return(tm)
    }

    df_drilldown_comparison <- compare_periods(tm_test_1_item, tm_test_2_item) %>%
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
        mutate_if(is.numeric, ~round(., 2)) 

    assign("pricing_reasons_table", df_drilldown_comparison, envir = .GlobalEnv)
}

get_reason_filters <- function(){


    tm_test_temp <- Complete_Sales_Data %>%
        # select(-Column_1) %>% 
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(
            Item_Description = gsub('#|[*]', '', Item_Description),
            Customer_Name = gsub("^\\*", "", Customer_Name)) 
        #     %>%
        # filter(Item_Code!='TRAILER', Item_Code!='MEMO')

    starttime1 <- Start_Time_P1
    starttime2 <- Start_Time_P2
    endtime1 <- End_Time_P1
    endtime2 <- End_Time_P2

    #' *Filter for desired date range*
    date_range_filter <- function(df, time1, time2){

        # df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
        df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)


        return(df1)
        
    }

    tm_test_temp_1 <- date_range_filter(tm_test_temp, starttime1, endtime1)
    tm_test_temp_2 <- date_range_filter(tm_test_temp, starttime2, endtime2)

    tm_test_temp <- tm_test_temp_1 %>% rbind(tm_test_temp_2)


    if (counter_k == 0){
        reason_custtype_selector <- 'All'
        reason_state_selector <- 'All'
        reason_sitename_selector <- 'All'
        reason_accmanager_selector <- 'All'
        reason_customer_selector <- 'All'
        reason_itmcategory_selector <- 'All'
        reason_item_selector <- 'All'
        assign("counter_k", 1, envir = .GlobalEnv) 
    } else {
        reason_custtype_selector <- Reason_CustType_Selector
        reason_state_selector <- Reason_State_Selector
        reason_sitename_selector <- Reason_SiteName_Selector
        reason_accmanager_selector <- Reason_AccManager_Selector
        reason_customer_selector <- Reason_Customer_Selector
        reason_itmcategory_selector <- Reason_ItmCategory_Selector
        reason_item_selector <- Reason_Item_Selector
    }




    if (reason_custtype_selector=='All' | is.null(reason_custtype_selector) | is.na(reason_custtype_selector)){
        custtype_group = (tm_test_temp %>% select(Customer_Type) %>% distinct())$Customer_Type
    } else {
        custtype_group <- reason_custtype_selector
    }
    
    if (reason_state_selector=='All' | is.null(reason_state_selector) | is.na(reason_state_selector)){
        state_group = (tm_test_temp %>% select(Site_State) %>% distinct())$Site_State
    } else {
        state_group <- reason_state_selector
    }
    
    if (reason_sitename_selector=='All' | is.null(reason_sitename_selector) | is.na(reason_sitename_selector)){
        sitename_group = (tm_test_temp %>% select(Site_Name) %>% distinct())$Site_Name
    } else {
        sitename_group <- reason_sitename_selector
    }
    
    if (reason_accmanager_selector=='All' | is.null(reason_accmanager_selector) | is.na(reason_accmanager_selector)){
        accmanager_group = (tm_test_temp %>% select(Account_Manager) %>% distinct())$Account_Manager
    } else {
        accmanager_group <- reason_accmanager_selector
    }
    
    if (reason_customer_selector=='All' | is.null(reason_customer_selector) | is.na(reason_customer_selector)){
        customer_group = (tm_test_temp %>% select(Customer_Name) %>% distinct())$Customer_Name
    } else {
        customer_group <- reason_customer_selector
    }
    
    if (reason_itmcategory_selector=='All' | is.null(reason_itmcategory_selector) | is.na(reason_itmcategory_selector)){
        itmcategory_group = (tm_test_temp %>% select(Item_Category) %>% distinct())$Item_Category
    } else {
        itmcategory_group <- reason_itmcategory_selector
    }
    
    if (reason_item_selector=='All' | is.null(reason_item_selector) | is.na(reason_item_selector)){
        item_group = (tm_test_temp %>% select(Item_Description) %>% distinct())$Item_Description
    } else {
        item_group <- reason_item_selector
    }

    tm_test_temp <- tm_test_temp %>%
        filter(Customer_Type %in% custtype_group,
                Site_State %in% state_group,
                Site_Name %in% sitename_group,
                Account_Manager %in% accmanager_group,
                Customer_Name %in% customer_group,
                Item_Category %in% itmcategory_group,
                Item_Description %in% item_group)


    # Pre-populate filters
    Customer_Type_List <- data.frame("Customer_Type" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type))
    Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
    Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name))
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))
    Customer_Name_List <- data.frame("Customer_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))
    Item_Category_List <- data.frame("Item_Category" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category))
    Item_Description_List <- data.frame("Item_Description" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description))


    gui.clearChoices("this","Reason_CustType_Selector")
    gui.addChoices("this","Reason_CustType_Selector",Customer_Type_List$Customer_Type, Customer_Type_List$Customer_Type)
    gui.setChoiceText("this","Reason_CustType_Selector",Customer_Type_List[1,"Customer_Type"],Customer_Type_List[1,"Customer_Type"] ) 

    gui.clearChoices("this","Reason_State_Selector")
    gui.addChoices("this","Reason_State_Selector",Site_State_List$Site_State, Site_State_List$Site_State)
    gui.setChoiceText("this","Reason_State_Selector",Site_State_List[1,"Site_State"],Site_State_List[1,"Site_State"] ) 

    gui.clearChoices("this","Reason_SiteName_Selector")
    gui.addChoices("this","Reason_SiteName_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)
    gui.setChoiceText("this","Reason_SiteName_Selector",Site_Name_List[1,"Site_Name"],Site_Name_List[1,"Site_Name"] ) 

    gui.clearChoices("this","Reason_AccManager_Selector")
    gui.addChoices("this","Reason_AccManager_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)
    gui.setChoiceText("this","Reason_AccManager_Selector",Account_Manager_List[1,"Account_Manager"],Account_Manager_List[1,"Account_Manager"] ) 

    gui.clearChoices("this","Reason_Customer_Selector")
    gui.addChoices("this","Reason_Customer_Selector",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)
    gui.setChoiceText("this","Reason_Customer_Selector",Customer_Name_List[1,"Customer_Name"],Customer_Name_List[1,"Customer_Name"] ) 

    gui.clearChoices("this","Reason_ItmCategory_Selector")
    gui.addChoices("this","Reason_ItmCategory_Selector",Item_Category_List$Item_Category, Item_Category_List$Item_Category)
    gui.setChoiceText("this","Reason_ItmCategory_Selector",Item_Category_List[1,"Item_Category"],Item_Category_List[1,"Item_Category"] ) 

    gui.clearChoices("this","Reason_Item_Selector")
    gui.addChoices("this","Reason_Item_Selector",Item_Description_List$Item_Description, Item_Description_List$Item_Description)
    gui.setChoiceText("this","Reason_Item_Selector",Item_Description_List[1,"Item_Description"],Item_Description_List[1,"Item_Description"] ) 



}

# Adapted from reset_filters_2 function
reset_reason_filters <- function(){


    tm_test_temp <- Complete_Sales_Data %>%
        # select(-Column_1) %>% 
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
        mutate(
            Item_Description = gsub('#|[*]', '', Item_Description),
            Customer_Name = gsub("^\\*", "", Customer_Name)) 
        #     %>%
        # filter(Item_Code!='TRAILER', Item_Code!='MEMO')



    starttime1 <- Start_Time_P1
    starttime2 <- Start_Time_P2
    endtime1 <- End_Time_P1
    endtime2 <- End_Time_P2

    #' *Filter for desired date range*
    date_range_filter <- function(df, time1, time2){
        # df1 <- df %>% filter(Invoice_date >= time1 & Invoice_date <= time2)
        df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)
        return(df1)
    }
    

    tm_test_temp_1 <- date_range_filter(tm_test_temp, starttime1, endtime1)
    tm_test_temp_2 <- date_range_filter(tm_test_temp, starttime2, endtime2)

    tm_test_temp <- tm_test_temp_1 %>% rbind(tm_test_temp_2)

    # Pre-populate filters
    Customer_Type_List <- data.frame("Customer_Type" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type))
    Site_State_List <- data.frame("Site_State" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
    Site_Name_List <- data.frame("Site_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Site_Name) %>% distinct() %>% arrange(Site_Name))
    Account_Manager_List <- data.frame("Account_Manager" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))
    Customer_Name_List <- data.frame("Customer_Name" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))
    Item_Category_List <- data.frame("Item_Category" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category))
    Item_Description_List <- data.frame("Item_Description" = t(c("All"))) %>% 
        rbind(tm_test_temp %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description))


    # For every press of reset filters button, resets selection of filters and displays 'All'
    gui.clearChoices("this","Reason_CustType_Selector")
    gui.addChoices("this","Reason_CustType_Selector",Customer_Type_List$Customer_Type, Customer_Type_List$Customer_Type)
    gui.setChoiceText("this","Reason_CustType_Selector",Customer_Type_List[1,"Customer_Type"],Customer_Type_List[1,"Customer_Type"] ) 
    gui.setValue("this","Reason_CustType_Selector", "All") 

    gui.clearChoices("this","Reason_State_Selector")
    gui.addChoices("this","Reason_State_Selector",Site_State_List$Site_State, Site_State_List$Site_State)
    gui.setChoiceText("this","Reason_State_Selector",Site_State_List[1,"Site_State"],Site_State_List[1,"Site_State"] ) 
    gui.setValue("this","Reason_State_Selector", "All") 

    gui.clearChoices("this","Reason_SiteName_Selector")
    gui.addChoices("this","Reason_SiteName_Selector",Site_Name_List$Site_Name, Site_Name_List$Site_Name)
    gui.setChoiceText("this","Reason_SiteName_Selector",Site_Name_List[1,"Site_Name"],Site_Name_List[1,"Site_Name"] ) 
    gui.setValue("this","Reason_SiteName_Selector", "All") 

    gui.clearChoices("this","Reason_AccManager_Selector")
    gui.addChoices("this","Reason_AccManager_Selector",Account_Manager_List$Account_Manager, Account_Manager_List$Account_Manager)
    gui.setChoiceText("this","Reason_AccManager_Selector",Account_Manager_List[1,"Account_Manager"],Account_Manager_List[1,"Account_Manager"] ) 
    gui.setValue("this","Reason_AccManager_Selector", "All") 

    gui.clearChoices("this","Reason_Customer_Selector")
    gui.addChoices("this","Reason_Customer_Selector",Customer_Name_List$Customer_Name, Customer_Name_List$Customer_Name)
    gui.setChoiceText("this","Reason_Customer_Selector",Customer_Name_List[1,"Customer_Name"],Customer_Name_List[1,"Customer_Name"] ) 
    gui.setValue("this","Reason_Customer_Selector", "All") 

    gui.clearChoices("this","Reason_ItmCategory_Selector")
    gui.addChoices("this","Reason_ItmCategory_Selector",Item_Category_List$Item_Category, Item_Category_List$Item_Category)
    gui.setChoiceText("this","Reason_ItmCategory_Selector",Item_Category_List[1,"Item_Category"],Item_Category_List[1,"Item_Category"] ) 
    gui.setValue("this","Reason_ItmCategory_Selector", "All") 

    gui.clearChoices("this","Reason_Item_Selector")
    gui.addChoices("this","Reason_Item_Selector",Item_Description_List$Item_Description, Item_Description_List$Item_Description)
    gui.setChoiceText("this","Reason_Item_Selector",Item_Description_List[1,"Item_Description"],Item_Description_List[1,"Item_Description"] ) 
    gui.setValue("this","Reason_Item_Selector", "All") 
}





