#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                           IMPORTING FUNCTIONS                           #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 



### CHECK FILE PATH FUNCTION --------------------------------------------------
# Checks whether the file path received leads to a readable file type
checkFilePath <- function(){
    
    # if(!file_test("-f",file_path)){
    #     gui.setValue("this", "<div class='error-wrapper'> File Path does not lead to a file </div>")
    #     return(FALSE)
    # }
    # if(!tolower(tools::file_ext(file_path)) %in% c("csv","xls","xlsx")){
    #     gui.setValue("this", "<div class='error-wrapper'> Cannot read selected file type</div>")
    #     return(FALSE)
    # }
    return(TRUE)
}


###############################################################################
#################################-BASE TABLE-##################################
###############################################################################


### IMPORT FILE FUNCTION ------------------------------------------------------
# Controller for importing csv, xls and xlsx files into the analyser
importFile <- function(upload_type){
    
    result <- FALSE

    if(checkFilePath()){
        uploaded_file <- readInFile()
        uploadFile(uploaded_file, upload_type)

        result <- TRUE
    }

    # data_v2_medium <- read.csv(fname)

    # uploadFile(uploaded_file, upload_type)
    # result <- TRUE

    uploadProgressBar()
}

### UPLOAD FILE FUNCTION ------------------------------------------------------
# Visualises the uploaded file onto the analyser on the specific tab it was
# input. Also assigns the uploaded file as a global variable.
uploadFile <- function(uploaded_file, upload_type){

    if (upload_type == "Sales"){
        assign("Sales_Data", uploaded_file, envir = .GlobalEnv) 
        # gui.setValue("this","Sales_head", head(Sales_Data,50))
    }
        
}

### READ FILE FUNCTION --------------------------------------------------------
# Reads the file based on the specific type it is (csv or excel).
readInFile <- function(){
    file_read <- NA

    # if(tolower(tools::file_ext(file_path)) %in% c("xls","xlsx")){
    #     suppressMessages(try(file_read <- read_excel(file_path) , silent = TRUE))
    # }
    # else if (tolower(tools::file_ext(file_path)) %in% c("csv")){
    #     suppressMessages(try(file_read <- read_csv(file_path) , silent = TRUE))
    # }
    # endp <- AzureStor::storage_endpoint("https://rpgmstorageaccountdev.blob.core.windows.net", key="XJPj9cPYYqNPAbFJDblpzMCwq/H0kk161nPn4AvIFQJospI5BcXM2PF6ankQBrClIAH9lnu97pU0atYx1kGA0g==")
    # cont <- AzureStor::storage_container(endp, "rpgm")
    # fname <- tempfile()
    # AzureStor::storage_download(cont, "data_v2_medium.csv", fname)

    # suppressMessages(try(file_read <- read_csv(fname) , silent = TRUE))

    # file_read <- fiodbcValidChannelle_read %>% mutate(Invoice_date=as.Date(Invoice_date, format="%d/%m/%Y"))

    # # SQL server file for local version
    # connstr <- 'Driver={ODBC Driver 17 for SQL Server};
    #        Server=tcp:pfdtest.database.windows.net,1433; 
    #        Database=pfdtest_db;Uid=pfdadmin;Pwd=Admin1234!;
    #        Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;'

    # conn <- odbcDriverConnect(connstr)

    # file_read <- sqlQuery(conn, "SELECT TOP (400000) * FROM [dbo].[pfd_ma_condensed]")

    # # File for server version
    # file_read <- read_csv("/home/azureuser/static/CondensedTableExtract.csv")

    # Updated CSV extract
    file_read <- read_csv("C:/Users/gaov/Documents/1 PFD Trading Margin/Inputs/TMAggregatedBaseTableDecOnly5perc.csv")
    # file_read <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/Archive/TMAggregatedBaseTableDecOnly5perc.csv")
    # file_read <- read_csv("/home/azureuser/static/TMAggregatedBaseTableDecOnly.csv")

    return(file_read)
}


###############################################################################
#############################-SUMMARY BASE TABLE-##############################
###############################################################################

### READ FILE FUNCTION --------------------------------------------------------
# Reads the file based on the specific type it is (csv or excel).
readInFileSum <- function(){
    file_read_summary <- NA

    # Updated CSV extract
    file_read_summary <- read_csv("C:/Users/gaov/Documents/1 PFD Trading Margin/Inputs/TMAggregatedBaseTableSummary.csv")
    # file_read_summary <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/Archive/TMAggregatedBaseTableSummary.csv")
    # file_read <- read_csv("/home/azureuser/static/TMAggregatedBaseTableSummary.csv")


    return(file_read_summary)
}

### IMPORT FILE FUNCTION ------------------------------------------------------
# Controller for importing csv, xls and xlsx files into the analyser
importFileSum <- function(upload_type){
    
    result <- FALSE

    if(checkFilePath()){

        uploaded_file_summary <- readInFileSum()
        uploadFileSum(uploaded_file_summary, upload_type)

        result <- TRUE
    }
    uploadProgressBar()
}

### UPLOAD FILE FUNCTION ------------------------------------------------------
# Visualises the uploaded file onto the analyser on the specific tab it was
# input. Also assigns the uploaded file as a global variable.
uploadFileSum <- function(uploaded_file_summary, upload_type){

    if (upload_type == "Summary"){
        assign("Summary_Sales_Data", uploaded_file_summary, envir = .GlobalEnv) 
    }
        
}




###############################################################################
##############################- COMPLETE  TABLE -##############################
###############################################################################

### READ FILE FUNCTION --------------------------------------------------------
# Reads the file based on the specific type it is (csv or excel).
readInFileComplete <- function(){
    file_read_complete <- NA

    # Updated CSV extract
    file_read_complete <- read_csv("C:/Users/gaov/Documents/1 PFD Trading Margin/Inputs/TMBaseTableSample.csv")
    # file_read_complete <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/TMBaseTableSample.csv")
    # file_read_complete <- read_csv("/home/azureuser/static/TMBaseTableSample.csv")


    return(file_read_complete)
}

### IMPORT FILE FUNCTION ------------------------------------------------------
# Controller for importing csv, xls and xlsx files into the analyser
importFileComplete <- function(upload_type){
    
    result <- FALSE

    if(checkFilePath()){

        uploaded_file_complete <- readInFileComplete()
        uploadFileComplete(uploaded_file_complete, upload_type)

        result <- TRUE
    }
    uploadProgressBar()
}

### UPLOAD FILE FUNCTION ------------------------------------------------------
# Visualises the uploaded file onto the analyser on the specific tab it was
# input. Also assigns the uploaded file as a global variable.
uploadFileComplete <- function(uploaded_file_complete, upload_type){

    if (upload_type == "Complete"){
        assign("Complete_Sales_Data", uploaded_file_complete, envir = .GlobalEnv) 
    }
        
}






uploadProgressBar <- function(){
    upload_progress <- 0
    upload_progress <- round(50,0)
    gui.setValue("this","Upload_progess_1",upload_progress)
}









# Summary_Sales_Data <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/TMBaseTableSample.csv")
# Complete_Sales_Data <- read_csv("Z:/3. Confidential/PFD Food Services Pty Ltd/01. Trading Margin/03. Outputs/Data Export SQL/TMBaseTableSample.csv")
# Sales_Data <- read_csv("C:/Users/gaov/Documents/1 PFD Trading Margin/Inputs/TMAggregatedBaseTableDecOnly5perc.csv")
