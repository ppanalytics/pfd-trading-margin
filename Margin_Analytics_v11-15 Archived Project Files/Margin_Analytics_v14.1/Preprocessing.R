#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                    DATA CLEANING AND PREPROCESSING                      #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 


get_filters <- function() {

    # On first load of dashboard, assign filter variables as 'All'
    if (counter_1 == 0){
        custtype_filter_selector <- 'All'
        state_filter_selector <- 'All'
        sitename_filter_selector <- 'All'
        accmanager_filter_selector <- 'All'
        customer_filter_selector <- 'All'
        itmcategory_filter_selector <- 'All'
        # item_filter_selector <- 'All'
    assign("counter_1", 1, envir = .GlobalEnv) 
    } else {
        custtype_filter_selector <- CustType_Filter_Selector
        state_filter_selector <- State_Filter_Selector
        sitename_filter_selector <- SiteName_Filter_Selector
        accmanager_filter_selector <- AccManager_Filter_Selector
        customer_filter_selector <- Customer_Filter_Selector
        itmcategory_filter_selector <- ItmCategory_Filter_Selector
    }

    # Assign user-selected variables into global environment for filtering
    assign("custtype_filter_selector", custtype_filter_selector, envir = .GlobalEnv)
    assign("state_filter_selector", state_filter_selector, envir = .GlobalEnv)
    assign("sitename_filter_selector", sitename_filter_selector, envir = .GlobalEnv)
    assign("accmanager_filter_selector", accmanager_filter_selector, envir = .GlobalEnv)
    assign("customer_filter_selector", customer_filter_selector, envir = .GlobalEnv)
    assign("itmcategory_filter_selector", itmcategory_filter_selector, envir = .GlobalEnv)

}

reset_filters <- function() {

  gui.setValue("this","CustType_Filter_Selector", "All") 
  gui.setValue("this","State_Filter_Selector", "All") 
  gui.setValue("this","SiteName_Filter_Selector", "All") 
  gui.setValue("this","AccManager_Filter_Selector", "All") 
  gui.setValue("this","Customer_Filter_Selector", "All") 
  gui.setValue("this","ItmCategory_Filter_Selector", "All") 

}

# # Sparklyr version of data preprocessing
# base_table_preprocessing <- function() {
#     # Initial data cleansing
#     TM_Base_Table <- TM_Base_Table_Raw %>% 
#         # Only select relevant columns
#         select(Customer_Type, Site_State, Account_Manager, Invoice_date, Site_Name, 
#             Customer_Name, Item_Category, Item_Description,
#             Qty, Sales_Inc_GST, COGS, Customer_Rebate, Sales_Claims, Promo_Claims) %>%
#         # Handling NAs
#         na.replace(Customer_Type = "UNK",
#                 Site_State = "UNK",
#                 Account_Manager = "UNK",
#                 Invoice_date = "UNK",  
#                 Site_Name = "UNK",  
#                 Customer_Name = "UNK",  
#                 Item_Category = "UNK",  
#                 Item_Description = "UNK",  
#                 Qty = 0, 
#                 Sales_Inc_GST = 0, 
#                 COGS = 0, 
#                 Customer_Rebate = 0, 
#                 Sales_Claims = 0, 
#                 Promo_Claims = 0)

#     # Update progress bar
#     upload_progress <- round(10,0)
#     gui.setValue("this","Upload_progess_1",upload_progress)  
  
#     # Add all selection to filters within TM breakdown 
#     if (custtype_filter_selector=='All'){
#         # custtype_group = Customer_Type_Master
#         custtype_group <- as.character((Customer_Type_Master%>%rbind("UNK"))$Customer_Type)
#     } else {
#         custtype_group <- custtype_filter_selector
#     }
    
#     if (state_filter_selector=='All'){
#         # state_group = Site_State_Master
#         state_group <- as.character((Site_State_Master%>%rbind("UNK"))$Site_State)
#     } else {
#         state_group <- state_filter_selector
#     }
    
#     if (sitename_filter_selector=='All'){
#         # sitename_group = Site_Name_Master
#         sitename_group <- as.character((Site_Name_Master%>%rbind("UNK"))$Site_Name)
#     } else {
#         sitename_group <- sitename_filter_selector
#     }
    
#     if (accmanager_filter_selector=='All'){
#         # accmanager_group = Account_Manager_Master
#         accmanager_group <- as.character((Account_Manager_Master%>%rbind("UNK"))$Account_Manager)
#     } else {
#         accmanager_group <- accmanager_filter_selector
#     }
    
#     if (customer_filter_selector=='All'){
#         # customer_group = Customer_Name_Master
#         customer_group <- as.character((Customer_Name_Master%>%rbind("UNK"))$Customer_Name)
#     } else {
#         customer_group <- customer_filter_selector
#     }
    
#     if (itmcategory_filter_selector=='All'){
#         # itmcategory_group = Item_Category_Master
#         itmcategory_group <- as.character((Item_Category_Master%>%rbind("UNK"))$Item_Category)
#     } else {
#         itmcategory_group <- itmcategory_filter_selector
#     }
  
#     # Update progress bar
#     upload_progress <- round(20,0)
#     gui.setValue("this","Upload_progess_1",upload_progress)  
  
#     # Filter dataframe based on user-selected GUI filters
#     TM_Base_Table <- TM_Base_Table %>%
#         filter(Customer_Type %in% custtype_group,
#             Site_State %in% state_group,
#             Site_Name %in% sitename_group,
#             Account_Manager %in% accmanager_group,
#             Customer_Name %in% customer_group,
#             Item_Category %in% itmcategory_group)

#     # Update progress bar
#     upload_progress <- round(40,0)
#     gui.setValue("this","Upload_progess_1",upload_progress)  
  
#     starttime1 <- Start_Time_P1
#     endtime1 <- End_Time_P1
#     starttime2 <- Start_Time_P2
#     endtime2 <- End_Time_P2
  
#     tm_test_1 <- date_range_filter(TM_Base_Table, starttime1, endtime1)
#     tm_test_2 <- date_range_filter(TM_Base_Table, starttime2, endtime2)

#     # Update progress bar
#     upload_progress <- round(60,0)
#     gui.setValue("this","Upload_progess_1",upload_progress)  
  

#     assign("tm_test_1", tm_test_1, envir = .GlobalEnv)

#     # Update progress bar
#     upload_progress <- round(80,0)
#     gui.setValue("this","Upload_progess_1",upload_progress)  
  
#     assign("tm_test_2", tm_test_2, envir = .GlobalEnv)

      
#     # Update progress bar
#     upload_progress <- round(100,0)
#     gui.setValue("this","Upload_progess_1",upload_progress)  
  

# }

# Local R version of preprocessing function
base_table_preprocessing <- function() {
    # Initial data cleansing
    TM_Base_Table <- TM_Base_Table_Local %>% 
        # Only select relevant columns
        select(Customer_Type, Site_State, Account_Manager, Invoice_date, Site_Name, 
            Customer_Name, Item_Category, Item_Description,
            Qty, Sales_Inc_GST, COGS, Customer_Rebate, Sales_Claims, Promo_Claims) %>%
        # Handling NAs
        mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
        mutate_if(is.character, ~replace(., is.na(.), 'UNK'))

    # Update progress bar
    upload_progress <- round(10,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
  
    # Add all selection to filters within TM breakdown 
    if (custtype_filter_selector=='All'){
        # custtype_group = Customer_Type_Master
        custtype_group <- as.character((Customer_Type_Master%>%rbind("UNK"))$Customer_Type)
    } else {
        custtype_group <- custtype_filter_selector
    }
    
    if (state_filter_selector=='All'){
        # state_group = Site_State_Master
        state_group <- as.character((Site_State_Master%>%rbind("UNK"))$Site_State)
    } else {
        state_group <- state_filter_selector
    }
    
    if (sitename_filter_selector=='All'){
        # sitename_group = Site_Name_Master
        sitename_group <- as.character((Site_Name_Master%>%rbind("UNK"))$Site_Name)
    } else {
        sitename_group <- sitename_filter_selector
    }
    
    if (accmanager_filter_selector=='All'){
        # accmanager_group = Account_Manager_Master
        accmanager_group <- as.character((Account_Manager_Master%>%rbind("UNK"))$Account_Manager)
    } else {
        accmanager_group <- accmanager_filter_selector
    }
    
    if (customer_filter_selector=='All'){
        # customer_group = Customer_Name_Master
        customer_group <- as.character((Customer_Name_Master%>%rbind("UNK"))$Customer_Name)
    } else {
        customer_group <- customer_filter_selector
    }
    
    if (itmcategory_filter_selector=='All'){
        # itmcategory_group = Item_Category_Master
        itmcategory_group <- as.character((Item_Category_Master%>%rbind("UNK"))$Item_Category)
    } else {
        itmcategory_group <- itmcategory_filter_selector
    }
  
    # Update progress bar
    upload_progress <- round(20,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
  
    # Filter dataframe based on user-selected GUI filters
    TM_Base_Table <- TM_Base_Table %>%
        filter(Customer_Type %in% custtype_group,
            Site_State %in% state_group,
            Site_Name %in% sitename_group,
            Account_Manager %in% accmanager_group,
            Customer_Name %in% customer_group,
            Item_Category %in% itmcategory_group)

    # Update progress bar
    upload_progress <- round(40,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
  
    starttime1 <- Start_Time_P1
    endtime1 <- End_Time_P1
    starttime2 <- Start_Time_P2
    endtime2 <- End_Time_P2
  
    tm_test_1 <- date_range_filter(TM_Base_Table, starttime1, endtime1)
    tm_test_2 <- date_range_filter(TM_Base_Table, starttime2, endtime2)

    # Update progress bar
    upload_progress <- round(60,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
  

    assign("tm_test_1", tm_test_1, envir = .GlobalEnv)

    # Update progress bar
    upload_progress <- round(80,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
  
    assign("tm_test_2", tm_test_2, envir = .GlobalEnv)

      
    # Update progress bar
    upload_progress <- round(100,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
  

}

# Function to initate all graphs upon first click
initiate_graphs <- function() {
    # Initiate margin breakdown tab
    margin_breakdown_v2()
    updateMarginVisuals_v2() 
    upload_progress <- round(20,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
    # Initiate TM trends tab
    tm_trend_processing() 
    updateTrendVisuals() 
    update_trend_titles() 
    upload_progress <- round(40,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
    # Initiate detailed analysis tab
    detailed_analysis_function() 
    updateDetailedVisuals() 
    upload_progress <- round(60,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
    # Initiate pricing reasons tab
    reasons_grouping_function()
    updateReasonVisuals() 
    upload_progress <- round(80,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
    # Initiate basket analysis tab
    basket_preprocessing() 
    updateNewBasketVisuals()
    upload_progress <- round(100,0)
    gui.setValue("this","Upload_progess_1",upload_progress)  
    

}
