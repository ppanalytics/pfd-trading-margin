



# Function for Trading Margin Trends Tab
compare_the_pair <- function(df_raw){

  if (counter_i == 0){

    # Set filter checkboxes to All when dashboard is first loaded
    trend_checkbox_1 <- "All"
    gui.setValue("this","Trend_Checkbox_1", "All") 
    trend_checkbox_2 <- "All"
    gui.setValue("this","Trend_Checkbox_2", "All") 
    trend_checkbox_3 <- "All"
    gui.setValue("this","Trend_Checkbox_3", "All") 

    # Update graph titles when dashboard is first loaded
    gui.setValue("this","Perc_Increases", "<strong><center>Top Trading Margin % Change </strong>") 
    gui.setValue("this","Perc_Decreases", "<strong><center>Bottom Trading Margin % Change</strong> ") 
    gui.setValue("this","Dollar_Increases", "<strong><center>Top Trading Margin $ Change</strong> ") 
    gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom Trading Margin $ Change</strong> ") 

    assign("counter_i", 1, envir = .GlobalEnv) 
  } else {
    trend_checkbox_1 <- Trend_Checkbox_1
    trend_checkbox_2 <- Trend_Checkbox_2
    trend_checkbox_3 <- Trend_Checkbox_3
  }

  
  #' *Select relevant/Drop irelevant columns*
  tm_test <- df_raw %>% 
    # select(-Column_1) %>% 
    mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
    mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
    mutate(
      # Item_Description = gsub('#|[*]', '', Item_Description),
          Customer_Name = gsub("^\\*", "", Customer_Name)) 
    #       %>%
    # filter(Item_Code!='TRAILER', Item_Code!='MEMO')


  # Get the user-specified dates 
  starttime1 <- Start_Time_P1
  endtime1 <- End_Time_P1
  starttime2 <- Start_Time_P2
  endtime2 <- End_Time_P2

  # Filter for desired date range
  tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
  tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)

  # Combine data tables for 2 time periods
  # This is required for obtaining the full list of unique elements for each filter (step below)
  tm_test_temp <- tm_test_1 %>% bind_rows(tm_test_2)

  # Pre-populate filters
  State_Nm <-  tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State)
 
  State_Nm <- data.frame("Site_State" = t(c("All"))) %>% 
    rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
  Key_ac <- data.frame("Customer_Type" = t(c("All"))) %>% 
    rbind(tm_test_temp %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type))
  # Acc_Mn <- data.frame("Account_Manager" = t(c("None"))) %>% 
  #   rbind(tm_test_temp %>% select(Account_Manager) %>% distinct() %>% arrange(Account_Manager))
  Cus_Nm <- data.frame("Customer_Name" = t(c("All"))) %>% 
    rbind(tm_test_temp %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))
  # It_Cat <- data.frame("Item_Category" = t(c("None"))) %>% 
  #   rbind(tm_test_temp %>% select(Item_Category) %>% distinct() %>% arrange(Item_Category))
  # Br <- data.frame("Brand" = t(c("None"))) %>% 
  #   rbind(tm_test_temp %>% select(Brand) %>% distinct() %>% arrange(Brand))
  # It_Desc <- data.frame("Item_Description" = t(c("None"))) %>% 
  #   rbind(tm_test_temp %>% select(Item_Description) %>% distinct() %>% arrange(Item_Description))

  # # Add "All" option to state checkbox selector
  # State_Nm <- data.frame("Site_State" = t(c("All"))) %>% 
  # rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))  

  # For every individual run of the tool, clear each selector of previously selected choices
  # Add in new choices (source from combined table of 2 time periods)
  # For visual display purposes, show the first choice
  gui.clearChoices("this","Key_Acc")
  gui.addChoices("this","Key_Acc",Key_ac$Customer_Type, Key_ac$Customer_Type)
  gui.setChoiceText("this","Key_Acc",Key_ac[1,"Customer_Type"],Key_ac[1,"Customer_Type"] )  

  # gui.clearChoices("this","Account_Man")
  # gui.addChoices("this","Account_Man",Acc_Mn$Account_Manager, Acc_Mn$Account_Manager)
  # gui.setChoiceText("this","Account_Man",Acc_Mn[1,"Account_Manager"],Acc_Mn[1,"Account_Manager"] )  

  gui.clearChoices("this","Cust_Name")
  gui.addChoices("this","Cust_Name",Cus_Nm$Customer_Name, Cus_Nm$Customer_Name)
  gui.setChoiceText("this","Cust_Name",Cus_Nm[2,"Customer_Name"],Cus_Nm[2,"Customer_Name"] )  

  # gui.clearChoices("this","Item_Cat")
  # gui.addChoices("this","Item_Cat",It_Cat$Item_Category, It_Cat$Item_Category)
  # gui.setChoiceText("this","Item_Cat", It_Cat[1,"Item_Category"],It_Cat[1,"Item_Category"])  

  # gui.clearChoices("this","Item_Brand")
  # gui.addChoices("this","Item_Brand",Br$Brand, Br$Brand)
  # gui.setChoiceText("this","Item_Brand",Br[1,"Brand"],Br[1,"Brand"] )  

  # gui.clearChoices("this","Item_Desc")
  # gui.addChoices("this","Item_Desc",It_Desc$Item_Description, It_Desc$Item_Description)
  # gui.setChoiceText("this","Item_Desc",It_Desc[1,"Item_Description"],It_Desc[1,"Item_Description"] )  

  # gui.clearChoices("this","Customer_Selector")
  # gui.addChoices("this","Customer_Selector",Cus_Nm$Customer_Name, Cus_Nm$Customer_Name)
  # gui.setChoiceText("this","Customer_Selector",Cus_Nm[1,"Customer_Name"],Cus_Nm[1,"Customer_Name"])

  gui.clearChoices("this","Trend_Checkbox_1")
  gui.addChoices("this","Trend_Checkbox_1",State_Nm$Site_State, State_Nm$Site_State)
  gui.setChoiceText("this","Trend_Checkbox_1",State_Nm[1,"Site_State"],State_Nm[1,"Site_State"])

  gui.clearChoices("this","Trend_Checkbox_2")
  gui.addChoices("this","Trend_Checkbox_2",Key_ac$Customer_Type, Key_ac$Customer_Type)
  gui.setChoiceText("this","Trend_Checkbox_2",Key_ac[1,"Customer_Type"],Key_ac[1,"Customer_Type"] )

  gui.clearChoices("this","Trend_Checkbox_3")
  gui.addChoices("this","Trend_Checkbox_3",Cus_Nm$Customer_Name, Cus_Nm$Customer_Name)
  gui.setChoiceText("this","Trend_Checkbox_3",Cus_Nm[1,"Customer_Name"],Cus_Nm[1,"Customer_Name"])

  # This is the grouping variable (i.e. selector) for the trading margin trends dashboard
  filteroption1 <- Filter_Option_1


  # Add all selection to filters within TM trends 
  if (trend_checkbox_1 =='All' | is.null(trend_checkbox_1) | is.na(trend_checkbox_1)) {
    sitestate_group2 = (tm_test %>% select(Site_State) %>% distinct())$Site_State
  } else {
      sitestate_group2 <- trend_checkbox_1
  }

  if (trend_checkbox_2 =='All' | is.null(trend_checkbox_2) | is.na(trend_checkbox_2)){
    custtype_group2 = (tm_test %>% select(Customer_Type) %>% distinct())$Customer_Type
  } else {
      custtype_group2 <- trend_checkbox_2
  }

  if (trend_checkbox_3=='All' | is.null(trend_checkbox_3) | is.na(trend_checkbox_3)){
    custname_group2 = (tm_test %>% select(Customer_Name) %>% distinct())$Customer_Name
  } else {
      custname_group2 <- trend_checkbox_3
  }

  
  # Filter based on selection
  tm_test_1 <- tm_test_1 %>%
    filter(Site_State %in% sitestate_group2, 
             Customer_Type %in% custtype_group2,
             Customer_Name %in% custname_group2)

  tm_test_2 <- tm_test_2 %>%
    filter(Site_State %in% sitestate_group2, 
             Customer_Type %in% custtype_group2,
             Customer_Name %in% custname_group2)


  
  #' *Calculate trading margin for two selected time periods based on selected filter*
  calculate_TM <- function(df){
    df1 <- df %>% 
      # select(-Invoice_date) %>% 
      # Group by user-selected choice
      group_by_at(c(filteroption1)) %>% 
      # Obtain summary figures at the grouping level
      summarise(Qty=sum(Qty),
                Sales_Inc_GST=sum(Sales_Inc_GST),
                COGS=sum(COGS),
                Customer_Rebate=sum(Customer_Rebate),
                Sales_Claims=sum(Sales_Claims), 
                Promo_Claims=sum(Promo_Claims)) %>% 
      # Get a few more useful numbers
      # mutate(Selling_Price = (Sales_Inc_GST)/Qty,
      #        Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
      #        TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0))
      ######## UPDATED MUTATE FROM CALCULATE_TM IN MARGIN_BREAKDOWN() ###########
      mutate(Selling_Price = (Sales_Inc_GST)/Qty,
             Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
             TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
             Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
             COGS_u = COGS/Qty,
             TM_percent = ifelse(is.na(TM_percent), 0, TM_percent))
    return(df1)   
  }
  
  # Get summarised data tables for both time periods
  tm_test_1_item <- calculate_TM(tm_test_1)
  tm_test_2_item <- calculate_TM(tm_test_2)

  #' *Compare two selected time periods side-by-side*
  compare_periods <- function(df1, df2){
    tm <- df1 %>% 
      # Full join so nothing is missed
      full_join(df2, 
                by=unique(c(filteroption1)),
                suffix=c("_1","_2")) %>%
      ungroup() %>%
      mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
      mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
      ############ ORIGINAL MUTATE FOR COMPARE_THE_PAIR() #############
      # Computing changes between 2 periods
      # mutate(Change_in_cost = ifelse(Qty_1!=0 & Qty_2!=0, COGS_2/Qty_2 - COGS_1/Qty_1,0),
      #        Change_in_sell = Selling_Price_2 - Selling_Price_1,
      #        Change_in_claim = ifelse(Qty_1!=0 & Qty_2!=0, Sales_Claims_2/Qty_2 - Sales_Claims_1/Qty_1,  0),
      #        Change_in_rebate = ifelse(Qty_1!=0 & Qty_2!=0, Customer_Rebate_2/Qty_2 - Customer_Rebate_1/Qty_1,  0),
      #        Change_in_promo = ifelse(Qty_1!=0 & Qty_2!=0, Promo_Claims_2/Qty_2 - Promo_Claims_1/Qty_1,  0),
      #        Change_in_qty = Qty_2 - Qty_1,
      #        New_Customer = ifelse(Qty_1==0 & Qty_2!=0,1,0),        # Indicator for new customer, might be useful later
      #        Lost_Customer = ifelse(Qty_1!=0 & Qty_2==0,1,0),       # Indicator for lost customer, might be useful later
      #        TM_change = TM_percent_2-TM_percent_1,
      #        # Impact on TM is PFD's formula from word doc, not sure if legit but can go with this
      #        Impact_on_TM = (-Change_in_cost+Change_in_sell+Change_in_claim)*Qty_2) %>% 
      ############ UPDATED MUTATE FROM COMPARE_PERIODS_V2() #############
      mutate(Change_in_cost =  COGS_2 - COGS_1,
             Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
             Change_in_sell = Selling_Price_2 - Selling_Price_1,
             Change_in_COGS_u = COGS_u_2 - COGS_u_1,
             Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
             Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
             Change_in_promo = Promo_Claims_2 - Promo_Claims_1,
             Change_in_qty = Qty_2 - Qty_1,
             TM_change = TM_percent_2-TM_percent_1,
             # Impact on TM is PFD's formula from word doc, not sure if legit but can go with this
             Impact_on_TM = (-Change_in_cost+Change_in_sell+Change_in_claim)*Qty_2,
             # Harmonic mean formula adapted from margin_breakdown() <- need to check
             harmonic_mean = (Net_Sales_1 + Net_Sales_2)/2) %>% 
      mutate(Price_eff = Change_in_sell*Qty_1,
             COGS_eff = -Change_in_COGS_u*Qty_1,
             Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
             Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2)) %>%
      # Calculations for percentage difference <- need to check 
      mutate(
              # # Price_eff_perc = Price_eff/harmonic_mean*100,
              # COGS_eff_perc = COGS_eff/harmonic_mean*100,
              # Qty_eff_perc2 = Qty_eff/harmonic_mean*100,
              # # Calculation for percentage effect of various component based off of waterfall chart generategraph14
              Price_eff_perc = (sum(ifelse((Qty_1!=0 & Qty_2!=0 & Selling_Price_1<=Selling_Price_2), Price_eff, 0))
                              + sum(ifelse((Qty_1!=0 & Qty_2!=0 & Selling_Price_1>Selling_Price_2), Price_eff, 0)))/harmonic_mean*100,
              COGS_eff_perc = (sum(ifelse((Qty_1!=0 & Qty_2!=0 & COGS_u_2<=COGS_u_1), COGS_eff, 0)) 
                              + sum(ifelse((Qty_1!=0 & Qty_2!=0 & COGS_u_2>COGS_u_1), COGS_eff, 0)))/harmonic_mean*100,
              Qty_eff_perc2 = (sum(ifelse((Qty_1!=0 & Qty_2!=0 & Qty_2>=Qty_1), Qty_eff, 0)) 
                              + sum(ifelse((Qty_1!=0 & Qty_2!=0 & Qty_2<Qty_1), Qty_eff, 0)))/harmonic_mean*100,
              # Currently these two values are not included in TM trends tab
              New_sales = (sum(ifelse((Qty_1!=0 & Qty_2!=0), Change_in_sales, 0)) 
                              - sum(ifelse((Qty_1!=0 & Qty_2!=0), Change_in_cost, 0)))/harmonic_mean*100,

              Lost_sales = (sum(ifelse((Qty_1!=0 & Qty_2!=0), Change_in_sales, 0)) 
                              - sum(ifelse((Qty_1!=0 & Qty_2!=0), Qty_eff, 0)))/Change_in_cost*100,

              Change_in_claim_perc = Change_in_claim/harmonic_mean*100,
              Change_in_promo_perc = Change_in_rebate/harmonic_mean*100,
              Change_in_rebate_perc = Change_in_qty/harmonic_mean*100) %>%
      #Formulas for volume weighted component impact 
      mutate(Impact_on_TM_volperc = Impact_on_TM/harmonic_mean*100,
              Price_eff_volperc = Price_eff/harmonic_mean*100,
              COGS_eff_volperc = COGS_eff/harmonic_mean*100,
              Qty_eff_volperc = Qty_eff/harmonic_mean*100,
              Change_in_claim_volperc = Change_in_claim/harmonic_mean*100,
              Change_in_promo_volperc = Change_in_promo/harmonic_mean*100,
              Change_in_rebate_volperc = Change_in_rebate/harmonic_mean*100) %>%
      filter(!(Qty_1==0 & Qty_2==0)) 
    return(tm)
  }


  df_1_vs_2 <- compare_periods(tm_test_1_item, tm_test_2_item)
  
  # Assign as global variable for usage in dashboard functions later
  assign("sales_with_margin", df_1_vs_2, envir = .GlobalEnv)

}


# Function for Trading Margin Trends Tab
reset_trend_filters <- function(df_raw){

  #' *Select relevant/Drop irelevant columns*
  tm_test <- df_raw %>% 
    # select(-Column_1) %>% 
    mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
    mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
    mutate(
      # Item_Description = gsub('#|[*]', '', Item_Description),
          Customer_Name = gsub("^\\*", "", Customer_Name)) 
    #       %>%
    # filter(Item_Code!='TRAILER', Item_Code!='MEMO')

  # Get the user-specified dates 
  starttime1 <- Start_Time_P1
  endtime1 <- End_Time_P1
  starttime2 <- Start_Time_P2
  endtime2 <- End_Time_P2

  # Filter for desired date range
  tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
  tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)

  # Combine data tables for 2 time periods
  # This is required for obtaining the full list of unique elements for each filter (step below)
  tm_test_temp <- tm_test_1 %>% bind_rows(tm_test_2)

  # Pre-populate filters 
  State_Nm <- data.frame("Site_State" = t(c("All"))) %>% 
    rbind(tm_test_temp %>% select(Site_State) %>% distinct() %>% arrange(Site_State))
  Key_ac <- data.frame("Customer_Type" = t(c("All"))) %>% 
    rbind(tm_test_temp %>% select(Customer_Type) %>% distinct() %>% arrange(Customer_Type))
  Cus_Nm <- data.frame("Customer_Name" = t(c("All"))) %>% 
    rbind(tm_test_temp %>% select(Customer_Name) %>% distinct() %>% arrange(Customer_Name))

  # For every press of reset filters button, resets selection of filters and displays 'All'
  gui.clearChoices("this","Trend_Checkbox_1")
  gui.addChoices("this","Trend_Checkbox_1",State_Nm$Site_State, State_Nm$Site_State)
  gui.setChoiceText("this","Trend_Checkbox_1",State_Nm[1,"Site_State"],State_Nm[1,"Site_State"])
  gui.setValue("this","Trend_Checkbox_1", "All") 

  gui.clearChoices("this","Trend_Checkbox_2")
  gui.addChoices("this","Trend_Checkbox_2",Key_ac$Customer_Type, Key_ac$Customer_Type)
  gui.setChoiceText("this","Trend_Checkbox_2",Key_ac[1,"Customer_Type"],Key_ac[1,"Customer_Type"] )
  gui.setValue("this","Trend_Checkbox_1", "All") 

  gui.clearChoices("this","Trend_Checkbox_3")
  gui.addChoices("this","Trend_Checkbox_3",Cus_Nm$Customer_Name, Cus_Nm$Customer_Name)
  gui.setChoiceText("this","Trend_Checkbox_3",Cus_Nm[1,"Customer_Name"],Cus_Nm[1,"Customer_Name"])
  gui.setValue("this","Trend_Checkbox_1", "All") 

  gui.setValue("this","Change_Selector", "Trading Margin") 
  gui.setValue("this","Weighting_Trends", "Unweighted") 

}



# Function to Update Titles in Trading Margin Trends Tab based on component selected
update_trend_titles <- function(df_raw){

    # Selector for displaying change in component
    change_selector <- Change_Selector 

    # Adjust graph headings based on which component is selected in change selector 
    if (change_selector == "TM_change"){ 
        gui.setValue("this","Perc_Increases", "<strong><center>Top Trading Margin % Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom Trading Margin % Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top Trading Margin $ Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom Trading Margin $ Change</strong> ") 

    } else if (change_selector == "Change_in_sell") {
        gui.setValue("this","Perc_Increases", "<strong><center>Top % Impact of Price on Sales Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom % Impact of Price on Sales Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top $ Impact of Price on Sales Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Top $ Bottom of Price on Sales Change</strong> ") 

    } else if (change_selector == "Change_in_cost") {
        gui.setValue("this","Perc_Increases", "<strong><center>Top % Impact of COGS on Sales Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom % Impact of COGS on Sales Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top $ Impact of COGS on Sales Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom $ Impact of COGS on Sales Change</strong> ") 

    } else if (change_selector == "Change_in_vol") {
        gui.setValue("this","Perc_Increases", "<strong><center>Top % Impact of Volume and Mix on Sales Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom % Impact of Volume and Mix on Sales Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top $ Impact of Volume and Mix on Sales Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom $ Impact of Volume and Mix on Sales Change</strong> ") 

    } else if (change_selector == "Change_in_claim") {
        gui.setValue("this","Perc_Increases", "<strong><center>Top % Change in Supplier Claim Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom % Change in Supplier Claim Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top $ Change in Supplier Claim Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom $ Change in Supplier Claim Change</strong> ") 

    } else if (change_selector == "Change_in_promo") {
        gui.setValue("this","Perc_Increases", "<strong><center>Top % Change in Promotional Claim Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom % Change in Promotional Claim Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top $ Change in Promotional Claim Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom $ Change in Promotional Claim Change</strong> ") 

    } else if (change_selector == "Change_in_rebate") {
        gui.setValue("this","Perc_Increases", "<strong><center>Top % Change in Customer Rebate Change </strong>") 
        gui.setValue("this","Perc_Decreases", "<strong><center>Bottom % Change in Customer Rebate Change</strong> ") 
        gui.setValue("this","Dollar_Increases", "<strong><center>Top $ Change in Customer Rebate Change</strong> ") 
        gui.setValue("this","Dollar_Decreases", "<strong><center>Bottom $ Change in Customer Rebate Change</strong> ") 

    } 

}

















































           
           
'Period 1<br>Trading Margin', 
sum(sales_margin_breakdown_v2$Trading_Margin_1)
/harmonic_mean*100,

'Net Impact of<br>Price on<br>Existing Sales', 
(sum((sales_margin_breakdown_v2 %>% 
        filter(Qty_1!=0, Qty_2!=0, 
                Selling_Price_1<=Selling_Price_2))$Price_eff) 
+ sum((sales_margin_breakdown_v2 %>% 
        filter(Qty_1!=0, Qty_2!=0, 
                Selling_Price_1>Selling_Price_2))$Price_eff))
/harmonic_mean*100,

'Net Impact of<br>COGS on<br>Existing Sales', 
(sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1!=0, Qty_2!=0, 
                    COGS_u_2<=COGS_u_1))$COGS_eff) 
+ sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1!=0, Qty_2!=0, 
                    COGS_u_2>COGS_u_1))$COGS_eff))
/harmonic_mean*100,

'Net Impact of<br>Volume and Mix on<br>Existing Sales', 
(sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1!=0, Qty_2!=0, 
                    Qty_2>=Qty_1))$Qty_eff) 
+ sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1!=0, Qty_2!=0, 
                Qty_2<Qty_1))$Qty_eff))
/harmonic_mean*100,

'New Sales', 
(sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1==0, Qty_2!=0))$Change_in_sales) 
- sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1==0, Qty_2!=0))$Change_in_cost))
/harmonic_mean*100,

'Lost Sales', 
 (sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1!=0, Qty_2==0))$Change_in_sales) 
- sum((sales_margin_breakdown_v2 
        %>% filter(Qty_1!=0, Qty_2==0))$Change_in_cost))
/harmonic_mean*100,

'Net Change in<br>Supplier Claims',
sum(sales_margin_breakdown_v2$Change_in_claim)/harmonic_mean*100,

'Net Change in<br>Promotional Claims',
sum(sales_margin_breakdown_v2$Change_in_promo)/harmonic_mean*100,

'Net Change in<br>Customer Rebates',
sum(sales_margin_breakdown_v2$Change_in_rebate)/harmonic_mean*100,

'Period 2<br>Trading Margin'
sum(sales_margin_breakdown_v2$Trading_Margin_2)/harmonic_mean*100




        waterfall_data = list(
            # 1 'Period 1<br>Trading Margin', 
            (sum(pricing_reasons_table$Trading_Margin_1)),
            # 2 'Impact of<br>Selling Price Increase<br>Greater Than Cost Increase', 
            (sum((pricing_reasons_table %>% filter(Qty_1!=0, 
                                                      Qty_2!=0, 
                                                      Net_Selling_Price_eff>0,
                                                      Net_Cost_Price_eff>0,
                                                      Net_Selling_Price_eff>Net_Cost_Price_eff))
                                                      $Net_TM_impact)),
            # 3 'Impact of<br>Selling Price Increase Only', 
            (sum((pricing_reasons_table %>% filter(Qty_1!=0, 
                                                      Qty_2!=0, 
                                                      Net_Selling_Price_eff>0,
                                                      Net_Cost_Price_eff==0))
                                                      $Net_TM_impact)),
            # 4 'Impact of<br>Selling Price Decrease Only',
            (sum((pricing_reasons_table %>% filter(Qty_1!=0, 
                                                      Qty_2!=0, 
                                                      Net_Selling_Price_eff<0,
                                                      Net_Cost_Price_eff==0))
                                                      $Net_TM_impact)),
            # 5 'Impact of<br>Cost Price Increase Not Passed On', 
            (sum((pricing_reasons_table %>% filter(Qty_1!=0, 
                                                      Qty_2!=0,
                                                      Net_Cost_Price_eff>0,
                                                      Net_Cost_Price_eff>Net_Selling_Price_eff))
                                                      $Net_TM_impact)),
            # 6 'Impact of<br>Selling Price Decrease<br>Greater Than Cost Price Decrease', 
            (sum((pricing_reasons_table %>% filter(Qty_1!=0, 
                                                      Qty_2!=0, 
                                                      Net_Selling_Price_eff>0,
                                                      Net_Cost_Price_eff>0,
                                                      Net_Cost_Price_eff>Net_Selling_Price_eff))
                                                      $Net_TM_impact)),
            # 7 'Impact of<br>New Sales',       
            (sum((pricing_reasons_table %>% filter(Qty_1==0, Qty_2!=0))$Net_TM_impact)),
            # 8 'Impact of<br>Lost Sales', 
            (sum((pricing_reasons_table %>% filter(Qty_1!=0, Qty_2==0))$Net_TM_impact)),
                        # 9 'Period 2<br>Trading Margin'
            (sum(pricing_reasons_table$Trading_Margin_2))
        )
