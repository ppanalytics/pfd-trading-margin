#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#                                                                         #-# 
#-#                              SKELETON CODE                              #-# 
#-#                                                                         #-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-# 



###################-FILE SET-UP - INITIALISES VARIABLES-######################## 

# Import libraries
suppressMessages(library(tidyverse))
suppressMessages(library(lubridate))
suppressMessages(library(dplyr))
suppressMessages(library(readxl))
suppressMessages(library(openxlsx))
suppressMessages(library(rlang))
# suppressMessages(library(AzureStor))
suppressMessages(library(readr))
suppressMessages(library(sparklyr))


# # suppressMessages
# options(error = expression(NULL))
# options(scipen = 999)
# options(pillar.sigfig = 5)

############################-0.OVERALL STRUCUTRE-##################################



# Progress bar
upload_progress <- 0
margin_breakdown_progress <- 0
basket_progress <- 0

# Initial date range
start_time_p1 <- '2019-12-01'
end_time_p1 <- '2019-12-31'
start_time_p2 <-  '2020-12-01'
end_time_p2 <- '2020-12-31'

# # start and end times for one week extract
# start_time_p1 <- '2020-12-01'
# end_time_p1 <- '2020-12-03'
# start_time_p2 <-  '2020-12-04'
# end_time_p2 <- '2020-12-06'

# Frequency-based time selection
time_selector <- "monthly"
weekly_end_selector1 <- "0000-01-01"
weekly_end_selector2 <- "0000-01-01"
quarterly_quarter_selector1 <- "Quarter 1"
quarterly_quarter_selector2 <- "Quarter 1"

counter_1 <- 0

# Initial filter options
custtype_filter_selector <- 'All'
state_filter_selector <- 'All'
sitename_filter_selector <- 'All'
accmanager_filter_selector <- 'All'
customer_filter_selector <- 'All'
itmcategory_filter_selector <- 'All'
item_filter_selector <- 'All'

# Trading Margin Breakdown Variables
filter_option_1 <- 'Item_Description'
xData_Selector <- ''
yData_Selector <- ''
selector_name <- ''
margin_level_selector <- 'Customer_Name'

# breakdown waterfall
margin_breakdown_selector <- 'net_impact'
margin_viewer_selector <- 'percentage_view'
# bubble graph 
bubble_selector <- 'Change_in_sales'
# trading margin trends bars
change_selector <- 'TM_Change'
weighting_trend_selector <- 'dollar_weighted'
# breakdown bar and table selector
breakdown_component_selector <- 'all'
breakdown_sortby_selector <- 'TM_Percent_Change'
breakdown_shownum_selector <- 'topbot'
breakdown_ascdesc_selector <- 'descending'
# underlying data export 
breakdown_underlying_data_name <- ''
breakdown_underlying_data_location <- ''

# Basket Analysis Variables
basket_analysis_customer <- 'All'
customer_level_grouping <-  'Item_Category'

# basket analysis waterfall
basket_waterfall_selector <- 'overview'
basket_waterfall_view <- 'percentage_view'
# basket change analysis bar chart
basketbar_filter_selector <- 'all'
basket_variable_selector <- 'TM_Percent_Change'
basketbar_shownum_selector <- 'topbot'
basketbar_ascdesc_selector <- 'descending'
# pricing reasons waterfall and bar chart
pricing_reason_view <- 'percentage_view'
reason_component_selector <- 'All'
table_sortby_selector <- 'TM_Percent_Change'
table_ascdesc_selector <- 'descending'
reason_shownum_selector <- 'topbot'
# underlying data export 
basket_underlying_data_name <- ''
basket_underlying_data_location <- ''

group1 <- 'None' 
group2 <- 'None' 
group3 <- 'None' 
group4 <- 'None' 
group5 <- 'None' 
group6 <- 'None' 

groupby_v2 <- 'Customer_Name'
GroupByv2 <- 'Customer_Name'


############################-8.DASHBOARD TAB-##################################
MyGraph1 <- FALSE
MyGraph2 <- FALSE
MyGraph2b <- FALSE
MyGraph2c <- FALSE
MyGraph4a <- FALSE
MyGraph4b <- FALSE
MyGraph4c <- FALSE

MyGraph_Basket1 <- FALSE
MyGraph_Basket2a <- FALSE
MyGraph_Basket2b <- FALSE
MyGraph_Basket2c <- FALSE
MyGraph_Basket3 <- FALSE
MyGraph_Basket4a <- FALSE
MyGraph_Basket4b <- FALSE
MyGraph_Basket4c <- FALSE

