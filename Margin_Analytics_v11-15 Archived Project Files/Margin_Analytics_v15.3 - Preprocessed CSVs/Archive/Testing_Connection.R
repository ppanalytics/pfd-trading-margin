
# Function to get user input within textbox and then assigned input to Sales_Data_Text dataframe
get_input_text <- function(){
    user_input_text <- parse(text = User_Input_Text)
    Sales_Data_Test <- eval(user_input_text)
    assign("Sales_Data_Test", Sales_Data_Test, envir = .GlobalEnv) 
}

# Updates the visual GUI of connection testing table
updateTest_Table <- function(){
    # Updates testing table
    gui.setValue("this", "MyGraph_Test", generategraph_Test())

}

# Visualises the table that has been assigned by user input S
generategraph_Test <- function(){

    # Formatting columns needed for display
    Sales_Data_Test <- Sales_Data_Test

    # Get the filter level you want to use
    
    filter_level <- GroupByv2
    filter_text <- str_replace(filter_level, "_", " ")
    filter_text <- sprintf("<b>%s</b>", filter_text)        # get the string variable and css combined 

    # This is the data you will be displaying as a table
    trace0 = list(
        Sales_Data_Test$Site_State,
        Sales_Data_Test$Customer_Type,
        Sales_Data_Test$Customer_Name,
        Sales_Data_Test$Week_Start_Date,
        Sales_Data_Test$Year,
        Sales_Data_Test$Quarter,
        Sales_Data_Test$Month,
        Sales_Data_Test$Qty,
        Sales_Data_Test$Sales_Inc_GST,
        Sales_Data_Test$COGS,
        Sales_Data_Test$Customer_Rebate,
        Sales_Data_Test$Sales_Claims,
        Sales_Data_Test$Promo_Claims
    )

    data = list(
        type = "table",
        # columnwidth = list(1400, 400, 400, 500, 500, 600, 600),         
        header = list(
            values = list(
                c("<b> Site_State </b>"), 
                c("<b> Customer_Type </b>"), 
                c("<b> Customer_Name </b>"), 
                c("<b> Week_Start_Date </b>"), 
                c("<b> Year </b>"), 
                c("<b> Quarter </b>"), 
                c("<b> Month </b>"), 
                c("<b> Qty </b>"), 
                c("<b> Sales_Inc_GST </b>"), 
                c("<b> COGS </b>"), 
                c("<b> Customer_Rebate </b>"), 
                c("<b> Sales_Claims </b>"), 
                c("<b> Promo_Claims </b>")
            ),
            align = "center",
            line = list(
                width = 1,
                color = 'black'
            ),
            fill = list(color = '#2149cc'),
            font = list(
                family = "Arial",
                size = 15,
                color = "white"
            )
        ),
        cells = list(
            values = trace0,
            align = c("left", "center"),
            line = list(
                color = "black",
                width = 1
            ),
            fill = list(
                color = c('white', 'white')
            ),
            font = list(
                family = "Arial",
                size = 13,
                color = c("black")
            )
        )
    )

    layout = list(
        autosize = TRUE, 
        margin = list(
            l = 5,
            r = 10, 
            b = 10,
            t = 10,
            pad = 2
        ),
        paper_bgcolor = 'rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0,0,0,0)'
    )

    return(list(traces=data, layout=layout))
}
