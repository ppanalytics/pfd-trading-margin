
library(sparklyr)
library(dplyr)


config <- spark_config()
config$`sparklyr.shell.driver-memory` <- "4G"
config$`sparklyr.shell.executor-memory` <- "4G"
config$`spark.yarn.executor.memoryOverhead` <- "1G"
config$`spark.driver.maxResultSize` <- "0"


sc <- spark_connect(method = "databricks", 
                    spark_home = "c:/users/gaov/anaconda3/envs/dbtest/lib/site-packages/pyspark",
                    config = config)


# Reading in entire base table
tm_full_test <- spark_read_csv(sc, 
                               path="dbfs:/FileStore/df/pfd_trading_margin_full.csv/part-00000-tid-8852796306218385673-c0757c75-5f05-4171-bc92-4aa5dca4e676-197-1-c000.csv")

Start_Time_P1 <- '2019-12-01'
End_Time_P1 <- '2019-12-31'
Start_Time_P2 <-  '2020-12-01'
End_Time_P2 <- '2020-12-31'

# Filters for Pricing Reason Tab
reason_custtype_selector <- 'All'
reason_state_selector <- 'All'
reason_sitename_selector <- 'All'
reason_accmanager_selector <- 'All'
reason_customer_selector <- 'All'
reason_itmcategory_selector <- 'All'
reason_item_selector <- 'All'
reason_grouping_selector <- 'Item_Description'
waterfall_view_selector <- 'dollar_view'
table_sortby_selector <- 'TM_Percent_Change'
table_ascdesc_selector <- 'descending'


# Adapted from Drill_Down_2 function
pricing_reasons <- function(){
  
  # Reset filters upon first run of code for Pricing Reasons tab
  if (counter_j == 0){
    reason_custtype_selector <- 'All'
    gui.setValue("this","Reason_CustType_Selector", "All")   
    reason_state_selector <- 'All'
    gui.setValue("this","Reason_State_Selector", "All")   
    reason_sitename_selector <- 'All'
    gui.setValue("this","Reason_SiteName_Selector", "All")   
    reason_accmanager_selector <- 'All'
    gui.setValue("this","Reason_AccManager_Selector", "All")   
    reason_customer_selector <- 'All'
    gui.setValue("this","Reason_Customer_Selector", "All")   
    reason_itmcategory_selector <- 'All'
    gui.setValue("this","Reason_ItmCategory_Selector", "All")   
    reason_item_selector <- 'All'
    gui.setValue("this","Reason_Item_Selector", "All")   
    assign("counter_j", 1, envir = .GlobalEnv) 
  } else {
    reason_custtype_selector <- Reason_CustType_Selector
    reason_state_selector <- Reason_State_Selector
    reason_sitename_selector <- Reason_SiteName_Selector
    reason_accmanager_selector <- Reason_AccManager_Selector
    reason_customer_selector <- Reason_Customer_Selector
    reason_itmcategory_selector <- Reason_ItmCategory_Selector
    reason_item_selector <- Reason_Item_Selector
  }
  

  ########################## SPARKLYR FUNCTION ########################## 
  
  tm_test <- tm_full_test %>%
    na.replace(Customer_No = "UNK",
               Customer_Name = "UNK",
               Customer_Type = "UNK",
               Key_Account = "UNK",
               Account_Manager = "UNK",
               Invoice_date = "UNK",  
               Week_Start_Date = "UNK",
               Item_Code = "UNK",
               Supplier_Name = "UNK",
               Item_Description = "UNK",
               Item_Category = "UNK",
               Qty = 0, 
               Sales_Inc_GST = 0, 
               COGS = 0, 
               Site = 0,        
               Site_State = "UNK",
               Site_Name = "UNK",  
               Brand = "UNK",
               Customer_Rebate = 0, 
               Sales_Claims = 0, 
               Promo_Claims = 0) %>%
    mutate(Customer_Name = regexp_replace(Customer_Name, "^\\*", "")) %>%
    mutate(Item_Description = regexp_replace(Item_Description, "#|[*]", "")) %>%
    filter(Item_Code!="TRAILER", Item_Code!='MEMO')
  
  #' *Filter for desired date range*
  date_range_filter <- function(df, time1, time2){
    df1 <- df %>% filter(Week_Start_Date >= time1 & Week_Start_Date <= time2)
    return(df1)
  }
  
  starttime1 <- Start_Time_P1
  starttime2 <- Start_Time_P2
  endtime1 <- End_Time_P1
  endtime2 <- End_Time_P2
  
  
  tm_test_1 <- date_range_filter(tm_test, starttime1, endtime1)
  tm_test_2 <- date_range_filter(tm_test, starttime2, endtime2)
  
  tm_test_temp <- tm_test_1 %>% rbind(tm_test_2)
  
  
  # if (reason_custtype_selector=='All' | is.null(reason_custtype_selector) | is.na(reason_custtype_selector)){
  #   custtype_group = as.list(collect(tm_test_temp %>% select(Customer_Type) %>% distinct()))
  #   # custtype_group = (tm_test_temp %>% select(Customer_Type) %>% distinct())$Customer_Type
  # } else {
  #   custtype_group <- reason_custtype_selector
  # }
  # 
  # if (reason_state_selector=='All' | is.null(reason_state_selector) | is.na(reason_state_selector)){
  #   # state_group = as.list(collect(tm_test_temp %>% select(Site_State) %>% distinct()))
  #   state_group = (tm_test_temp %>% select(Site_State) %>% distinct())$Site_State
  # } else {
  #   state_group <- reason_state_selector
  # }
  # 
  # if (reason_sitename_selector=='All' | is.null(reason_sitename_selector) | is.na(reason_sitename_selector)){
  #   # sitename_group = as.list(collect(tm_test_temp %>% select(Site_Name) %>% distinct()))
  #   sitename_group = (tm_test_temp %>% select(Site_Name) %>% distinct())$Site_Name
  # } else {
  #   sitename_group <- reason_sitename_selector
  # }
  # 
  # if (reason_accmanager_selector=='All' | is.null(reason_accmanager_selector) | is.na(reason_accmanager_selector)){
  #   # accmanager_group = as.list(collect(tm_test_temp %>% select(Account_Manager) %>% distinct()))
  #   accmanager_group = (tm_test_temp %>% select(Account_Manager) %>% distinct())$Account_Manager
  # } else {
  #   accmanager_group <- reason_accmanager_selector
  # }
  # 
  # if (reason_customer_selector=='All' | is.null(reason_customer_selector) | is.na(reason_customer_selector)){
  #   # customer_group = as.list(collect(tm_test_temp %>% select(Customer_Name) %>% distinct()))
  #   customer_group = (tm_test_temp %>% select(Customer_Name) %>% distinct())$Customer_Name
  # } else {
  #   customer_group <- reason_customer_selector
  # }
  # 
  # if (reason_itmcategory_selector=='All' | is.null(reason_itmcategory_selector) | is.na(reason_itmcategory_selector)){
  #   # itmcategory_group = as.list(collect(tm_test_temp %>% select(Item_Category) %>% distinct()))
  #   itmcategory_group = (tm_test_temp %>% select(Item_Category) %>% distinct())$Item_Category
  # } else {
  #   itmcategory_group <- reason_itmcategory_selector
  # }
  # 
  # if (reason_item_selector=='All' | is.null(reason_item_selector) | is.na(reason_item_selector)){
  #   # item_group = as.list(collect(tm_test_temp %>% select(Item_Description) %>% distinct()))
  #   item_group = (tm_test_temp %>% select(Item_Description) %>% distinct())$Item_Description
  # } else {
  #   item_group <- reason_item_selector
  # }
  # 
  # tm_test_1 <- tm_test_1 %>%
  #   filter(Customer_Type %in% custtype_group,
  #          Site_State %in% state_group,
  #          Site_Name %in% sitename_group,
  #          Account_Manager %in% accmanager_group,
  #          Customer_Name %in% customer_group,
  #          Item_Category %in% itmcategory_group,
  #          Item_Description %in% item_group)
  # 
  # tm_test_2 <- tm_test_2 %>%
  #   filter(Customer_Type %in% custtype_group,
  #          Site_State %in% state_group,
  #          Site_Name %in% sitename_group,
  #          Account_Manager %in% accmanager_group,
  #          Customer_Name %in% customer_group,
  #          Item_Category %in% itmcategory_group,
  #          Item_Description %in% item_group)
  
  #' *Calculate trading margin*
  calculate_TM <- function(df){
    df1 <- df %>% 
      group_by(Customer_Type, Site_State, Site_Name, Account_Manager, 
               Customer_Name, Item_Category, Item_Description) %>%
      summarise(Qty=sum(Qty),
                Sales_Inc_GST=sum(Sales_Inc_GST),
                COGS=sum(COGS),
                Customer_Rebate=sum(Customer_Rebate),
                Sales_Claims=sum(Sales_Claims), 
                Promo_Claims=sum(Promo_Claims)) %>% 
      mutate(Selling_Price = (Sales_Inc_GST)/Qty,
             Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
             Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate),
             Net_Cost_Price = COGS/Qty) %>%
      mutate(Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims) %>%
      mutate(TM_percent = Trading_Margin/Sales_Inc_GST*100)
    # mutate(Selling_Price = (Sales_Inc_GST)/Qty,
    #         # COGS_u = COGS/Qty,
    #         Trading_Margin = Sales_Inc_GST-COGS+Customer_Rebate+Sales_Claims+Promo_Claims,
    #         Net_Sales = Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate,
    #         TM_percent = ifelse(round(Qty,0)!=0, Trading_Margin/(Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)*100,0),
    #         TM_percent = ifelse(is.na(TM_percent), 0, TM_percent),
    #         # Additional formula from excel sheet "Customer Example" provided by PFD
    #         Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate),
    #         Net_Cost_Price = COGS/Qty)
    return(df1)
  }
  
  tm_test_1_item <- calculate_TM(tm_test_1)
  tm_test_2_item <- calculate_TM(tm_test_2)
  
  
  compare_periods <- function(df1, df2){
    tm <- df1 %>% 
      full_join(df2, 
                by=c("Customer_Type",
                     "Site_State",
                     "Site_Name",
                     "Account_Manager",
                     "Customer_Name",
                     "Item_Category",
                     "Item_Description"),
                suffix=c("_1","_2")) %>%
      ungroup() %>%
      # rename_all(function(x) paste0(x,"_1")) %>%
      # bind_cols(df2 %>% rename_all(function(x) paste0(x,"_2"))) %>% 
      ########################## SPARKLYR FUNCTION ########################## 
      # mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>% 
      # mutate_if(is.character, ~replace(., is.na(.), 'UNK')) %>%
      na.replace(Customer_Type = "UNK",
                 Site_State  = "UNK",
                 Site_Name = "UNK",
                 Account_Manager = "UNK",
                 Customer_Name = "UNK",
                 Item_Category = "UNK",
                 Item_Description = "UNK",
                 Qty_1 = 0, 
                 Sales_Inc_GST_1 = 0, 
                 COGS_1 = 0, 
                 Customer_Rebate_1 = 0, 
                 Sales_Claims_1 = 0, 
                 Promo_Claims_1 = 0, 
                 Selling_Price_1 = 0, 
                 Net_Sales_1 = 0, 
                 Net_Selling_Price_1 = 0, 
                 Net_Cost_Price_1 = 0, 
                 Trading_Margin_1 = 0, 
                 TM_percent_1 = 0,                 
                 Qty_2 = 0, 
                 Sales_Inc_GST_2 = 0, 
                 COGS_2 = 0, 
                 Customer_Rebate_2 = 0, 
                 Sales_Claims_2 = 0, 
                 Promo_Claims_2 = 0, 
                 Selling_Price_2 = 0, 
                 Net_Sales_2 = 0, 
                 Net_Selling_Price_2 = 0, 
                 Net_Cost_Price_2 = 0, 
                 Trading_Margin_2 = 0, 
                 TM_percent_2 = 0) %>%
      mutate(Change_in_cost =  COGS_2 - COGS_1,
             Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
             Change_in_sell = Selling_Price_2 - Selling_Price_1,
             Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
             Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
             Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
             Change_in_qty = Qty_2 - Qty_1,
             TM_change = Trading_Margin_2-Trading_Margin_1,
             TM_percent_change = TM_percent_2 - TM_percent_1,
             #  Additional formula from excel sheet "Customer Example" provided by PFD
             Net_Selling_Price_eff = Net_Selling_Price_2/Qty_2 - Net_Selling_Price_1/Qty_1,
             Net_Cost_Price_eff = COGS_2/Qty_2 - COGS_1/Qty_1) %>% 
      mutate(Net_TM_impact = ifelse((Qty_1!=0 & Qty_2!=0), 
                                    ((Net_Selling_Price_eff - Net_Cost_Price_eff) * Qty_2),
                                    Trading_Margin_2 - Trading_Margin_1)) %>%
      # mutate(Change_in_cost =  COGS_2 - COGS_1,
      #        Change_in_sales = Sales_Inc_GST_2 - Sales_Inc_GST_1,
      #        Change_in_sell = Selling_Price_2 - Selling_Price_1,
      #        Change_in_COGS_u = COGS_u_2 - COGS_u_1,
      #        Change_in_claim = Sales_Claims_2 - Sales_Claims_1,
      #        Change_in_rebate = Customer_Rebate_2 - Customer_Rebate_1,
      #        Change_in_promo = Promo_Claims_2 - Promo_Claims_1, 
      #        Change_in_qty = Qty_2 - Qty_1,
      #        TM_change = Trading_Margin_2-Trading_Margin_1,
      #        TM_percent_change = TM_percent_2 - TM_percent_1,
      #         #  Additional formula from excel sheet "Customer Example" provided by PFD
    #        Change_in_net_selling_price = Net_Selling_Price_2/Qty_2 - Net_Selling_Price_1/Qty_1, 
    #        Change_in_net_cost_price = Net_Cost_Price_2 - Net_Cost_Price_1) %>% 
    # # Additional formula from excel sheet "Customer Example" provided by PFD
    # # Net_Selling_Price = (Sales_Inc_GST+Sales_Claims+Promo_Claims+Customer_Rebate)/Qty,
    # # mutate(Net_Selling_Price_eff = (Net_Selling_Price_2/Qty_2)-(Net_Selling_Price_1/Qty_1),
    # mutate(Net_Selling_Price_eff = Net_Selling_Price_2 - Net_Selling_Price_1,
    #        Net_Cost_Price_eff = Net_Cost_Price_2 - Net_Cost_Price_1) %>%
    # mutate(Net_TM_impact = (Net_Selling_Price_eff - Net_Cost_Price_eff)*Qty_2) %>%
    # # mutate(Price_eff = Change_in_sell*Qty_1,
    # #      COGS_eff = -Change_in_COGS_u*Qty_1,
    # #      Qty_eff = (Selling_Price_2 - COGS_u_2)*Change_in_qty,
    # #      Qty_eff_perc = (Selling_Price_2 - COGS_u_2)*(Qty_2*Net_Sales_1 - Qty_1*Net_Sales_2),
    # # filter(!(Qty_1==0 & Qty_2==0)) %>%
    arrange(Change_in_qty)
    return(tm)
  }
  
  df_drilldown_comparison <- compare_periods(tm_test_1_item, tm_test_2_item) 
  df_drilldown_comparison <- collect(df_drilldown_comparison)
  # %>%
  #   mutate_if(is.numeric, ~replace(., is.na(.), 0)) %>%
  #   mutate_if(is.numeric, ~round(., 2)) 
  
  assign("pricing_reasons_table", df_drilldown_comparison, envir = .GlobalEnv)
}