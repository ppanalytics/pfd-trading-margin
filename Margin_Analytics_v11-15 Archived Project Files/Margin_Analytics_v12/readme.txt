
############################## To Do ##############################
- REASON TAB
    1. Review maths and logic behind waterfall chart
    2. Conditional formatting for Underlying Data
        - include conditional formatting for 'Detailed Analysis' tab as well  
- BASKET ANALYSIS
    - Check to see if values are correct for those that are actually basket changes as in, increase in qty or decrease in qty
- TRENDS TAB   
    - add "New Sales" and "Lost Sales" to components
- DETAILED ANALYSIS TAB
    - conditional formatting for underlying data in 'Detailed Analysis' tab
- BREAKDOWN TAB
    - Add new display option to breakdown tab based off of new waterfall requirements
- UI CHANGES
    - Filters
        - try getting rid of 'all' button
        - get reset filter button to update entire dashboard upon click
    - Resolver error when filters in TM trends & reasons are not selected
    - Remove user input bar from tab 1
    - Update descriptions for tab 1
- ADDITIONAL COMMENTS FROM PFD 
    - Price vs Volume impact needs been shown
        - e.g. yes, i've increased price, but volume has gone through the roof  
    - what are the items that the cost price increase not passed on, illustrate the impact of each individual item 


############################## QUESTIONS FOR PFD ##############################
- Are there any columns that should be added or removed for underlying data in reasons tab?

############################## VERSION UPDATES ##############################
v11.3
- Removed test for querying from user input and data table in script

importFile();get_all_filters();compare_the_pair();updateDashboardVisuals();margin_breakdown();updateMarginVisuals();customers_of_interest(Sales_Data);updateCustomersofInterest();deep_insights(Sales_Data);updateDeepInsightsVisuals();get_filters_2();drill_down_2();updateDrillDownVisuals_2();pricing_reasons();updateReasonVisuals();get_reason_filters();update_time_view();basket_preprocessing();get_basket_filters();updateNewBasketVisuals();
importFile();get_all_filters();margin_breakdown();updateMarginVisuals();

v11.2
- TM Trends Tab
    - change to channel, state, branch, sales rep
    - update filters in TM trends tab such that they change with selection
    - Updated hover format for bar charts
- Basket Analysis tab
    - Underlying data table to accompany basket analysis
- Data is loaded when dashboard is opened
    - Removed "importFile("Sales"), importFileComplete("Complete"), importFileSum("Summary")" from "Load Data" GUI button
- Reason tab 
    - Linked grouping for bar charts and table so that only one selector is used
    - Updated hover format for bar charts
- Added in first tab: user-input bar and ability to affect dataframe

v11.1
- TMA GUI moved time filter to seperate container
- Replaced all Start_Time_n and End_Time_n variables
    - changed to Start_Time_P1, Start_Time_P2, End_Time_P1, End_Time_P2
        - changed in Skeleton_Code, Preprocessing, Time_Functions, Drilldown_Functions and Pricing_Reason_Functions
- Updated Underlying Data table in Reason tab
    - generategraph37 removed formating mutate to table and added formating to trace0 list
- Hid aspects of time GUI upon open of dashboard in Skeleton_Code.R
- Updated waterfall generategraph33 in Reason_Visuals by removing lost sales and new sales columns (index 7 & 8)
    - Saved commented out version with both columns at bottom of Reason_Visuals
- Unhid basket visuals tab, moved all graphs for basket visuals into Basket_Visuals.R
- Migrated all old basket analysis visuals to Basket_Analysis.R
    - created get_basket_filters() and reset_basket_filters() in Basket_Visuals.R
    - added reset of basket visuals to pricing_reasons() in Pricing_Reason_Functions.R 
- Bar chart for basket analysis
- Merge tm breakdown % and $ view with a selector


v11.0
- Fixed % weighting for bottom % change graph in Trading Margin Trends 
- Updated time frequency GUI and created Time_Functions.R to feed into pricing reasons tab
    - added update_time_view() to load button
- Changed Reason_Dashboard_Functions to Reason_Visuals 

v10.7 
- Created a duplicate of trading margin breakdown tab named trading margin pricing reason ctab
    - added updateReasonVisuals() from Drill_down_v2
- Create Pricing_Reason_Functions for trading margin pricing reason tab
    - create pricing_reasons function 
- Created get_reason_filters() to populate filters for trading margin pricing reason tab
- Updated dataframe for Sales_Data and Sales_Data_Complete in Skeleton_Code
- Added functions for Complete_Sales_Data in Importing_Functions
    - added importFile("Complete"); to loading button
- Import TMBASETABLESAMPLE into dashboard
    - incorporate new filters into waterfall chart
        - customer type, state, branch, sales rep, customers then product group
- Seperated reason visuals into Reason_Dashboard_Functions.r
- Created vertical bar charts in tm pricing reason 
- Updated table for TM pricing reason tab
- Added adjustable filters across TM pricing reason tab

v10.6
- Creating change in component filter for TM Trends tab
    - added Change_in_promo to compare_periods() in compare_the_pair()
    - adapted calculate_TM() and compare_periods() in compare_the_pair() with formula components from margin_breakdown()
    - updated graphs for component filter: generategraph1(), generategraph2(), generategraph3(), generategraph4()
        - added variables based on component selection: Selector_Hover, Selector_Title, xData_Selector, yData_Selector
- Created dynamic titles for TM trends in update_trend_titles() based off componet selector
- Updated formula for Price_eff_perc, COGS_eff_perc, and Qty_eff_perc2 in compare_the_pair()

v10.5
- Removed level_selector from TM breakdown tab and in margin_breakdown()
- Incoproated "TMAggregatedBaseTableSummary" into TM breakdown
    - Added importFileSum("Summary") to load data file
    - Updated Importing_Functions.R with importFileSum(), readInFileSum(), and uploadFileSum()
- Moved reference to margin_checkbox_x into margin_breakdown() function instead of compare_the_pair()
    - Commented out Margin_Checkbox_3
- Formatted numbers in waterfall graph for TMB 
- Added Reset Filters Button in Detailed Analysis
- Update filter upon selection for TM breakdown and TM trends, updated default selection to customer level
- Updated filters for all tabs
    - pre-selects 'All' for all buttons upon open
    - reset_filters_2() for detailed analysis
    - reset_trend_filters() for TM trends
    - reset_margin_filters() for TM breakdown

v10.4
- Changed to "TMAggregatedBaseTableDecOnly" table
    - Updated key_account to customer_type 
- Incorporating new filters to trading margin breakdown tab and adding grouped filters to margin_breakdown() in preprocessing
- Updating filters in TM breakdown & TM trends
    - Updated margin_breakdown() and compare_the_pair() function in Preprocessing.R to account for new filters
- UI & Design Changes
    - Changing hover colour

v10.3
- Resolved inital errors with loading new "MarginAnalyticsAggregatedBaseTable" table
    - Incorporating new csv file with new columns
    - removing Item_Category, Item_Description, Invoice_date, Account_Manager, Item_Brand (Brand), Supplier_Name, Site_Name
    - updating Invoice_date with Week_Start_Date
- removed calls to basket_analysis(Sales_Data);updateBasketVisuals(); in first page button
- updated start time and end time to reflect new december only "MarginAnalyticsAggregatedBaseTableDecOnly.csv" file 

v10.2
- Updating button to "Load Data"
- Renaming originall "Drill Down" tab to "Detailed Analysis"
- Added GUI key account, state and customer filters to tab 2 & 3 but not connected to the graphs 


v10.1
- Reverting back to drill down v2 to resolve issue with new drill down tab not linking correctly to data files and code 
- Removed previous attempt to replicate drill down v2 to v3 
- Updated filters for only state, key account and customer 
- Hidden basket growth tab, detailed insights and drill-down v1
- Removed calls to drill-down v1 in "Display Dashboards" button ("drill_down();updateDrillDownVisuals();")